import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Member } from '../model/Member';
import { Constants } from '../app.constants';

@Injectable({
  providedIn: 'root',
})
export class MemberService {

  api = Constants.SERVER_API_URL;

  constructor(private http: HttpClient) {}

  getMembers(): Observable<Member[]> {
    return this.http.get<Member[]>(`${this.api}` + `members`);
  }
  // lay member theo id
  getMember(id): Observable<Member> {
    return this.http.get<Member>(`${this.api}` + `member/` + `${id}`);
  }

  // sua thong tin ten tai khoan
  editInformation(member): Observable<Member> {
    return this.http.get<Member>(`${this.api}` + `member/` + `${member.id}`);
  }

  // update
  updateMember(member): Observable<Member> {
    return this.http.put<Member>(`${this.api}` + `member/` + `${member.id}`, member);
  }
}
