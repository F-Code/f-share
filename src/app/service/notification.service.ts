import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Constants } from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  api = Constants.SERVER_API_URL;

  constructor(
    private http: HttpClient
  ) { }
  
  getAllNotification() {
    return this.http.get<any[]>(`${this.api}/notifications`);
  }

  getAllNotificationId(id){
    return this.http.get<any[]>(`${this.api}/${id}/notifications`);
  }

  getNotificationId(id){
    return this.http.get<any[]>(`${this.api}/notifications/${id}`);
  }

  getNotificationIdmember(id){
    return this.http.put<any[]>(`${this.api}/${id}/notification-edit`, id);
  }
}
