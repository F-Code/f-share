import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Answer } from '../model/Answer';
import { Course } from '../model/Course';
import { Exercise } from '../model/Exercise';
import { Question } from '../model/Question';
import { QuestionExervise } from '../model/QuestionExervise';
import { Quiz } from '../model/Quiz';
import { Sessions } from '../model/Sessions';
import { Theory } from '../model/Theory';
import { CourseAnswer } from '../model/CourseAnswer';
import { HistoryExercise } from '../model/HistoryExercise';
import { HistoryQuiz } from '../model/HistoryQuiz';
import { QuestionEvaluation } from '../model/QuestionEvaluation';
import { Evaluate } from '../model/Evaluate';
import { AnswersEvaluation } from '../model/AnswersEvaluation';
import { Constants } from '../app.constants';

@Injectable({
  providedIn: 'root',
})
export class QuizzService {
  api = Constants.SERVER_API_URL;

  constructor(private http: HttpClient) {}

  // phan hoc ly thuyet
  getTheorys(): Observable<Theory[]> {
    return this.http.get<Theory[]>(`${this.api}/theorys`);
  }

  // lay theory theo idSession
  getTheoryByIdSession(idSession): Observable<Theory> {
    return this.http.get<Theory>(`${this.api}/${idSession}/theorys`);
  }

  // lay quiz theo idSession
  getQuizByIdSession(idSession): Observable<Quiz> {
    return this.http.get<Quiz>(`${this.api}/${idSession}/quizs`);
  }

  // lay exercise theo idSession
  getExerciseByIdSession(idSession): Observable<Exercise> {
    return this.http.get<Exercise>(`${this.api}/${idSession}/exercises`);
  }

  // lay question theo idQuiz
  getQuestionByIdQuiz(idQuiz): Observable<Question> {
    return this.http.get<Question>(`${this.api}/${idQuiz}/top10-questions`);
  }

  // post answer
  postAnswer(answer: any[]): Observable<Answer> {
    return this.http.post<Answer>(`${this.api}/answer`, answer);
  }

  // lay muoi questionExercise bat ky
  getQuestionExerciseByIdExercise(idExercise): Observable<QuestionExervise> {
    return this.http.get<QuestionExervise>(
      `${this.api}/${idExercise}/top10-question-exercises`
    );
  }

  // post AnswerQuestionExercise
  postAnswerQuestionExercise(couserAswer: any[]): Observable<CourseAnswer> {
    return this.http.post<CourseAnswer>(
      `${this.api}/courseanswer`,
      couserAswer
    );
  }

  // getAnswerByIdMemberAndIdQuiz
  getAnswerByIdMemberAndIdQuiz(idMember, idQuiz): Observable<Answer> {
    return this.http.get<Answer>(`${this.api}/${idMember}/${idQuiz}/answers`);
  }

  // getAnswerExerciseByIdMemberAndIdExercise
  getAnswerExerciseByIdMemberAndIdExercise(
    idMember,
    idExercise
  ): Observable<CourseAnswer> {
    return this.http.get<CourseAnswer>(
      `${this.api}/${idMember}/${idExercise}/courseanswers`
    );
  }

  // them moi HistoryExercise
  postHistoryExercise(historyExercise): Observable<HistoryExercise> {
    return this.http.post<HistoryExercise>(
      `${this.api}/historyexercise`,
      historyExercise
    );
  }

  // them moi HistoryQuiz
  possHistoryQuiz(historyQuiz): Observable<HistoryQuiz> {
    return this.http.post<HistoryQuiz>(`${this.api}/historyquiz`, historyQuiz);
  }

  // api tinh diem
  // lay diem thuoc bai hoc
  getPointHistoryQuiz(idMember): Observable<HistoryQuiz[]> {
    return this.http.get<HistoryQuiz[]>(
      `${this.api}/${idMember}/historyquiz-point-member`
    );
  }

  // lay diem historyExercise
  getPointHistoryExercise(idMember): Observable<HistoryExercise[]> {
    return this.http.get<HistoryExercise[]>(
      `${this.api}/${idMember}/historyexercise-point-member`
    );
  }

  // lay hai muoi Evaluatequestion bat ky
  getEvaluateQuestion(idEvaluate): Observable<QuestionEvaluation> {
    return this.http.get<QuestionEvaluation>(
      `${this.api}/${idEvaluate}/top20-questionevaluates`
    );
  }

  // get Evaluate
  getEvaluate(): Observable<Evaluate> {
    return this.http.get<Evaluate>(`${this.api}/evaluates`);
  }

  // post AnswerQuestionExercise
  postAnswerQuestionEvaluation(
    answersEvaluation
  ): Observable<AnswersEvaluation> {
    return this.http.post<AnswersEvaluation>(
      `${this.api}/answersevaluation`,
      answersEvaluation
    );
  }

  getPointQuestionEvaluation(
    idMember,
    idEvaluation
  ): Observable<AnswersEvaluation[]> {
    return this.http.get<AnswersEvaluation[]>(
      `${this.api}/${idMember}/${idEvaluation}/answersevaluations`
    );
  }

  deleteAnswerQuestionEvaluation(id): Observable<AnswersEvaluation> {
    return this.http.delete<AnswersEvaluation>(
      `${this.api}/answersevaluation/${id}`
    );
  }

  // lay chi tiet session
  getSessionById(idSession): Observable<Sessions> {
    return this.http.get<Sessions>(`${this.api}/session/${idSession}`);
  }
  getCourseById(idCourse): Observable<Course> {
    return this.http.get<Course>(`${this.api}/course/${idCourse}`);
  }
  // lay khoa hoc theo id
  putStatusCourse(course): Observable<Course> {
    return this.http.put<Course>(`${this.api}/course/${course.id}`, course);
  }
}
