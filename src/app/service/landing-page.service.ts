import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Interviews } from '../model/Interviews';
import { Constants } from '../app.constants';

@Injectable({
  providedIn: 'root',
})
export class LandingPageService {

  constructor(private http: HttpClient) {
  }
  
  api = Constants.SERVER_API_URL;

  addInterview(Interview) {
    return this.http.post<Interviews>(this.api + 'interview', Interview);
  }
}
