import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from '../app.constants';
import { Blog } from '../model/Blog';
import { Discussion } from '../model/Discussion';
import { DiscussionComment } from '../model/Discussion-Comment';
import { Like } from '../model/Like';

@Injectable({
  providedIn: 'root',
})
export class DiscussionService {
  api = Constants.SERVER_API_URL;
  constructor(private http: HttpClient) {}

  getDiscussion(): Observable<Discussion[]> {
    return this.http.get<Discussion[]>(this.api + `discusss`);
  }

  // lấy thao luan theo id
  getDiscussionByID(id): Observable<Discussion> {
    return this.http.get<Discussion>(this.api + `discuss/` + id);
  }

  // tạo thao luan mớipa
  postDiscussion(discussion): Observable<Discussion> {
    return this.http.post<Discussion>(this.api + `discuss`, discussion);
  }

  // tao comment
  postComment(disComment): Observable<DiscussionComment> {
    return this.http.post<DiscussionComment>(this.api + `comment`, disComment);
  }

  // lay comment
  getComment(idDiscuss): Observable<DiscussionComment[]> {
    return this.http.get<DiscussionComment[]>(
      `${this.api}${idDiscuss}` + '/comments'
    );
  }

  // sua view
  putViewBlog(discussion): Observable<Blog> {
    return this.http.put<Blog>(
      `${this.api}` + `discuss/` + `${discussion.id}`,
      discussion
    );
  }

  // sua comment
  putComment(comment): Observable<DiscussionComment> {
    return this.http.put<DiscussionComment>(
      `${this.api}comment/${comment.id}`,
      comment
    );
  }

  // lay comment theo ID
  getCommentByID(idComment): Observable<DiscussionComment> {
    return this.http.get<DiscussionComment>(`${this.api}comment/${idComment}`);
  }

  // tao moi like
  postLike(like): Observable<Like> {
    return this.http.post<Like>(`${this.api}like`, like);
  }

  // getLikeByIdMemberAndIdComment
  getLikeByIdMemberAndIdComment(
    idMember,
    idCommentDiscuss
  ): Observable<Like[]> {
    return this.http.get<Like[]>(
      `${this.api}${idMember}/${idCommentDiscuss}/likes-member`
    );
  }

  deleteLikeByIdMemberAndIdComment(id): Observable<Like> {
    return this.http.delete<Like>(`${this.api}like/${id}`);
  }

  deleteComment(id): Observable<Comment> {
    return this.http.delete<Comment>(`${this.api}/comment/${id}`);
  }
}
