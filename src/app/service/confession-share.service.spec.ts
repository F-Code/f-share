import { TestBed } from '@angular/core/testing';

import { ConfessionShareService } from './confession-share.service';

describe('ConfessionShareService', () => {
  let service: ConfessionShareService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfessionShareService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
