import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Bookchelud } from '../model/Bookchelud';
import { Constants } from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class BookcheludService {
  step: 0 | 1 | 2 = 1;

  again() {
    this.step = 0;
  }

  constructor(private http: HttpClient) {
    this.again();
  }

  api = Constants.SERVER_API_URL;
  
  getAllregistrationId(bookchelud){
    return this.http.get<Bookchelud[]>(this.api + 'registration/findRegistrationByIdMember/' + bookchelud);
  }
  getRegistrationById(bookchelud) {
    return this.http.get<Bookchelud>(this.api + 'registration' + '/' + bookchelud);
  }
  addRegistration(bookchelud) {
    return this.http.post<Bookchelud>(this.api + 'registration', bookchelud);
  }
  removeRegistration(ID) {
    return this.http.delete<Bookchelud>(this.api + 'registration' + `/${ID}`);
  }

  pay(Order: object) {
    return this.http.post<any>(this.api + 'vnpay' + '/post', Order);
  }
  
  getCoureaffibyId(bookchelud) {
    return this.http.get<Bookchelud>(this.api + 'courseaffiliate' + '/' + bookchelud);
  }
}
