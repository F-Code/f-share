// import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs';
//
// @Injectable({
//   providedIn: 'root',
// })
// export class UploadImgService {
//   private images: object[] = [];
//   private url = 'https://api.imgur.com/3/image';
//   private clientId = '37d42677e6e0a99';
//   private accessToken = '3f0bbf54ba41f793f16feb3898c08077726c92c3';
//   imageLink: any;
//
//   constructor(private http: HttpClient) {}
//
//   uploadImage(imageFile: File) {
//     const formData = new FormData();
//     formData.append('image', imageFile, imageFile.name);
//
//     const myHeaders = new HttpHeaders({
//       Authorization: 'Bearer ' + this.accessToken,
//     });
//
//     const imageData = this.http.post(this.url, formData, { headers: myHeaders }).toPromise();
//     // console.log(imageData);
//     return imageData;
//   }
//
//   getImages() {
//     return this.imageLink;
//   }
// }

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UploadImgService {
  private url = 'https://api.imgur.com/3/image';
  private accessToken = '3f0bbf54ba41f793f16feb3898c08077726c92c3';
  imageLink: any;

  constructor(private http: HttpClient) {}

  uploadImage(imageFile: File) {
    const formData = new FormData();
    formData.append('image', imageFile, imageFile.name);
    const myHeaders = new HttpHeaders({
      Authorization: 'Bearer ' + this.accessToken,
    });
    const imageData = this.http.post(this.url, formData, { headers: myHeaders }).toPromise();
    return imageData;
  }
  getImages() {
    return this.imageLink;
  }
}
