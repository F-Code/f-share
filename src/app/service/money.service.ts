import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../app.constants';
import { PostComment } from '../model/Post-Comment';
import { Observable } from 'rxjs';
import { Courseaffiliate } from '../model/Courseaffiliate';

@Injectable({
  providedIn: 'root',
})
export class MoneyService {
  constructor(private http: HttpClient) {}

  api = Constants.SERVER_API_URL;

  getAllMember() {
    return this.http.get<any[]>(`${this.api}` + 'members/active');
  }
  getMemberById(subject) {
    return this.http.get<any>(this.api + 'member' + '/' + subject);
  }
  addMember(subject: object) {
    return this.http.post<any>(this.api + 'member', subject);
  }
  removeMember(ID) {
    return this.http.delete<any>(this.api + 'member' + `/${ID}`);
  }

  getAllCourseaffiliate() {
    return this.http.get<any>(this.api + 'courseaffiliates');
  }
  getCourseaffiliateById(post: object) {
    return this.http.get<any>(this.api + 'courseaffiliates' + '/' + post);
  }
  getCourseaffiliateSeclectById(post: object) {
    return this.http.get<any>(this.api + 'courseaffiliate' + '/' + post);
  }
  updateCourseaffiliate(Post): Observable<Courseaffiliate> {
    return this.http.put<Courseaffiliate>(`${this.api}` + 'courseaffiliate' + `/${Post.id}`, Post);
  }

  getAllCourseaffiliateId(Post){
    return this.http.get<Courseaffiliate[]>(this.api + `${Post}/` + 'courseaffiliates/active/');
  }

  getAllselectId(level){
    return this.http.get<any>(this.api + 'courseaffiliate/searchCourseAffiliateLevel/' + level );
  }

  pay(Order: object) {
    return this.http.post<any>(this.api + 'vnpay' + '/post', Order);
  }
//////
  getCommentPost(idDiscuss): Observable<PostComment[]> {
    return this.http.get<PostComment[]>(
      `${this.api}${idDiscuss}` + '/commentposts'
    );
  }
  postCommentPost(disComment): Observable<PostComment> {
    return this.http.post<PostComment>(
      `${this.api}` + '/commentpost',
      disComment
    );
  }
}
