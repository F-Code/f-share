import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { ConfessionShare } from '../model/Confession-share';
import { Constants } from '../app.constants';


@Injectable({
  providedIn: 'root'
})
export class ConfessionShareService {

  api = Constants.SERVER_API_URL;

  constructor(
    private http: HttpClient
  ) {}

  postConfession(confession): Observable<ConfessionShare>{
    return this.http.post<ConfessionShare>(this.api + `confession`, confession);
  }
}
