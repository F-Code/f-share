import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Course} from '../model/Course';
import { Sessions } from '../model/Sessions';
import { Constants } from '../app.constants';


@Injectable({
  providedIn: 'root'
})
export class CourseService {
  
  api = Constants.SERVER_API_URL;

  constructor(
    private http: HttpClient
  ) {
  }

  getCourses(): Observable<Course[]>{
    return this.http.get<Course[]>(this.api + `courses`);
  }

  // chi tiet khoa hoc
  getCourseByID(id): Observable<Course[]>{
    return this.http.get<Course[]>(this.api + `course/` + id);
  }

  // Session
  // lay danh sách bài hoc theo khoa hoc
  getSessionByIdCourse(idCourse): Observable<Sessions[]>{
    return this.http.get<Sessions[]>(`${this.api}${idCourse}/` + 'sessions');
  }

  // lay bai hoc theo id
  getSessionById(idCourse): Observable<Sessions[]>{
    return this.http.get<Sessions[]>(`${this.api}session/${idCourse}`);
  }

}
