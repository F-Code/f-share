import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Member } from '../model/Member';
import { Constants } from '../app.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  step: 0 | 1 | 2 = 1;

  again() {
    this.step = 0;
  }

  constructor(private http: HttpClient) {
    this.again();
  }

  api = Constants.SERVER_API_URL;

  getAccountbyID(ID): Observable<Member> {
    return this.http.get<Member>(`${this.api}` + 'member' + `/${ID}`);
  }

  addAcount(Acount: object) {
    return this.http.post<Member>(this.api + 'member', Acount);
  }

  userLogin(Acount: object) {
    return this.http.post<Member>(this.api + 'member/login', Acount);
  }

  updateAccount(Account): Observable<Member> {
    return this.http.put<Member>(
      `${this.api}` + 'member' + `/${Account.id}`,
      Account
    );
  }
  getAccount(Account) {
    return this.http.get<Member[]>(
      `${this.api}` + 'member/check?email=' + `${Account}`
    );
  }

  pay(Order: object) {
    return this.http.post<any>(this.api + 'vnpay' + '/post', Order);
  }
  payup(Order: object) {
    return this.http.post<any>(this.api + 'vnpay' + '/load', Order);
  }
  addhistorypayup(Order: object) {
    return this.http.post<any>(this.api + 'deposithistory' , Order);
  }
}
