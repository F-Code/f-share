import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Login } from '../model/Login';
import { Forgot } from '../model/Forgot';
import { Observable } from 'rxjs';
import { Member } from '../model/Member';
import { Blog } from '../model/Blog';
import { Constants } from '../app.constants';

@Injectable({
  providedIn: 'root',
})
export class LoginService {

  api = Constants.SERVER_API_URL;

  constructor(private http: HttpClient) {}

  userLogin(user: Login) {
    return this.http.post(`${this.api}` + `member/login`, user);
  }

  // forgot password
  forgot_Password(member: Forgot) {
    return this.http.put(`${this.api}` + `member/forgot-password`, member);
  }

  // get member theo email
  getMemberByEmail(Member) {
    return this.http.get<Member[]>(`${this.api}` + 'member/check?email=' + `${Member}`);
  }

  // update Member
  updateAccount(Member): Observable<Member> {
    return this.http.put<Member>(`${this.api}` + 'member' + `/${Member.id}`, Member);
  }
}
