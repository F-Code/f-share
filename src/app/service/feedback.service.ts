import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../app.constants';
import { Feedback } from '../model/Feedback';

@Injectable()
export class FeedbackService {
  step: 0 | 1 | 2 = 1;

  again() {
    this.step = 0;
  }

  constructor(private http: HttpClient) {
    this.again();
  }

  api = Constants.SERVER_API_URL;

  addFeedback(Feedback: object) {
    return this.http.post<Feedback[]>(this.api + 'feedback', Feedback);
  }
}
