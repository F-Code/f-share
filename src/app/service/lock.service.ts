import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Forgot } from '../model/Forgot';
import { Constants } from '../app.constants';
import { Observable } from 'rxjs';
import { Member } from '../model/Member';

@Injectable()
export class LockService {

  constructor(private http: HttpClient) {
  }

  api = Constants.SERVER_API_URL;

  userForgot(Login: object) {
    return this.http.put<Forgot>(this.api + 'member/forgot-password', Login);
  }

  updateAccount(Account): Observable<Member> {
    return this.http.put<Member>(
      `${this.api}` + 'member' + `/${Account.id}`,
      Account
    );
  }

  getAccount(Account) {
    return this.http.get<Member[]>(
      `${this.api}` + 'member/check?email=' + `${Account}`
    );
  }

  getAllaccount() {
    return this.http.get<Member[]>(`${this.api}` + 'members');
  }
}
