import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Blog } from '../model/Blog';
import { Constants } from '../app.constants';

@Injectable({
  providedIn: 'root',
})
export class BlogService {

  api = Constants.SERVER_API_URL;
  
  constructor(private http: HttpClient) {}

  getBlogs(): Observable<Blog[]> {
    return this.http.get<Blog[]>(this.api + 'blogs');
  }

  // lấy blog theo id
  getBlogsByID(id): Observable<Blog> {
    return this.http.get<Blog>(this.api + 'blog/' + id);
  }

  // tìm kiếm blog
  getBlogByTitle(title): Observable<Blog> {
    return this.http.get<Blog>(this.api + `blog/findBlogByTitle/` + title);
  }

  // tạo blog mới
  postBlog(blog): Observable<Blog> {
    return this.http.post<Blog>(this.api + `blog`, blog);
  }

  // sua blog
  putViewBlog(blog): Observable<Blog> {
    return this.http.put<Blog>(`${this.api}` + `blog/` + `${blog.id}`, blog);
  }
}
