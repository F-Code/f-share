import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../app.constants';
import { Observable } from 'rxjs';
import { Courseaffiliate } from '../model/Courseaffiliate';
import { Bookchelud } from '../model/Bookchelud';

@Injectable({
  providedIn: 'root',
})
export class ManageService {
  constructor(private http: HttpClient) {}

  api = Constants.SERVER_API_URL;

  getCourseaffiliateById(Post) {
    return this.http.get<Courseaffiliate[]>(this.api + 'courseaffiliates' + `/${Post}`);
  }
  getCourseaffiliateSeclectById(Post) {
    return this.http.get<Courseaffiliate>(this.api + 'courseaffiliate' + `/${Post}`);
  }
  addCourseaffiliate(Post) {
    return this.http.post<Courseaffiliate>(this.api + 'courseaffiliate', Post);
  }
  updateCourseaffiliate(Post): Observable<Courseaffiliate> {
    return this.http.put<Courseaffiliate>(`${this.api}` + 'courseaffiliate' + `/${Post.id}`, Post);
  }
  removeCourseaffiliate(ID) {
    return this.http.delete<Courseaffiliate>(this.api + 'courseaffiliate' + `/${ID}`);
  }

  getAllCourseaffiliateId(Post){
    return this.http.get<Courseaffiliate[]>(this.api + 'courseaffiliate/searchCourseAffiliate/' + Post);
  }

  getAllregistrationId(Post){
    return this.http.get<Courseaffiliate[]>(this.api + 'registration/findRegistrationByIdCourseAffiliate/' + Post);
  }

  showregistrationId(Post){
    return this.http.get<Bookchelud[]>(this.api + 'registration/' + Post);
  }
  updateRegistration(Post): Observable<Courseaffiliate> {
    return this.http.put<Courseaffiliate>(`${this.api}` + 'registration' + `/${Post.id}`, Post);
  }

  
}
