import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MemberService } from '../../../../service/Member.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Member } from '../../../../model/Member';

@Component({
  selector: 'app-change-information',
  templateUrl: './change-information.component.html',
  styleUrls: ['./change-information.component.less'],
})
export class ChangeInformationComponent implements OnInit {
  users: Member;
  use: Member;
  private jsonString: string;
  private isVisible = false;
  private pass1;
  private pass2;
  private disName;
  private pass;

  validateForm: FormGroup;
  validateFormName: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }
  submitFormName(): void {
    for (const i in this.validateFormName.controls) {
      this.validateFormName.controls[i].markAsDirty();
      this.validateFormName.controls[i].updateValueAndValidity();
    }
  }

  constructor(
    private fb: FormBuilder,
    private nameValidate: FormBuilder,
    private memberService: MemberService,
    private router: Router,
    private activeRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.jsonString = localStorage.getItem('information');
    this.users = JSON.parse(this.jsonString);
    this.editInformation();
    this.pass = JSON.parse(localStorage.getItem('pass'));

    this.validateForm = this.fb.group({
      pass1: [null, [Validators.required]],
      pass2: [null, [Validators.required]],
    });

    this.validateFormName = this.nameValidate.group({
      disName: [null, [Validators.required]],
    });
  }

  editInformation() {
    this.memberService.getMember(this.users.id).subscribe((data) => {
      this.use = data;
    });
  }

  updateName() {
    this.disName = <HTMLInputElement>document.getElementById('disName')['value'];
    this.memberService.updateMember(this.use).subscribe((data) => {
      alert('Thay doi thanh cong');
      this.editInformation();
    });
  }
  updatePassword() {
    this.memberService.updateMember(this.use).subscribe((data) => {
      this.editInformation();
    });
  }

  showModal(Idmember): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.updatePassword();
    alert('Thay mat khau thanh cong');
    location.reload();
    this.isVisible = false;
  }

  myFunction() {
    this.pass1 = <HTMLInputElement>document.getElementById('pass1')['value'];
    this.pass2 = <HTMLInputElement>document.getElementById('pass2')['value'];
    if (this.pass1 == this.pass2 && this.pass1 == this.pass && this.pass2 == this.pass) {
      this.showModal(this.use.id);
    } else {
      alert('Mat khau cua ban khong trung nhau');
    }
  }
}
