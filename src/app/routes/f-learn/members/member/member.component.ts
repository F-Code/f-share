import { Component, OnInit } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd';
import { Member } from '../../../../model/Member';
import { MemberService } from '../../../../service/Member.service';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.less'],
})
export class MemberComponent implements OnInit {
  members: Member[];
  member: Member;
  isVisible = false;
  constructor(private memberservice: MemberService, private notification: NzNotificationService) {}
  createBasicNotification(): void {
    this.notification.blank('Notification Title', '<app-member-detail></app-member-detail>', {
      nzStyle: {
        width: '600px',
        marginLeft: '-265px',
      },
      nzClass: 'test-class',
    });
  }

  ngOnInit(): void {
    this.getMembers();
  }
  showModal(Idmember): void {
    this.isVisible = true;

    this.memberservice.getMember(Idmember).subscribe((data) => {
      this.member = data;
    });
  }
  handleOk(): void {
    this.isVisible = false;
  }
  // lấy toàn bộ danh sách
  getMembers() {
    this.memberservice.getMembers().subscribe((data) => {
      this.members = data;
      window.localStorage.setItem('members', JSON.stringify(data));
    });
  }
}
