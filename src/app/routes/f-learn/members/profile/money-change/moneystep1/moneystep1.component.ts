import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { AccountService } from '../../../../../../service/account.service';
import { Pay } from '../../../../../../model/Pay';

@Component({
  selector: 'app-moneystep1',
  templateUrl: './moneystep1.component.html',
  styleUrls: ['./moneystep1.component.less']
})
export class Moneystep1Component implements OnInit {
  constructor(
    private passwordServicestep1: AccountService
  ) {
  }

  get item() {
    return this.passwordServicestep1;
  }

  ngOnInit() {
    localStorage.removeItem('VNPhitory');
    localStorage.removeItem('linkVNP');
    localStorage.removeItem('moneyup');
  }

  getText() {
    if ((document.getElementById('notmoney-1').style.display = 'block')) {
      document.getElementById('notmoney-1').style.display = 'none';
    }
    if ((document.getElementById('notmoney-2').style.display = 'block')) {
      document.getElementById('notmoney-2').style.display = 'none';
    }
    if ((document.getElementById('notmoney-3').style.display = 'block')) {
      document.getElementById('notmoney-3').style.display = 'none';
    }
  }

  pay1: Pay = new Pay();
  isVisible = false;
  linkVNP = null;
  money = null;
  moneymax = 20000000;
  moneymin = 10000

  open() {
    this.money = <HTMLInputElement>(document.getElementById('money')['value'].toString().trim());
    if (isNaN(this.money)) {
      document.getElementById('notmoney-1').style.display = 'block';
      document.getElementById('notmoney-1').style.color = 'red';
    } else if (this.money > this.moneymax) {
      document.getElementById('notmoney-2').style.display = 'block';
      document.getElementById('notmoney-2').style.color = 'red';
    } else if (this.money < this.moneymin) {
      document.getElementById('notmoney-3').style.display = 'block';
      document.getElementById('notmoney-3').style.color = 'red';
    } else {
      this.money = <HTMLInputElement>(document.getElementById('money')['value'].toString().trim());
      localStorage.setItem('moneyup', JSON.stringify(this.money));
      this.pay1.price = this.money;
      this.passwordServicestep1.payup(this.pay1).subscribe((data) => {
        localStorage.setItem('linkVNP', JSON.stringify(data.data));
        ++this.item.step;
      });
    }
  }
}
