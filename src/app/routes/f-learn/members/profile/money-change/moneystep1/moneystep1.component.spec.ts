import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Moneystep1Component } from './moneystep1.component';

describe('Moneystep1Component', () => {
  let component: Moneystep1Component;
  let fixture: ComponentFixture<Moneystep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Moneystep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Moneystep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
