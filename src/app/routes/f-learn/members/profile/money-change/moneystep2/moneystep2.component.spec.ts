import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Moneystep2Component } from './moneystep2.component';

describe('Moneystep2Component', () => {
  let component: Moneystep2Component;
  let fixture: ComponentFixture<Moneystep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Moneystep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Moneystep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
