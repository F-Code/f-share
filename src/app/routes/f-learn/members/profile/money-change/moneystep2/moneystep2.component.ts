import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { Member, AccountChange } from '../../../../../../model/Member';
import { AccountService } from '../../../../../../service/account.service';

@Component({
  selector: 'app-moneystep2',
  templateUrl: './moneystep2.component.html',
  styleUrls: ['./moneystep2.component.less']
})
export class Moneystep2Component implements OnInit {

  constructor(
    private accountService: AccountService,
  ) { }

  get item() {
    return this.accountService;
  }

  bookchedule = null;
  moneyupdate = null;
  linkVNP = null;
  moneyupne = null;
  isVisible = false;
  vnphitory = 0;

  ngOnInit() {
    var bookcheduleData = JSON.parse(localStorage.getItem('information'));
    this.linkVNP = JSON.parse(localStorage.getItem('linkVNP'));
    this.moneyupne = JSON.parse(localStorage.getItem('moneyup'));
    this.bookchedule = bookcheduleData;
  }

  open() {
    this.isVisible = true;
    localStorage.setItem('VNPhitory', JSON.stringify(this.vnphitory));
    this.linkVNP = JSON.parse(localStorage.getItem('linkVNP'));
  }

  handleOk() {
    this.isVisible = false;
    localStorage.removeItem('VNPhitory');
  }

  prev() {
    --this.item.step;
    localStorage.removeItem('linkVNP');
  }

}
