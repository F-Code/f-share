import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Moneystep3Component } from './moneystep3.component';

describe('Moneystep3Component', () => {
  let component: Moneystep3Component;
  let fixture: ComponentFixture<Moneystep3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Moneystep3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Moneystep3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
