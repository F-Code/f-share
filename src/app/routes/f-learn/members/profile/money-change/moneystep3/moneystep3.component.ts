import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../../../../../../service/account.service';
import { Deposithistory } from '../../../../../../model//Deposithistory';


@Component({
  selector: 'app-moneystep3',
  templateUrl: './moneystep3.component.html',
  styleUrls: ['./moneystep3.component.less'],
  providers: [AccountService],
})
export class Moneystep3Component implements OnInit {
  constructor(
    private srv: AccountService,
    private router: Router
  ) { }

  get item() {
    return this.srv;
  }

  moneyupdatauser = null;
  historypayup: Deposithistory = new Deposithistory();
  time = new Date();
  summoneyuser = [];
  checkdonemoney = 0;

  ngOnInit() {
    this.summoneyuser = JSON.parse(localStorage.getItem('information'));
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      const parameters = window.location.href;
      var VNPhitory = JSON.parse(localStorage.getItem('VNPhitory'));
      var checkdone = parameters.includes('NCB&vnp_BankTranNo')
      if (parameters.length > 350 && VNPhitory === 0 && checkdone === true) {
        this.checkdonemoney = 1;
        let informationdatamoneyup = JSON.parse(localStorage.getItem('information'));
        let moneyuserData = JSON.parse(localStorage.getItem('moneyup'));
        var money = Number(moneyuserData);
        var moneyuser = Number(informationdatamoneyup.moneyIntalent);
        var sum2money = money + moneyuser;
        this.moneyupdatauser = informationdatamoneyup;
        this.moneyupdatauser.moneyIntalent = sum2money;
        this.srv.updateAccount(this.moneyupdatauser).subscribe((data) => {
          localStorage.setItem('information', JSON.stringify(data));
          this.summoneyuser = JSON.parse(localStorage.getItem('information'));
        });
        this.historypayup.familyName = informationdatamoneyup.familyName + informationdatamoneyup.displayName;
        this.historypayup.emailAddress = informationdatamoneyup.emailAddress;
        this.historypayup.phoneNumber = informationdatamoneyup.phoneNumber;
        this.historypayup.depositAmount = moneyuserData;
        var time = this.time.getHours() + ":" + this.time.getMinutes() + ":" + this.time.getSeconds();
        var date = this.time.getDate()+'-'+(this.time.getMonth()+1)+'-'+this.time.getFullYear();
        var fulltime = time + " " + date
        this.historypayup.registrationPeriod = fulltime;
        this.srv.addhistorypayup(this.historypayup).subscribe((data) => {
        });
        localStorage.removeItem('VNPhitory');
        localStorage.removeItem('linkVNP');
        localStorage.removeItem('moneyup');
        this.ngOnInit();
      }
    }
  }
}
