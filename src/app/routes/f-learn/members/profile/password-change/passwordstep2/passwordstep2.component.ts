import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { Member, AccountChange } from '../../../../../../model/Member';
import { AccountService } from '../../../../../../service/account.service';

@Component({
  selector: 'app-passwordstep2',
  templateUrl: './passwordstep2.component.html',
  styleUrls: ['./passwordstep2.component.less'],
})
export class Passwordstep2Component implements OnInit {
  constructor(private fb: FormBuilder, private accountService: AccountService) {
    this.passwordForm = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm: [
        null,
        [Validators.required,],
      ],
    });
  }

  get item() {
    return this.accountService;
  }

  accountChange: AccountChange = new AccountChange();
  account: Member = new Member();
  passwordForm: FormGroup;
  passwordLoad = null;
  loading = false;

  ngOnInit() {
    var datachange = JSON.parse(localStorage.getItem('informationchange'));
    this.passwordLoad = datachange;
    this.passwordForm.patchValue(datachange);
  }

  get password() {
    return this.passwordForm.controls.password;
  }
  get confirm() {
    return this.passwordForm.controls.confirm;
  }

  getText() {
    if ((document.getElementById('passwordgiong').style.display = 'block')) {
      document.getElementById('passwordgiong').style.display = 'none';
    }
  }

  passwordFormstep2() {
    Object.keys(this.passwordForm.controls).forEach((key) => {
      this.passwordForm.controls[key].markAsDirty();
      this.passwordForm.controls[key].updateValueAndValidity();
    });
    if (this.passwordForm.invalid) {
      return;
    }
    var pass = <HTMLInputElement>(
      document.getElementById('password')['value'].toString().trim()
    );
    var pass2 = <HTMLInputElement>(
      document.getElementById('password2')['value'].toString().trim()
    );
    if (pass == pass2) {
        var changeData = JSON.parse(localStorage.getItem('information'));
        this.account.id = changeData.id;
        this.account.avatar = changeData.avatar;
        this.account.familyName = changeData.familyName;
        this.account.phoneNumber = changeData.phoneNumber;
        this.account.emailAddress = changeData.emailAddress;
        this.account.status = changeData.status;
        this.account.displayName = changeData.displayName;
        this.account.birthYear = changeData.birthYear;
        this.account.province = changeData.province;
        this.account.streetInformation = changeData.streetInformation;
        this.account.position = changeData.position;
        this.account.summary = changeData.summary;
        this.account.school = changeData.school;
        this.account.department = changeData.department;
        this.account.grade = changeData.grade;
        this.account.moneyIntalent = changeData.moneyIntalent;
        this.account.centralRight = changeData.centralRight;
        this.accountService.updateAccount(this.account).subscribe((data) => {
          localStorage.setItem('information', JSON.stringify(data));
          var acountStep2 = JSON.parse(localStorage.getItem('account'));
          this.accountChange.displayNameOrEmailAddress =
            acountStep2.displayNameOrEmailAddress;
          this.accountChange.password = this.passwordForm.value.password;
          this.accountChange.remember = acountStep2.remember;
          localStorage.setItem('account', JSON.stringify(this.accountChange));
          localStorage.removeItem('informationchange');
          this.loading = true;
          setTimeout(() => {
            this.loading = false;
          }, 500);
        });
        ++this.item.step;
    } else {
      document.getElementById('passwordgiong').style.display = 'block';
      document.getElementById('passwordgiong').style.color = 'red';
    }
  }

  prev() {
    --this.item.step;
    localStorage.removeItem('informationchange');
  }
}
