import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passwordstep2Component } from './passwordstep2.component';

describe('Passwordstep2Component', () => {
  let component: Passwordstep2Component;
  let fixture: ComponentFixture<Passwordstep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passwordstep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passwordstep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
