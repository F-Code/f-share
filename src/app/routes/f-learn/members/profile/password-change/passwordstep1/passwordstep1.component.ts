import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { AccountService } from '../../../../../../service/account.service';

@Component({
  selector: 'app-passwordstep1',
  templateUrl: './passwordstep1.component.html',
  styleUrls: ['./passwordstep1.component.less']
})
export class Passwordstep1Component implements OnInit {
  constructor(
    private passwordValidator: FormBuilder,
    private passwordServicestep1: AccountService
  ) {
    this.passwordChangeform = passwordValidator.group({
      password: [
        null,
        [Validators.required,
        ],
      ],
    });
  }

  get password() {
    return this.passwordChangeform.controls.password;
  }
  get confirm() {
    return this.passwordChangeform.controls.confirm;
  }
  get phoneNumber() {
    return this.passwordChangeform.controls.phoneNumber;
  }
  get item() {
    return this.passwordServicestep1;
  }

  account = null;
  passwordChangeform: FormGroup;

  ngOnInit() {
    var informationStep2id = JSON.parse(localStorage.getItem('information'));
    var accountId = informationStep2id.id;
    this.passwordServicestep1.getAccountbyID(accountId).subscribe((data) => {
      this.account = data;
    });
  }

  getText() {
    if ((document.getElementById('notpassword').style.display = 'block')) {
      document.getElementById('notpassword').style.display = 'none';
    }
  }

  passwordFormstep1() {
    Object.keys(this.passwordChangeform.controls).forEach((key) => {
      this.passwordChangeform.controls[key].markAsDirty();
      this.passwordChangeform.controls[key].updateValueAndValidity();
    });
    if (this.passwordChangeform.invalid) {
      return;
    }
    var email = <HTMLInputElement>(
      document.getElementById('password')['value'].toString().trim()
    );
    var account = JSON.parse(localStorage.getItem('account'));
    if (email !== account.password) {
      document.getElementById('notpassword').style.display = 'block';
      document.getElementById('notpassword').style.color = 'red';
    } else {
      Object.assign(this.item, this.passwordChangeform.value);
      ++this.item.step;
      const informationchange = this.account;
      localStorage.setItem(
        'informationchange',
        JSON.stringify(informationchange)
      );
    }
  }
}
