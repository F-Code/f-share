import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passwordstep1Component } from './passwordstep1.component';

describe('Passwordstep1Component', () => {
  let component: Passwordstep1Component;
  let fixture: ComponentFixture<Passwordstep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passwordstep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passwordstep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
