import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../../../../../service/account.service';

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.less'],
  providers: [AccountService],
})
export class PasswordChangeComponent implements OnInit {
  constructor(private srv: AccountService, private router: Router) {}

  get item() {
    return this.srv;
  }

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    }
  }
}
