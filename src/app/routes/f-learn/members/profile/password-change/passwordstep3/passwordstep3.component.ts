import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AccountService } from '../../../../../../service/account.service';

@Component({
  selector: 'app-passwordstep3',
  templateUrl: './passwordstep3.component.html',
  styleUrls: ['./passwordstep3.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Passwordstep3Component {
  constructor(private srv: AccountService) {}

  get item() {
    return this.srv;
  }
}
