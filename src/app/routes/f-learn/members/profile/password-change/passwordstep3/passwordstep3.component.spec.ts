import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Passwordstep3Component } from './passwordstep3.component';

describe('Passwordstep3Component', () => {
  let component: Passwordstep3Component;
  let fixture: ComponentFixture<Passwordstep3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Passwordstep3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Passwordstep3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
