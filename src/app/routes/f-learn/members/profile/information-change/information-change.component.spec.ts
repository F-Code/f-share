import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationChangeComponent } from './information-change.component';

describe('InformationChangeComponent', () => {
  let component: InformationChangeComponent;
  let fixture: ComponentFixture<InformationChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
