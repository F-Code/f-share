import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Member } from '../../../../../../model/Member';
import { AccountService } from '../../../../../../service/account.service';

@Component({
  selector: 'app-informationstep2',
  templateUrl: './informationstep2.component.html',
  styleUrls: ['./informationstep2.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Informationstep2Component implements OnInit {
  constructor(
    private infomationValidator: FormBuilder,
    private infomationServicestep2: AccountService
  ) {
    this.infomationChangeform = this.infomationValidator.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  get item() {
    return this.infomationServicestep2;
  }

  infomationChangestep2: Member = new Member();
  infomationChangeform: FormGroup;
  infomationLoad = null;
  loading = false;

  ngOnInit() {
    var changeData = JSON.parse(localStorage.getItem('informationchange'));
    this.infomationLoad = changeData;
  }

  infomationFormstep2() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      ++this.item.step;
    }, 500);
    var changeData = JSON.parse(localStorage.getItem('informationchange'));
    this.infomationChangestep2.id = changeData.id;
    this.infomationChangestep2.avatar = changeData.avatar;
    this.infomationChangestep2.familyName = changeData.familyName;
    this.infomationChangestep2.phoneNumber = changeData.phoneNumber;
    this.infomationChangestep2.emailAddress = changeData.emailAddress;
    this.infomationChangestep2.password = changeData.password;
    this.infomationChangestep2.status = changeData.status;
    this.infomationChangestep2.displayName = changeData.displayName;
    this.infomationChangestep2.birthYear = changeData.birthYear;
    this.infomationChangestep2.province = changeData.province;
    this.infomationChangestep2.streetInformation = changeData.streetInformation;
    this.infomationChangestep2.position = changeData.position;
    this.infomationChangestep2.summary = changeData.summary;
    this.infomationChangestep2.school = changeData.school;
    this.infomationChangestep2.department = changeData.department;
    this.infomationChangestep2.grade = changeData.grade;
    this.infomationChangestep2.moneyIntalent = changeData.moneyIntalent;
    this.infomationChangestep2.centralRight = changeData.centralRight;
    this.infomationServicestep2.updateAccount(this.infomationChangestep2).subscribe((data) => {
      localStorage.setItem('information', JSON.stringify(data));
    });
    localStorage.removeItem('informationchange');
  }

  prev() {
    --this.item.step;
    localStorage.removeItem('informationchange');
  }
}
