import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Informationstep2Component } from './informationstep2.component';

describe('Informationstep2Component', () => {
  let component: Informationstep2Component;
  let fixture: ComponentFixture<Informationstep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Informationstep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Informationstep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
