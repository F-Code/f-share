import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Informationstep1Component } from './informationstep1.component';

describe('Informationstep1Component', () => {
  let component: Informationstep1Component;
  let fixture: ComponentFixture<Informationstep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Informationstep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Informationstep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
