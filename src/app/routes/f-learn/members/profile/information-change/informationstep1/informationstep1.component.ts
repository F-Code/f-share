import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../../../../../../service/account.service';

@Component({
  selector: 'app-informationstep1',
  templateUrl: './informationstep1.component.html',
  styleUrls: ['./informationstep1.component.less']
})
export class Informationstep1Component implements OnInit {
  constructor(
    private infomationValidator: FormBuilder,
    private infomationServicestep1: AccountService
  ) {
    this.infomationChangeform = this.infomationValidator.group({
      displayName: ['', [Validators.required]],
      familyName: ['', [Validators.required]],
      emailAddress: ['', [Validators.required]],
      password: ['', []],
      birthYear: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required]],
      province: ['', [Validators.required]],
      streetInformation: ['', [Validators.required]],
      position: ['', [Validators.required]],
      summary: ['', [Validators.required,Validators.maxLength(500)]],
      avatar: ['', []],
      school: ['', [Validators.required]],
      department: ['', [Validators.required]],
      grade: ['', [Validators.required]],
      moneyIntalent: ['', []],
    });
  }

  get familyName() {
    return this.infomationChangeform.controls.familyName;
  }
  get emailAddress() {
    return this.infomationChangeform.controls.emailAddress;
  }
  get phoneNumber() {
    return this.infomationChangeform.controls.phoneNumber;
  }
  get password() {
    return this.infomationChangeform.controls.password;
  }
  get avatar() {
    return this.infomationChangeform.controls.avatar;
  }
  get item() {
    return this.infomationServicestep1;
  }

  infomationChangeform: FormGroup;
  informationChangestep1 = null;
  informationStep1 = null;

  ngOnInit() {
    var informationChangestep1 = JSON.parse(localStorage.getItem('information'));
    this.informationChangestep1 = JSON.parse(localStorage.getItem('account'));
    var accountId = informationChangestep1.id;
    this.infomationServicestep1.getAccountbyID(accountId).subscribe((data) => {
      this.informationStep1 = data;
    });
  }

  checkTrimfamilyName() {
    this.informationStep1.familyName = this.informationStep1.familyName.trim();
  }

  infomationFormstep1() {
    Object.keys(this.infomationChangeform.controls).forEach((key) => {
      this.infomationChangeform.controls[key].markAsDirty();
      this.infomationChangeform.controls[key].updateValueAndValidity();
    });
    if (this.infomationChangeform.invalid) {
      return;
    }
    Object.assign(this.item, this.infomationChangeform.value);
    ++this.item.step;
    this.informationChangestep1 = JSON.parse(localStorage.getItem('account'));
    this.informationStep1.password = this.informationChangestep1.password;
    const data = this.informationStep1;
    localStorage.setItem('informationchange', JSON.stringify(data));
  }
}
