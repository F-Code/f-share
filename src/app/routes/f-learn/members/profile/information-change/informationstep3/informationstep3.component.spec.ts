import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Informationstep3Component } from './informationstep3.component';

describe('Informationstep3Component', () => {
  let component: Informationstep3Component;
  let fixture: ComponentFixture<Informationstep3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Informationstep3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Informationstep3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
