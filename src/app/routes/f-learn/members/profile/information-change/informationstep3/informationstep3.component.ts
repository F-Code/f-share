import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AccountService } from '../../../../../../service/account.service';

@Component({
  selector: 'app-informationstep3',
  templateUrl: './informationstep3.component.html',
  styleUrls: ['./informationstep3.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Informationstep3Component {
  constructor(private srv: AccountService) {}

  get item() {
    return this.srv;
  }
}
