import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Member } from '../../../../model/Member';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AccountService } from '../../../../service/account.service';
import { UploadImgService } from '../../../upload-img.service';

@Component({
  selector: 'frofilre-show',
  template: `
      <img class="photo-profile-img" [src]="account.avatar" /><br>
      <div (click)="showModalavatar()" class="download-resume">
      <i class="fas fa-camera" aria-hidden="true"></i>
        <span class="text-download" >Thay ảnh đại diện</span>
      </div>
      <nz-modal
        nzOkText="Ok"
        [nzCancelText]="null"
        [(nzVisible)]="isVisible"
        [nzTitle]="'Chọn ảnh thay đổi'"
        [nzOkLoading]="isOkLoading"
        (nzOnOk)="handleOk()"
        nzWidth="860px"
        [nzClosable]="false"
        [nzVisible]="visible"
      >
        <div class="div-img">
          <form nz-form [formGroup]="avatarForm">
            <input
              type="file"
              formControlName="avatar"
              (change)="onSelectFile($event)"
            />
            <img class="img-change-post" [src]="link" />
          </form>
        </div>
      </nz-modal>
`,
styles: [
  `
  .photo-profile-img{
    height: 500px;
  }
  `,
],
styleUrls: ['./profile.component.less'],
})
export class ProfilreShow  implements OnInit {
  constructor(
    private router: Router,
    private avatarValidator: FormBuilder,
    private accountService: AccountService,
    private uploadImgService: UploadImgService,
  ) {
    this.avatarForm = this.avatarValidator.group({
      avatar: ['', []],
    });
  }

  get avatar() {
    return this.avatarForm.controls.avatar;
  }

  loaddatainfomation: Member = new Member();
  changeAvatar = null;
  avatarForm: FormGroup;
  account: Member[];
  isVisible = false;
  checkchengimage = 0;

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.loaddatainfomation.id = 0;
      this.loaddatainfomation.displayName = "";
      this.loaddatainfomation.familyName = "";
      this.loaddatainfomation.emailAddress = "";
      this.loaddatainfomation.password = "";
      this.loaddatainfomation.birthYear = "";
      this.loaddatainfomation.phoneNumber = "";
      this.loaddatainfomation.province = "";
      this.loaddatainfomation.streetInformation = "";
      this.loaddatainfomation.position = "";
      this.loaddatainfomation.summary = "";
      this.loaddatainfomation.avatar = "";
      this.loaddatainfomation.school = "";
      this.loaddatainfomation.department = "";
      this.loaddatainfomation.grade = "";
      this.loaddatainfomation.status = "";
      this.loaddatainfomation.moneyIntalent = 0;
      this.loaddatainfomation.centralRight = "";
      localStorage.setItem('informationcheck', JSON.stringify(this.loaddatainfomation));
      this.account = JSON.parse(window.localStorage.getItem('informationcheck'));
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.account = JSON.parse(window.localStorage.getItem('information'));
    }
  }

  showModalavatar() {
    this.isVisible = true;
    var informationUser = JSON.parse(localStorage.getItem('information'));
    var informationId = informationUser.id;
    this.accountService.getAccountbyID(informationId).subscribe((data) => {
      this.changeAvatar = data; 
      var passwordAvatar = JSON.parse(localStorage.getItem('account'));
      this.changeAvatar.password = passwordAvatar.password;
      const informationchange = this.changeAvatar;
      localStorage.setItem(
        'informationchange',
        JSON.stringify(informationchange)
      );
    });
  }

  imageFile: File;
  link: string | ArrayBuffer;

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.imageFile = event.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.link = event.target.result;
      };
      this.checkchengimage = 1;
    }
  }
  
  loading = false;

  async handleOk() {
    var changeData = JSON.parse(localStorage.getItem('informationchange'));
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 5000);
    if (this.checkchengimage === 0) {
      this.isVisible = false;
      localStorage.removeItem('informationchange');
    } else {
      const image_imgur = await this.uploadImgService.uploadImage(this.imageFile);
      localStorage.setItem('uploadimage', JSON.stringify(image_imgur));
      var image = JSON.parse(localStorage.getItem('uploadimage'));
      this.changeAvatar.id = changeData.id;
      this.changeAvatar.familyName = changeData.familyName;
      this.changeAvatar.avatar = image.data.link;
      this.changeAvatar.password = changeData.password;
      this.changeAvatar.phoneNumber = changeData.phoneNumber;
      this.changeAvatar.emailAddress = changeData.emailAddress;
      this.changeAvatar.status = changeData.status;
      this.changeAvatar.birthYear = changeData.birthYear;
      this.changeAvatar.province = changeData.province;
      this.changeAvatar.streetInformation = changeData.streetInformation;
      this.changeAvatar.position = changeData.position;
      this.changeAvatar.summary = changeData.summary;
      this.changeAvatar.school = changeData.school;
      this.changeAvatar.department = changeData.department;
      this.changeAvatar.grade = changeData.grade;
      this.changeAvatar.moneyIntalent = changeData.moneyIntalent;
      this.changeAvatar.centralRight = changeData.centralRight;
      this.accountService.updateAccount(this.changeAvatar).subscribe((data) => {
        localStorage.setItem('information', JSON.stringify(data));
        location.reload();
      });
      localStorage.removeItem('informationchange');
      localStorage.removeItem('uploadimage');
      this.link = null;
      this.isVisible = false;
    }
  }

  handleCancel() {
    this.isVisible = false;
    localStorage.removeItem('informationchange');
    this.link = null;
  }
}
