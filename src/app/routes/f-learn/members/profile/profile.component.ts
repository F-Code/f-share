import { Component, OnInit } from '@angular/core';
import { Member } from '../../../../model/Member';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MemberService } from '../../../../service/Member.service';
import { Router } from '@angular/router';
import { UploadImgService } from '../../../../service/upload-img.service';
import { da } from 'date-fns/esm/locale';

@Component({
  selector: 'app-add',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less'],
})
export class ProfileComponent implements OnInit {
  users: Member[];
  showuser: Member[];
  isVisible = false;

  account22: Member = new (class implements Member {
    id: number;
    familyName: string;
    emailAddress: string;
    password: string;
    phoneNumber: string;
    avatar: string;
    status: string;
    displayName: string;
    birthYear: string;
    province: string;
    streetInformation: string;
    position: string;
    summary: string;
    school: string;
    department: string;
    grade: string;
    moneyIntalent: number;
    centralRight: string;
  })();

  formAccount: FormGroup;
  accountLoad = null;
  loading = false;

  imageFile: File;
  link: string | ArrayBuffer;

  constructor(
    private fb: FormBuilder,
    private memberService: MemberService,
    private router: Router,
    private uploadImgService: UploadImgService,
  ) {
    this.formAccount = this.fb.group({
      avatar: ['', []],
    });
  }

  get avatar() {
    return this.formAccount.controls.avatar;
  }

  ngOnInit(): void {
    this.users = JSON.parse(window.localStorage.getItem('information'));
    if (this.users == null) {
      this.router.navigate(['f-learn.poly/login']);
    } else {
      this.showuser = JSON.parse(window.localStorage.getItem('information'));
    }
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.imageFile = event.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.link = event.target.result;
      };
    }
  }

  showModal(): void {
    this.isVisible = true;
    var userLogin = JSON.parse(window.localStorage.getItem('information'));
    var idUser = userLogin.id;

    this.memberService.getMember(idUser).subscribe((data) => {
      this.account22 = data;
      localStorage.setItem('informationchange', JSON.stringify(data));
    });
  }

  async handleOk() {
    this.isVisible = false;
    var datachange = JSON.parse(localStorage.getItem('informationchange'));
    const image_res = await this.uploadImgService.uploadImage(this.imageFile);
    localStorage.setItem('uploadimage', JSON.stringify(image_res));
    var image = JSON.parse(localStorage.getItem('uploadimage'));

    this.account22.avatar = image.data.link;
    this.memberService.updateMember(this.account22).subscribe((data) => {
      localStorage.setItem('information', JSON.stringify(data));
      location.reload();
    });
    localStorage.removeItem('uploadimage');
  }
  handleCancel(): void {
    this.isVisible = false;
    localStorage.removeItem('informationchange');
  }
}
