import { Component, DoCheck, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent } from '@angular/router';
import { Blog } from '../../../../model/Blog';
import { BlogService } from '../../../../service/Blog.service';

@Component({
  selector: 'app-blog-dtail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.less'],
})
export class BlogDetailComponent implements OnInit {
  blog: Blog;
  showLoader = true;

  constructor(private blogservice: BlogService, private activeroute: ActivatedRoute, private router: Router) {
    router.events.subscribe((routerEvent: RouterEvent) => {
      this.checkRouteChange(routerEvent);
    });
  }

  checkRouteChange(routerEvent: RouterEvent) {
    // if route change started
    if (routerEvent instanceof NavigationStart) {
      this.showLoader = true;
    }
    // if route change ended
    if (routerEvent instanceof NavigationEnd || routerEvent instanceof NavigationCancel || routerEvent instanceof NavigationError) {
      this.showLoader = false;
    }
  }

  ngOnInit(): void {
    this.getBlogsByID();
  }

  getBlogsByID() {
    this.activeroute.params.subscribe((param) => {
      return this.blogservice.getBlogsByID(param.id).subscribe((data) => {
        this.blog = data;
      });
    });
  }
}
