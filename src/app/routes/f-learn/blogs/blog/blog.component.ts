import { Component, OnInit } from '@angular/core';
import { Blog } from '../../../../model/Blog';
import { BlogService } from '../../../../service/Blog.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.less'],
})
export class BlogComponent implements OnInit {
  blogs: Blog[];
  blog: Blog;
  searchText;
  page: 5;

  constructor(
    private blogservice: BlogService,
    private activeroute: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.getBlogs();
  }
  getBlogs() {
    this.blogservice.getBlogs().subscribe((data) => {
      this.blogs = data;

      // hàm sắp xếp động
      function compareValues(key, order = 'asc') {
        return function (a, b) {
          if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            // nếu không tồn tại
            return 0;
          }

          const varA =
            typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
          const varB =
            typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

          let comparison = 0;
          if (varA < varB) {
            comparison = 1;
          } else if (varA > varB) {
            comparison = -1;
          }
          return order == 'desc' ? comparison * -1 : comparison;
        };
      }

      this.blogs.sort(compareValues('view', 'asc'));
    });
  }

  // sua luot xem blog
  putBlog(id) {
    return this.blogservice.getBlogsByID(id).subscribe((data) => {
      (this.blog = data), (this.blog.view = this.blog.view + 1);
      this.updateView();
    });
  }

  // update view
  updateView() {
    this.blogservice.putViewBlog(this.blog).subscribe((data) => {});
  }
}
