import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfessionShareComponent } from './confession-share.component';

describe('ConfessionShareComponent', () => {
  let component: ConfessionShareComponent;
  let fixture: ComponentFixture<ConfessionShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfessionShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfessionShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
