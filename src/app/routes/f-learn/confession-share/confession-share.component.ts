import { Component, OnInit, ɵSafeScript } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ConfessionShare } from '../../../model/Confession-share';
import { ConfessionShareService } from '../../../service/confession-share.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-confession-share',
  templateUrl: './confession-share.component.html',
  styleUrls: ['./confession-share.component.less'],
})
export class ConfessionShareComponent implements OnInit {
  confession: ConfessionShare = new (class implements ConfessionShare {
    content: string;
    id: string;
    idMember: string;
    status: boolean;
  })();
  tinymceInit: any;
  private tinymce: any;

  validateForm: FormGroup;

  private user: any;
  private jsonString: string;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  constructor(
    private confessionService: ConfessionShareService,
    private nzMessageService: NzMessageService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.tinymceInit = {
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern',
      ],
      toolbar:
        'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
      image_advtab: true,
      file_picker_callback(cb, value, meta) {
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

        // Note: In modern browsers input[type='file'] is functional without
        // even adding it to the DOM, but that might not be the case in some older
        // or quirky browsers like IE, so you might want to add it to the DOM
        // just in case, and visually hide it. And do not forget do remove it
        // once you do not need it anymore.

        input.onchange = () => {
          const file = input.files[0];

          const reader = new FileReader();
          reader.onload = () => {
            // Note: Now we need to register the blob in TinyMCEs image blob
            // registry. In the next release this part hopefully won't be
            // necessary, as we are looking to handle it internally.
            const id = 'blobid' + new Date().getTime();
            const blobCache = this.tinymce.activeEditor.editorUpload.blobCache;
            let base64 = '';
            if (typeof reader.result === 'string') {
              base64 = reader.result.split(',')[1];
            }
            const blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            // call the callback and populate the Title field with the file name
            cb(blobInfo.blobUri(), { title: file.name });
          };
          reader.readAsDataURL(file);
        };
        input.click();
      },
    };
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      content: [null, [Validators.required]],
    });
    this.jsonString = localStorage.getItem('information');
    this.user = JSON.parse(this.jsonString);
  }

  shareConfession() {
    this.confession.idMember = this.user.id;
    this.confessionService.postConfession(this.confession).subscribe((data) => {
      this.router.navigateByUrl('/f-learn.poly/home');
    });
    this.nzMessageService.info(
      'Nội dung của bạn đang đã được gửi tới Admin. Vui lòng chờ xác nhận tử Admin nhé!!'
    );
  }
}
