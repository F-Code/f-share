import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Sessions } from '../../../../model/Sessions';
import { Theory } from '../../../../model/Theory';
import { CourseService } from '../../../../service/Course.service';
import { Quiz } from '../../../../model/Quiz';
import { QuizzService } from '../../../../service/Quizz.service';
import { Exercise } from '../../../../model/Exercise';

@Component({
  selector: 'app-session-detail',
  templateUrl: './session-detail.component.html',
  styleUrls: ['./session-detail.component.less'],
})
export class SessionDetailComponent implements OnInit {
  session: Sessions[];
  theory: Theory;
  quiz: Quiz = new (class implements Quiz {
    id: string;
    idSession: string;
    reviewName: string;
  })();
  exe: Exercise = new (class implements Exercise {
    id: string;
    idSession: string;
    title: string;
  })();
  constructor(private sessionServer: CourseService, private quizzService: QuizzService, private activeroute: ActivatedRoute) {}

  ngOnInit(): void {
    this.getQuizByIdSession();
    this.getSessionByID();
    this.getExerciseByIdSession();
    this.getTheoryByIdSession();
  }

  // lấy title của session
  getSessionByID() {
    this.activeroute.params.subscribe((param) => {
      return this.sessionServer.getSessionById(param.id).subscribe((data) => (this.session = data));
    });
  }

  getQuizByIdSession() {
    this.activeroute.params.subscribe((param) => {
      this.quizzService.getQuizByIdSession(param.id).subscribe((data) => {
        this.quiz = data;
      });
    });
  }

  getExerciseByIdSession() {
    this.activeroute.params.subscribe((param) => {
      this.quizzService.getExerciseByIdSession(param.id).subscribe((data) => {
        this.exe = data;
      });
    });
  }

  getTheoryByIdSession() {
    this.activeroute.params.subscribe((param) => {
      this.quizzService.getTheoryByIdSession(param.id).subscribe((data) => {
        this.theory = data;
      });
    });
  }
}
