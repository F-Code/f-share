import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Course } from '../../../../model/Course';
import { Sessions } from '../../../../model/Sessions';
import { CourseService } from '../../../../service/Course.service';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.less'],
})
export class SessionComponent implements OnInit {
  session: Sessions = new (class implements Sessions {
    title: string;
    content: string;
    id: string;
    idCourse: string;
  })();
  cour: Course = new (class implements Course {
    desscription: string;
    id: string;
    name: string;
    status: number;
    urlLink: string;
  })();

  sess: Sessions[];
  courses: Course[];

  constructor(private router: Router,private courseservice: CourseService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.getSessionByIdCourse();
      this.getCourseById();
    }
  }

  // lay thong tin cua bai hoc theo id cua khoa hoc
  getSessionByIdCourse() {
    this.activatedRoute.params.subscribe((param) => {
      this.courseservice.getSessionByIdCourse(param.idCourse).subscribe((data) => {
        this.sess = data;
        window.localStorage.setItem('session', JSON.stringify(data));
      });
    });
  }

  // lay thong tin cua khoa hoc theo id
  getCourseById() {
    this.activatedRoute.params.subscribe((param) => {
      this.courseservice.getCourseByID(param.idCourse).subscribe((data) => {
        this.courses = data;
      });
    });
  }
}
