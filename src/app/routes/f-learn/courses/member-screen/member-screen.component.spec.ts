import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberScreenComponent } from './member-screen.component';

describe('MemberScreenComponent', () => {
  let component: MemberScreenComponent;
  let fixture: ComponentFixture<MemberScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
