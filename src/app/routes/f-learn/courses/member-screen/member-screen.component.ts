import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { bootloader } from '@angularclass/hmr';
import { AnswersEvaluation } from '../../../../model/AnswersEvaluation';
import { Course } from '../../../../model/Course';
import { QuestionEvaluation } from '../../../../model/QuestionEvaluation';
import { CourseService } from '../../../../service/Course.service';
import { QuizzService } from '../../../../service/Quizz.service';

@Component({
  selector: 'app-edit',
  templateUrl: './member-screen.component.html',
  styleUrls: ['./member-screen.component.less'],
})
export class MemberScreenComponent implements OnInit {
  constructor(
    private courseService: CourseService,
    private quizzService: QuizzService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }
  courses: Course[];
  answer = [];
  answersEvaluation: AnswersEvaluation = new (class
    implements AnswersEvaluation {
    answerEvaluation: string;
    id: string;
    idEvaluation: string;
    idMember: string;
    idQuestionEvaluation: string;
    status: number;
    timesPlayed: number;
  })();
  questionsEvaluation: QuestionEvaluation;

  public anrrayAnswer = [];
  public idQuestions = [];
  private user: any;
  private jsonString: string;
  private diem = 0;
  private point;
  private idEvaluation;
  // isVisible1 = false;
  isVisible2: boolean;

  isVisible1 = false;
  isVisibleTest: boolean;

  ngOnInit(): void {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      localStorage.setItem('showmodal', '1');
    } else {
      this.jsonString = localStorage.getItem('information');
      this.user = JSON.parse(this.jsonString);
      this.getCourses();
      this.showModal1();
      // this.showModal();
    }
  }

  showModal1(): void {
    const check = localStorage.getItem('showmodal');
    if (check === '1') {
      this.isVisible1 = false;
    } else {
      this.isVisible1 = true;
    }
  }
  // không xem tab con nữa
  untilmodal() {
    localStorage.setItem('showmodal', '1');
    this.isVisible1 = false;
  }
  cancel() {
    this.isVisible1 = false;
  }

  // lay danh sach khoa hoc
  getCourses() {
    this.courseService.getCourses().subscribe((data) => (this.courses = data));
  }

  // modal
  handleOk1(): void {
    this.isVisible1 = false;
    this.showModal2();
  }
  handleCancel1(): void {
    this.isVisible1 = false;
  }
  showModal2(): void {
    this.isVisible2 = true;
    this.getEvaluateQuestion();
  }
  handleOkTest(): void {
    this.isVisibleTest = false;
  }

  onItemChange(answer, id) {
    const copyObject = {
      idMember: '',
      idEvaluation: '',
      idQuestionEvaluation: '',
      answerEvaluation: '',
    };
    const se = 60,
      mi = 14;

    copyObject.answerEvaluation = answer;
    copyObject.idQuestionEvaluation = id;
    copyObject.idMember = this.user.id;
    copyObject.idEvaluation = this.idEvaluation;
    // copyObject.durationQuiz =
    //   (mi - parseInt(document.getElementById('minute').innerText)).toString() +
    //   ':' +
    //   (se - parseInt(document.getElementById('second').innerText)).toString();
    this.anrrayAnswer.push(copyObject);
    this.idQuestions.push(copyObject.idQuestionEvaluation);
    this.anrrayAnswer = this.anrrayAnswer.map(function (item) {
      if (item.idQuestionEvaluation === copyObject.idQuestionEvaluation) {
        item.answerValuation = copyObject.answerEvaluation;
      }
      return item;
    });
  }
  // getQuestionsByIdQuiz
  getEvaluateQuestion() {
    this.quizzService.getEvaluate().subscribe((data) => {
      this.idEvaluation = data[0].id;
      this.quizzService.getEvaluateQuestion(data[0].id).subscribe((src) => {
        this.questionsEvaluation = src;
        this.timeAnswer();
      });
    });
  }
  postAnswer() {
    const listOfTags = this.anrrayAnswer,
      keys = ['idQuestionEvaluation', 'answerValuation'],
      filtered = listOfTags.filter(
        ((s) => (o) =>
          ((k) => !s.has(k) && s.add(k))(keys.map((k) => o[k]).join('|')))(
            new Set()
          )
      );
    this.quizzService
      .postAnswerQuestionEvaluation(filtered)
      .subscribe((data) => {
        this.getPoint();
      });
    document.getElementById('nopBai').style.display === 'none';
    this.isVisible2 = false;
  }

  cland(){
    this.isVisible2 = false;
  }
  // tinh diem
  getPoint() {
    this.quizzService.getEvaluate().subscribe((src) => {
      this.quizzService
        .getPointQuestionEvaluation(this.user.id, src[0].id)
        .subscribe((data) => {
          this.point = data;
          for (
            let i = this.point.length - this.anrrayAnswer.length;
            i < this.point.length;
            i++
          ) {
            if (this.point[i].status === 1) {
              this.diem = this.diem + 1;
              console.log('a',this.diem);
            }
          }
          if (this.diem < 10) {
            const a = 'Bạn đang ở mức yếu bạn cần cố gắng nhiều hơn nữa';
            document.getElementById('loiNhac').innerHTML = a;
            localStorage.setItem('levelcoureaffi', JSON.stringify("Học thử"));
          } else if (this.diem >= 10 && this.diem <= 14) {
            const b = 'Bạn đang ở mức trung bình cần cố gắng hơn để cải thiện kiến thức của mình';
            document.getElementById('loiNhac').innerHTML = b;
            localStorage.setItem('levelcoureaffi', JSON.stringify("Cơ bản"));
          } else if (this.diem > 14 && this.diem <= 18) {
            const c =
              'Bạn giỏi thật đấy nhưng vẫn cần cố gắng hơn để có kết quả cao hơn nhé';
            document.getElementById('loiNhac').innerHTML = c;
            localStorage.setItem('levelcoureaffi', JSON.stringify("Nâng cao"));
          } else if (this.diem > 18) {
            const d =
              'Bạn là người làm bài rất xuất sắc. Hãy giữ vững phong độ này nhé';
            document.getElementById('loiNhac').innerHTML = d;
            localStorage.setItem('levelcoureaffi', JSON.stringify("Nâng cao"));
          }
        });
    });
    this.isVisibleTest = true;
  }

  // tinh thoi gian
  timeAnswer() {
    let se = 60,
      mi = 15;

    function getTimeRemaining(endtime) {
      const t = Date.parse(endtime) - Date.now();
      const seconds = Math.floor((t / 1000) % se);
      const minutes = Math.floor((t / 1000 / 60) % mi);

      return {
        total: t,
        minutes,
        seconds,
      };
    }

    function initializeClock(id, endtime) {
      const clock = document.getElementById(id);
      const minutesSpan = clock.querySelector('.minutes');
      const secondsSpan = clock.querySelector('.seconds');

      function stopTime() {
        if ((document.getElementById('nopBai').style.display = null)) {
          return true;
        }
      }

      function updateClock() {
        const t = getTimeRemaining(endtime);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
        if (t.minutes === 0 && t.seconds === 0) {
          dungThoiGia();
          document.getElementById('nopBai').click();
          // stopTime();
        } else if (
          document.getElementById('modalTest').style.display === 'block'
        ) {
          se = 0;
          mi = 0;
          dungThoiGia();
          // stopTime();
        }
      }

      function dungThoiGia() {
        clearInterval(timeinterval);
      }
      updateClock();
      const timeinterval = setInterval(updateClock, 1000);
    }
    const deadline = new Date(Date.parse(Date()) + 15 * 24 * 60 * 60 * 1000);
    initializeClock('clockdiv', deadline);
  }

  getPointQuestionEvaluation() {
    this.quizzService.getEvaluate().subscribe((data) => {
      this.quizzService
        .getPointQuestionEvaluation(this.user.id, data[0].id)
        .subscribe((data2) => {
          for (let i = 0; i < data2.length; i++) {
            this.quizzService
              .deleteAnswerQuestionEvaluation(data2[i].id)
              .subscribe((dataDelete) => {});
          }
        });
    });
  }
}
