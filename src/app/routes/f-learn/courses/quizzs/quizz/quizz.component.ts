import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Answer } from '../../../../../model/Answer';
import { Course } from '../../../../../model/Course';
import { Question } from '../../../../../model/Question';
import { QuizzService } from '../../../../../service/Quizz.service';

@Component({
  selector: 'app-quizz',
  templateUrl: './quizz.component.html',
  styleUrls: ['./quizz.component.less'],
})
export class QuizzComponent implements OnInit {
  ans: Answer = new (class implements Answer {
    durationQuiz: string;
    idQuiz: string;
    answerQuiz: string;
    id: string;
    idMember: string;
    idQuestion: string;
    status: number;
    timesPlayed: number;
  })();
  questions: Question;

  coures: Course = new (class implements Course {
    desscription: string;
    id: string;
    name: string;
    status: number;
    urlLink: string;
  })();

  public anrrayAnswer = [];
  public idQuestions = [];
  private user: any;
  private jsonString: string;
  private diem = 0;
  private point;
  private idQuiz;
  isVisible = false;
  private session: any;
  private routerSession;

  constructor(
    private quizzService: QuizzService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private notification: NzNotificationService
  ) { }

  ngOnInit(): void {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.jsonString = localStorage.getItem('information');
      this.user = JSON.parse(this.jsonString);

      this.activatedRoute.params.subscribe((param) => {
        this.idQuiz = param.idQuiz;
        this.session = param.idSession;

        this.routerSession =
          '/f-learn.poly/session/' +
          param.idSession +
          '/' +
          param.idSession +
          '/theorys';
      });
    }
  }

  onItemChange(answer, id) {
    const copyObject = {
      idMember: '',
      idQuiz: '',
      idQuestion: '',
      answerQuiz: '',
      durationQuiz: '',
    };
    var se = 60,
      mi = 14;

    copyObject.answerQuiz = answer;
    copyObject.idQuestion = id;
    copyObject.idMember = this.user.id;
    copyObject.idQuiz = this.idQuiz;
    copyObject.durationQuiz =
      (mi - parseInt(document.getElementById('minute').innerText)).toString() +
      ':' +
      (se - parseInt(document.getElementById('second').innerText)).toString();
    this.anrrayAnswer.push(copyObject);

    this.idQuestions.push(copyObject.idQuestion);
    this.anrrayAnswer = this.anrrayAnswer.map(function (item) {
      if (item.idQuestion === copyObject.idQuestion) {
        item.answerQuiz = copyObject.answerQuiz;
      }
      return item;
    });
  }

  // getQuestionsByIdQuiz
  getQuestionsByIdQuiz() {
    this.activatedRoute.params.subscribe((param) => {
      this.quizzService.getQuestionByIdQuiz(param.idQuiz).subscribe((data) => {
        this.questions = data;
      });
    });
  }

  showModal(): void {
    this.isVisible = true;
  }
  // modal
  handleOk(): void {
    this.isVisible = false;
    this.router.navigate([this.routerSession]);
    document.getElementById('sessionDetail').style.display = 'block';
  }

  // tinh diem
  // tinhDiem() {
  //   this.jsonString = localStorage.getItem('point');
  //   this.point = JSON.parse(this.jsonString);
  //   for (let i = this.point.length - this.anrrayAnswer.length; i < this.point.length; i++) {
  //     if (this.point[i].status === 1) {
  //       this.diem = this.diem + 1;
  //     }
  //   }
  //   if (this.diem < 5) {
  //     var a = 'Ban dang o muc yeu va can co gang nhieu hon nua';
  //     document.getElementById('loiNhac').innerHTML = a;
  //   } else if (this.diem >= 5 && this.diem <= 7) {
  //     var b = 'Ban dang o muc trung binh can co gang nhieu hon nua';
  //     document.getElementById('loiNhac').innerHTML = b;
  //   } else if (this.diem > 7 && this.diem <= 9) {
  //     var c = 'Ban gioi that day nhung van can co gang nhieu hon nua nha';
  //     document.getElementById('loiNhac').innerHTML = c;
  //   } else if ((this.diem = 10)) {
  //     var d = 'Ban la nguoi hoc bai xuat sac nhat ma toi tung thay! Hay giu vung phong do trong nhung bai hoc tiep theo nhe';
  //     document.getElementById('loiNhac').innerHTML = d;
  //   }
  // }

  // lay status
  getPoint() {
    this.activatedRoute.params.subscribe((param) => {
      this.quizzService
        .getAnswerByIdMemberAndIdQuiz(this.user.id, param.idQuiz)
        .subscribe((data) => {
          this.point = data;
          // window.localStorage.setItem('point', JSON.stringify(data));
          for (
            let i = this.point.length - this.anrrayAnswer.length;
            i < this.point.length;
            i++
          ) {
            if (this.point[i].status === 1) {
              this.diem = this.diem + 1;
            }
          }
          if (this.diem < 5) {
            const a = 'Bạn đang ở mức yếu bạn cần cố gắng nhiều hơn nữa';
            document.getElementById('loiNhac').innerHTML = a;
          } else if (this.diem >= 5 && this.diem <= 7) {
            const b =
              'Bạn đang ở mức trung bình cần cố gắng hơn để cải thiện kiến thức của mình';
            document.getElementById('loiNhac').innerHTML = b;
          } else if (this.diem > 7 && this.diem <= 9) {
            const c =
              'Bạn giỏi thật đấy nhưng vẫn cần cố gắng hơn để có kết quả cao hơn nhé';
            document.getElementById('loiNhac').innerHTML = c;
          } else if (this.diem === 10) {
            const d =
              'Bạn là người làm bài rất xuất sắc. Hãy giữ vững phong độ này nhé';
            document.getElementById('loiNhac').innerHTML = d;
          }
        });
    });

    document.getElementById('clockdiv').style.display = 'none';
    document.getElementById('baiTap').style.display = 'none';

    this.showModal();
  }

  postAnswer() {
    const listOfTags = this.anrrayAnswer,
      keys = ['idQuestion', 'answerQuiz'],
      filtered = listOfTags.filter(
        ((s) => (o) =>
          ((k) => !s.has(k) && s.add(k))(keys.map((k) => o[k]).join('|')))(
            new Set()
          )
      );
    if (filtered.length === 0) {
      this.notification.create(
        'info',
        'Vui lòng chọn câu trả lời!',
        'Hãy chọn câu trả lời đúng nhất cho các câu hỏi bên dưới!'
      );
    } else if (filtered.length === 1) {
      this.notification.create(
        'warning',
        'Vui lòng chọn câu trả lời!',
        'Hãy chọn cố gắng hoàn thành bài tập để đạt được kết quả cao nhất!'
      );
    }

    this.quizzService.postAnswer(filtered).subscribe((data) => {
      this.getPoint();
    });
    this.putStatusCourse();
  }

  // tinh thoi gian
  timeAnswer() {
    this.getQuestionsByIdQuiz();
    document.getElementById('start').style.display = 'none';
    document.getElementById('baiTap').style.display = 'block';
    document.getElementById('nopBai').style.display = 'block';
    document.getElementById('sessionDetail').style.display = 'none';
    document.getElementById('contentLayout').style.marginLeft = '300px';

    var se = 60,
      mi = 15;

    function getTimeRemaining(endtime) {
      var t = Date.parse(endtime) - Date.now();
      var seconds = Math.floor((t / 1000) % se);
      var minutes = Math.floor((t / 1000 / 60) % mi);

      return {
        total: t,
        minutes: minutes,
        seconds: seconds,
      };
    }

    function initializeClock(id, endtime) {
      var clock = document.getElementById(id);
      var minutesSpan = clock.querySelector('.minutes');
      var secondsSpan = clock.querySelector('.seconds');

      function stopTime() {
        document.getElementById('contentLayout').style.marginLeft = '0px';
        document.getElementById('clockdiv').style.display = 'none';
        document.getElementById('nopBai').style.display = 'none';
        document.getElementById('diem').style.display = 'block';
        document.getElementById('sessionDetail').style.display = 'block';
      }

      function updateClock() {
        var t = getTimeRemaining(endtime);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
        if (t.minutes == 0 && t.seconds == 0) {
          dungThoiGia();
          document.getElementById('nopBai').click();
          stopTime();
        } else if (document.getElementById('baiTap').style.display == 'none') {
          dungThoiGia();
          stopTime();
        }
      }

      function dungThoiGia() {
        clearInterval(timeinterval);
      }
      updateClock();
      var timeinterval = setInterval(updateClock, 1000);
    }
    var deadline = new Date(Date.parse(Date()) + 15 * 24 * 60 * 60 * 1000);
    initializeClock('clockdiv', deadline);
  }

  // sua trang thai khoa học
  putStatusCourse() {
    this.activatedRoute.params.subscribe((param) => {
      this.quizzService.getSessionById(param.idSession).subscribe((data) => {
        this.quizzService.getCourseById(data.idCourse).subscribe((data2) => {
          this.coures = data2;
          this.coures.status = 1;
          this.quizzService
            .putStatusCourse(this.coures)
            .subscribe((data3) => { });
        });
      });
    });
  }
}
