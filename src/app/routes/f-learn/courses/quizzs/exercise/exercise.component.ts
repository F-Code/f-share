import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Course } from '../../../../../model/Course';
import { CourseAnswer } from '../../../../../model/CourseAnswer';
import { QuestionExervise } from '../../../../../model/QuestionExervise';
import { QuizzService } from '../../../../../service/Quizz.service';
import { HistoryExercise } from '../../../../../model/HistoryExercise';

@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.less'],
})
export class ExerciseComponent implements OnInit {
  quesExercise: QuestionExervise;
  answerQuestionExercise: CourseAnswer = new (class implements CourseAnswer {
    idExercise: string;
    answerExercise: string;
    id: string;
    idMember: string;
    idQuestionExercise: string;
    status: number;
    timePlayed: string;
    durationExercise: string;
  })();

  coures: Course = new (class implements Course {
    desscription: string;
    id: string;
    name: string;
    status: number;
    urlLink: string;
  })();

  private jsonString;
  private user;
  private anrrayAnswer = [];
  private idExercises = [];
  private diem = 0;
  private point;
  private idexe;
  isVisible = false;
  private session: any;
  private routerSession;

  constructor(
    protected quizzService: QuizzService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private notification: NzNotificationService
  ) {}

  ngOnInit(): void {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.jsonString = localStorage.getItem('information');
      this.user = JSON.parse(this.jsonString);

      this.activatedRoute.params.subscribe((param) => {
        this.idexe = param.idexe;
        this.session = param.idSession;

        this.routerSession =
          '/f-learn.poly/session/' +
          param.idSession +
          '/' +
          param.idSession +
          '/theorys';
      });
    }
  }

  gettext(value, id) {
    var se = 60,
      mi = 14;

    const copyObject = {
      idMember: '',
      idExercise: '',
      idQuestionExercise: '',
      answerQuestionExercise: '',
      durationExercise: '',
    };
    copyObject.answerQuestionExercise = value.toString().trim();
    copyObject.idQuestionExercise = id;
    copyObject.idMember = this.user.id;
    copyObject.idExercise = this.idexe;
    copyObject.durationExercise =
      (mi - parseInt(document.getElementById('minute').innerText)).toString() +
      ':' +
      (se - parseInt(document.getElementById('second').innerText)).toString();
    this.anrrayAnswer.push(copyObject);

    this.idExercises.push(copyObject.idQuestionExercise);

    this.anrrayAnswer = this.anrrayAnswer.map(function (item) {
      if (item.idQuestionExercise === copyObject.idQuestionExercise) {
        item.answerExercise = copyObject.answerQuestionExercise;
      }
      return item;
    });
  }

  // getQuestionExerciseByIdExercise
  getQuestionExerciseByIdExercise() {
    this.activatedRoute.params.subscribe((param) => {
      this.quizzService
        .getQuestionExerciseByIdExercise(param.idexe)
        .subscribe((data) => {
          this.quesExercise = data;
        });
    });
  }

  // post AnswerQuestionExercise
  postAnswerQuestionExercise() {
    const listOfTags = this.anrrayAnswer,
      keys = ['idQuestionExercise', 'answerExercise'],
      filtered = listOfTags.filter(
        ((s) => (o) =>
          ((k) => !s.has(k) && s.add(k))(keys.map((k) => o[k]).join('|')))(
          new Set()
        )
      );
    if (filtered.length === 0) {
      this.notification.create(
        'info',
        'Vui lòng chọn câu trả lời!',
        'Hãy chọn câu trả lời đúng nhất cho các câu hỏi bên dưới!'
      );
    } else if (filtered.length === 1) {
      this.notification.create(
        'warning',
        'Vui lòng chọn câu trả lời!',
        'Hãy chọn cố gắng hoàn thành bài tập để đạt được kết quả cao nhất!'
      );
    }
    this.quizzService.postAnswerQuestionExercise(filtered).subscribe((data) => {
      this.getPoint();
    });
    this.putStatusCourse();
  }

  // lay status
  getPoint() {
    this.activatedRoute.params.subscribe((param) => {
      this.quizzService
        .getAnswerExerciseByIdMemberAndIdExercise(this.user.id, param.idexe)
        .subscribe((data) => {
          window.localStorage.setItem('point', JSON.stringify(data));
          this.tinhDiem();
        });
    });

    document.getElementById('clockdiv').style.display = 'none';
    document.getElementById('baiTap').style.display = 'none';

    this.showModal();
  }

  showModal(): void {
    this.isVisible = true;
  }
  // modal
  handleOk(): void {
    this.isVisible = false;
    this.router.navigate([this.routerSession]);
    document.getElementById('sessionDetail').style.display = 'block';
  }

  // tinh diem
  tinhDiem() {
    this.jsonString = localStorage.getItem('point');
    this.point = JSON.parse(this.jsonString);
    for (
      let i = this.point.length - this.anrrayAnswer.length;
      i < this.point.length;
      i++
    ) {
      if (this.point[i].status == 1) {
        this.diem = this.diem + 1;
      }
    }
    if (this.diem < 5) {
      const a = 'Bạn đang ở mức yếu bạn cần cố gắng nhiều hơn nữa';
      document.getElementById('loiNhac').innerHTML = a;
    } else if (this.diem >= 5 && this.diem <= 7) {
      const b =
        'Bạn đang ở mức trung bình cần cố gắng hơn để cải thiện kiến thức của mình';
      document.getElementById('loiNhac').innerHTML = b;
    } else if (this.diem > 7 && this.diem <= 9) {
      const c =
        'Bạn giỏi thật đấy nhưng vẫn cần cố gắng hơn để có kết quả cao hơn nhé';
      document.getElementById('loiNhac').innerHTML = c;
    } else if (this.diem === 10) {
      const d =
        'Bạn là người làm bài rất xuất sắc. Hãy giữ vững phong độ này nhé';
      document.getElementById('loiNhac').innerHTML = d;
    }
  }

  // tinh thoi gian
  timeAnswer() {
    this.getQuestionExerciseByIdExercise();
    document.getElementById('start').style.display = 'none';
    document.getElementById('baiTap').style.display = 'block';
    document.getElementById('nopBai').style.display = 'block';
    document.getElementById('sessionDetail').style.display = 'none';
    document.getElementById('contentLayout').style.marginLeft = '300px';

    var se = 60,
      mi = 15;

    function getTimeRemaining(endtime) {
      var t = Date.parse(endtime) - Date.now();
      var seconds = Math.floor((t / 1000) % se);
      var minutes = Math.floor((t / 1000 / 60) % mi);

      return {
        total: t,
        minutes: minutes,
        seconds: seconds,
      };
    }

    function initializeClock(id, endtime) {
      var clock = document.getElementById(id);
      var minutesSpan = clock.querySelector('.minutes');
      var secondsSpan = clock.querySelector('.seconds');

      function stopTime() {
        document.getElementById('contentLayout').style.marginLeft = '0px';
        document.getElementById('clockdiv').style.display = 'none';
        document.getElementById('nopBai').style.display = 'none';
        document.getElementById('diem').style.display = 'block';
        document.getElementById('baiTap').style.display = 'none';
      }

      function updateClock() {
        var t = getTimeRemaining(endtime);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
        if (t.minutes == 0 && t.seconds == 0) {
          dungThoiGia();
          document.getElementById('nopBai').click();
          stopTime();
        } else if (document.getElementById('baiTap').style.display == 'none') {
          dungThoiGia();
          stopTime();
        }
      }

      function dungThoiGia() {
        clearInterval(timeinterval);
      }
      updateClock();
      var timeinterval = setInterval(updateClock, 1000);
    }
    var deadline = new Date(Date.parse(Date()) + 15 * 24 * 60 * 60 * 1000);
    initializeClock('clockdiv', deadline);
  }

  // sua trang thai khoa học
  putStatusCourse() {
    this.activatedRoute.params.subscribe((param) => {
      this.quizzService.getSessionById(param.idSession).subscribe((data) => {
        this.quizzService.getCourseById(data.idCourse).subscribe((data2) => {
          this.coures = data2;
          this.coures.status = 1;
          this.quizzService
            .putStatusCourse(this.coures)
            .subscribe((data3) => {});
        });
      });
    });
  }
}
