import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Theory } from '../../../../../model/Theory';
import { QuizzService } from '../../../../../service/Quizz.service';

@Component({
  selector: 'app-theory',
  templateUrl: './theory.component.html',
  styleUrls: ['./theory.component.less'],
})
export class TheoryComponent implements OnInit {
  theory: Theory;

  page: 5;

  constructor(
    private quizzService: QuizzService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }
  ngOnInit(): void {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.getTheoryByIdSession();
    }
  }

  getTheoryByIdSession() {
    this.activatedRoute.params.subscribe((param) => {
      this.quizzService
        .getTheoryByIdSession(param.idSession)
        .subscribe((data) => {
          this.theory = data;
        });
    });
  }
}
