import { Component, OnInit } from '@angular/core';
import { QuizzService } from '../../../../service/Quizz.service';
import { HistoryQuiz } from '../../../../model/HistoryQuiz';
import { HistoryExercise } from '../../../../model/HistoryExercise';
import { lt } from 'date-fns/locale';

@Component({
  selector: 'app-rank',
  templateUrl: './rank.component.html',
  styleUrls: ['./rank.component.less'],
})
export class RankComponent implements OnInit {
  historyquiz: HistoryQuiz[];
  historyExericse: HistoryExercise[];

  private member = [];
  private jsonStringMember;
  private user;
  private jsonString: string;
  public tongDiemQuiz = 0;
  public tongDiemExecise = 0;
  public tongDiem = 0;

  private quiz = [];
  private exercise = [];
  private arrayMember = [];

  constructor(private quizzService: QuizzService) { }

  ngOnInit(): void {
    this.jsonString = localStorage.getItem('information');
    this.user = JSON.parse(this.jsonString);

    this.jsonStringMember = localStorage.getItem('members');
    this.member = JSON.parse(this.jsonStringMember);

    this.getPointByIDMemberQuiz();
  }

  // lay diem theo id member
  getPointByIDMemberQuiz() {
    for (let j = 0; j < this.member.length; j++) {
      this.quizzService.getPointHistoryQuiz(this.member[j].id).subscribe((data) => {
        this.historyquiz = data;
        var pointLast = this.historyquiz.length;
        if (pointLast == 0) {
          this.tongDiemQuiz = 0;
          this.quiz.push(this.tongDiemQuiz);
        } else {
          this.tongDiemQuiz = this.historyquiz[pointLast - 1].point;
          this.quiz.push(this.tongDiemQuiz);
        }
      });

      // hàm sắp xếp động
      function compareValues(key, order = 'asc') {
        return function (a, b) {
          if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            // nếu không tồn tại
            return 0;
          }

          const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
          const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

          let comparison = 0;
          if (varA < varB) {
            comparison = 1;
          } else if (varA > varB) {
            comparison = -1;
          }
          return order == 'desc' ? comparison * -1 : comparison;
        };
      }

      // so
      function sanitise(x) {
        if (isNaN(x)) {
          return NaN;
        }
        return x;
      }
      // for (let j = 0; j < this.member.length; j++) {
      this.quizzService.getPointHistoryExercise(this.member[j].id).subscribe((data) => {
        this.historyExericse = data;
        var pointLast = this.historyExericse.length;
        if (pointLast - 1 < 0) {
          this.tongDiemExecise = 0;
          this.exercise.push(this.tongDiemExecise);
        } else {
          this.tongDiemExecise = this.historyExericse[pointLast - 1].point;
          this.exercise.push(this.tongDiemExecise);
        }

        let objectTongDiem = {
          nameMember: '',
          avatarMember: '',
          sumPoint: 0,
        };

        if (this.quiz[j] == null) {
          this.quiz[j] = 0;
        }
        if (this.exercise[j] == null) {
          this.exercise[j] = 0;
        }

        this.tongDiem = this.quiz[j] + this.exercise[j];
        
        objectTongDiem.sumPoint = this.tongDiem;
        objectTongDiem.avatarMember = this.member[j].avatar;
        objectTongDiem.nameMember = this.member[j].familyName;
        this.arrayMember.push(objectTongDiem);
        this.arrayMember.sort(compareValues('sumPoint', 'asc'));
      });
    }
  }
}
