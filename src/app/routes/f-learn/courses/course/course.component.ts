import { Component, OnInit, TemplateRef } from '@angular/core';
import { Course } from '../../../../model/Course';
import { CourseService } from '../../../../service/Course.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Router } from '@angular/router';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.less'],
})
export class CourseComponent implements OnInit {
  courses: Course[];
  searchText;
  page: 5;

  constructor(
    private notification: NzNotificationService,
    private router: Router,
    private courseservice: CourseService,
    ) {}

  ngOnInit(): void {
    this.getCourses();
  }
  getCourses() {
    this.courseservice.getCourses().subscribe((data) => {
      this.courses = data;
    });
  }
  checkaccount(template: TemplateRef<{}>, id){
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.notification.template(template);
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.router.navigate(['/f-learn.poly/'+ id +'/sessions']);
    }
  }
}
