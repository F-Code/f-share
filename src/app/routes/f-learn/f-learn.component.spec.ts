import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FLearnComponent } from './f-learn.component';

describe('FLearnComponent', () => {
  let component: FLearnComponent;
  let fixture: ComponentFixture<FLearnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FLearnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FLearnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
