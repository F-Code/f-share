import { Component, OnInit, TemplateRef } from '@angular/core';
import { Discussion } from '../../../../model/Discussion';
import { DiscussionService } from '../../../../service/Discussion.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Router } from '@angular/router';

@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.less'],
})
export class DiscussionComponent implements OnInit {
  discussions: Discussion[];
  discussion: Discussion;
  searchText;
  page: 5;

  constructor(
    private notification: NzNotificationService,
    private discussionService: DiscussionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getDiscussions();
  }
  getDiscussions() {
    this.discussionService.getDiscussion().subscribe((data) => {
      this.discussions = data;
      // hàm sắp xếp động
      function compareValues(key, order = 'asc') {
        return function (a, b) {
          if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            // nếu không tồn tại
            return 0;
          }

          const varA =
            typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
          const varB =
            typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

          let comparison = 0;
          if (varA < varB) {
            comparison = 1;
          } else if (varA > varB) {
            comparison = -1;
          }
          return order == 'desc' ? comparison * -1 : comparison;
        };
      }
      this.discussions.sort(compareValues('view', 'asc'));
    });
  }

  // sua luot xem blog
  putDiscussion(id) {
    return this.discussionService.getDiscussionByID(id).subscribe((data) => {
      (this.discussion = data),
        (this.discussion.view = this.discussion.view + 1);
      this.updateView();
    });
  }

  // update view
  updateView() {
    this.discussionService.putViewBlog(this.discussion).subscribe((data) => {});
  }
  checkaccount(template: TemplateRef<{}>) {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.notification.template(template);
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.router.navigate(['/f-learn.poly/discussion/post']);
    }
  }
}
