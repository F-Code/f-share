import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Discussion } from '../../../../model/Discussion';
import { DiscussionService } from '../../../../service/Discussion.service';

@Component({
  selector: 'app-discussion-post',
  templateUrl: './discussion-post.component.html',
  styleUrls: ['./discussion-post.component.less'],
})
export class DiscussionPostComponent implements OnInit {
  dis: Discussion = new (class implements Discussion {
    view: number;
    emailReviewName: string;
    avatar: string;
    reviewName: string;
    content: string;
    id: string;
    title: string;
  })();

  tinymceInit: any;
  private tinymce: any;
  private jsonString;
  private discuss;

  validateForm: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private disService: DiscussionService,
    private router: Router,
    private fb: FormBuilder,
  ) {
    this.tinymceInit = {
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern',
      ],
      toolbar:
        'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
      image_advtab: true,
      file_picker_callback(cb, value, meta) {
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

        // Note: In modern browsers input[type='file'] is functional without
        // even adding it to the DOM, but that might not be the case in some older
        // or quirky browsers like IE, so you might want to add it to the DOM
        // just in case, and visually hide it. And do not forget do remove it
        // once you do not need it anymore.

        input.onchange = () => {
          const file = input.files[0];

          const reader = new FileReader();
          reader.onload = () => {
            // Note: Now we need to register the blob in TinyMCEs image blob
            // registry. In the next release this part hopefully won't be
            // necessary, as we are looking to handle it internally.
            const id = 'blobid' + new Date().getTime();
            const blobCache = this.tinymce.activeEditor.editorUpload.blobCache;
            let base64 = '';
            if (typeof reader.result === 'string') {
              base64 = reader.result.split(',')[1];
            }
            const blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            // call the callback and populate the Title field with the file name
            cb(blobInfo.blobUri(), { title: file.name });
          };
          reader.readAsDataURL(file);
        };
        input.click();
      },
    };
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      title: [null, [Validators.required]],
      content: [null, [Validators.required]],
    });
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
    this.router.navigate(['/f-learn.poly/login']);
    } else {
      // luu thong tin nguoi dang
      this.jsonString = localStorage.getItem('information');
      this.discuss = JSON.parse(this.jsonString);
      this.activatedRoute.data.subscribe((data) => {
        data = this.dis;
      });
    }
  }

  // tạo discussion
  async DisPost() {
    this.dis.avatar = this.discuss.avatar;
    this.dis.reviewName = this.discuss.familyName;
    this.dis.emailReviewName = this.discuss.emailAddress;

    this.disService.postDiscussion(this.dis).subscribe((data) => {
      this.router.navigate(['/f-learn.poly/discussion']);
    });
  }
}
