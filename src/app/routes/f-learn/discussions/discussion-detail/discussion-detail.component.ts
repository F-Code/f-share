import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Discussion } from '../../../../model/Discussion';
import { Member } from '../../../../model/Member';
import { DiscussionService } from '../../../../service/Discussion.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-discussion-detail',
  templateUrl: './discussion-detail.component.html',
  styleUrls: ['./discussion-detail.component.less'],
})
export class DiscussionDetailComponent implements OnInit {
  users: Member[];
  dis: Discussion;

  constructor(private discussionService: DiscussionService, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.users = JSON.parse(window.localStorage.getItem('information'));
    this.showModal();
  }

  showModal() {
    this.activatedRoute.params.subscribe((param) => {
      this.discussionService.getDiscussionByID(param.id).subscribe((data) => {
        this.dis = data;
      });
    });
  }
}
