import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDistance } from 'date-fns';
import { NzMessageService } from 'ng-zorro-antd/message';
import { DiscussionComment } from '../../../../model/Discussion-Comment';
import { Like } from '../../../../model/Like';
import { Member } from '../../../../model/Member';
import { DiscussionService } from '../../../../service/Discussion.service';
import { QuizzService } from '../../../../service/Quizz.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-discussion-comment',
  templateUrl: './discussion-comment.component.html',
  styleUrls: ['./discussion-comment.component.less'],
})
export class DiscussionCommentComponent implements OnInit {
  constructor(
    private discussionService: DiscussionService,
    private activatedRoute: ActivatedRoute,
    private notification: NzNotificationService,
    private fb: FormBuilder,
    private quizzService: QuizzService,
    private router: Router,
    private nzMessageService: NzMessageService
  ) {
    this.tinymceInit = {
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern',
      ],
      toolbar:
        'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
      image_advtab: true,
      file_picker_callback(cb, value, meta) {
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

        // Note: In modern browsers input[type='file'] is functional without
        // even adding it to the DOM, but that might not be the case in some older
        // or quirky browsers like IE, so you might want to add it to the DOM
        // just in case, and visually hide it. And do not forget do remove it
        // once you do not need it anymore.

        input.onchange = () => {
          const file = input.files[0];

          const reader = new FileReader();
          reader.onload = () => {
            // Note: Now we need to register the blob in TinyMCEs image blob
            // registry. In the next release this part hopefully won't be
            // necessary, as we are looking to handle it internally.
            const id = 'blobid' + new Date().getTime();
            const blobCache = this.tinymce.activeEditor.editorUpload.blobCache;
            let base64 = '';
            if (typeof reader.result === 'string') {
              base64 = reader.result.split(',')[1];
            }
            const blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            // call the callback and populate the Title field with the file name
            cb(blobInfo.blobUri(), { title: file.name });
          };
          reader.readAsDataURL(file);
        };
        input.click();
      },
    };
  }
  validateForm: FormGroup;
  disCommet: DiscussionComment = new (class implements DiscussionComment {
    idMember: string;
    dateComment: string;
    avatar: string;
    content: string;
    id: string;
    idDiscuss: string;
    reviewName: string;
    like: number;
    dislike: number;
  })();
  disCommet2: DiscussionComment = new (class implements DiscussionComment {
    idMember: string;
    dateComment: string;
    avatar: string;
    content: string;
    id: string;
    idDiscuss: string;
    reviewName: string;
    like: number;
    dislike: number;
  })();

  likeAndDisLike: Like = new (class implements Like {
    dislike: number;
    id: string;
    idCommentDiscuss: string;
    idDiscuss: string;
    idMember: string;
    like: number;
  })();

  private arrLike = [];

  comment: DiscussionComment[];
  isVisible = false;
  private disLikeData2 = [];

  data: any[] = [];

  tinymceInit: any;
  private tinymce: any;
  private jsonString;
  private member;
  time = new Date();
  private idCommentDelete;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      content: [null, [Validators.required]],
    });
    this.getComment();
    this.jsonString = localStorage.getItem('information');
    this.member = JSON.parse(this.jsonString);

    this.activatedRoute.params.subscribe((param) => {
      this.disCommet.idDiscuss = param.id;
    });
  }

  // them moi comment
  postComment(template: TemplateRef<{}>) {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.notification.template(template);
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.disCommet.avatar = this.member.avatar;
      this.disCommet.reviewName = this.member.familyName;
      let time =
        this.time.getHours() +
        ':' +
        this.time.getMinutes() +
        ':' +
        this.time.getSeconds();
      let date =
        this.time.getDate() +
        '-' +
        (this.time.getMonth() + 1) +
        '-' +
        this.time.getFullYear();
      let fulltime = time + ' ' + date;
      this.disCommet.dateComment = fulltime;
      this.disCommet.like = 0;
      this.disCommet.dislike = 0;
      this.disCommet.idMember = this.member.id;

      this.activatedRoute.params.subscribe((param) => {
        this.discussionService.postComment(this.disCommet).subscribe((data) => {
          this.getComment();
          this.router.navigate([
            '/f-learn.poly/discussion/' + this.disCommet.idDiscuss,
          ]);
        });
      });
    }
  }

  // lay danh sach comment
  getComment() {
    this.activatedRoute.params.subscribe((param) => {
      this.discussionService.getComment(param.id).subscribe((data) => {
        this.comment = data;
      });
    });
  }

  like(id) {
    this.discussionService
      .getLikeByIdMemberAndIdComment(this.member.id, id)
      .subscribe((data) => {
        this.disLikeData2 = data;

        this.discussionService.getCommentByID(id).subscribe((src) => {
          this.disCommet2 = src;

          if (this.disLikeData2.length === 0) {
            this.disCommet2.like = this.disCommet2.like + 1;
            this.updateComment();
            this.getComment();
            this.postLike(id);
          } else if (
            this.disLikeData2.length === 1 &&
            this.disLikeData2[0].like === 0
          ) {
            this.disCommet2.like = this.disCommet2.like + 1;
            this.disCommet2.dislike = this.disCommet2.dislike - 1;
            this.updateComment();
            this.getComment();
            this.deleteLikeByIdMemberAndIdComment(this.disLikeData2[0].id);
            this.postLike(id);
          } else if (
            this.disLikeData2.length === 1 &&
            this.disLikeData2[0].like === 1
          ) {
            this.disCommet2.like = this.disCommet2.like - 1;
            this.updateComment();
            this.getComment();
            this.deleteLikeByIdMemberAndIdComment(this.disLikeData2[0].id);
          }
        });
      });
  }

  dislike(id) {
    const idComment = id.toString().substring(1);
    this.discussionService
      .getLikeByIdMemberAndIdComment(this.member.id, idComment)
      .subscribe((data) => {
        this.disLikeData2 = data;

        this.discussionService.getCommentByID(idComment).subscribe((src) => {
          this.disCommet2 = src;

          if (this.disLikeData2.length === 0) {
            this.disCommet2.dislike = this.disCommet2.dislike + 1;
            this.updateComment();
            this.getComment();
            this.postDisLike(idComment);
          } else if (
            this.disLikeData2.length === 1 &&
            this.disLikeData2[0].dislike === 0
          ) {
            this.disCommet2.dislike = this.disCommet2.dislike + 1;
            this.disCommet2.like = this.disCommet2.like - 1;
            this.updateComment();
            this.getComment();
            this.deleteLikeByIdMemberAndIdComment(this.disLikeData2[0].id);
            this.postDisLike(idComment);
          } else if (
            this.disLikeData2.length === 1 &&
            this.disLikeData2[0].dislike === 1
          ) {
            this.disCommet2.dislike = this.disCommet2.dislike - 1;
            this.updateComment();
            this.getComment();
            this.deleteLikeByIdMemberAndIdComment(this.disLikeData2[0].id);
          }
        });
      });
  }

  // update like
  updateComment() {
    this.discussionService.putComment(this.disCommet2).subscribe((data) => {});
  }
  // delete comment
  deleteComment() {
    this.discussionService
      .deleteComment(this.idCommentDelete)
      .subscribe((data) => {});
  }
  clickDelete(id) {
    this.idCommentDelete = id.toString().substring(1).trim();
  }

  cancel(): void {}

  confirmDelete() {
    this.discussionService
      .getCommentByID(this.idCommentDelete)
      .subscribe((src) => {
        this.disCommet2 = src;

        if (this.disCommet2.idMember === this.member.id) {
          this.deleteComment();
          this.nzMessageService.info('Xóa thành công!');
          this.getComment();
        } else {
          this.nzMessageService.info('Bạn không thể xóa comment này!');
        }
      });
  }
  confirmEdit() {
    this.discussionService
      .getCommentByID(this.idCommentDelete)
      .subscribe((data) => {
        this.disCommet2 = data;
        if (this.disCommet2.idMember === this.member.id) {
          this.isVisible = true;
        } else {
          this.isVisible = false;
          this.nzMessageService.info('Bạn không thể sửa comment này!');
        }
      });
  }

  handleOk(): void {
    this.updateComment();
    this.isVisible = false;
    this.nzMessageService.info('Sửa thành công!');
    this.getComment();
  }
  handleCancel() {
    this.isVisible = false;
  }

  deleteLikeByIdMemberAndIdComment(id) {
    this.discussionService
      .deleteLikeByIdMemberAndIdComment(id)
      .subscribe((data) => {});
  }

  postLike(id) {
    this.likeAndDisLike.idMember = this.member.id;
    this.likeAndDisLike.idCommentDiscuss = id;
    this.likeAndDisLike.idDiscuss = this.disCommet.idDiscuss;
    this.likeAndDisLike.like = 1;
    this.likeAndDisLike.dislike = 0;
    this.discussionService
      .postLike(this.likeAndDisLike)
      .subscribe((data) => {});
  }

  postDisLike(id) {
    this.likeAndDisLike.idMember = this.member.id;
    this.likeAndDisLike.idCommentDiscuss = id;
    this.likeAndDisLike.idDiscuss = this.disCommet.id;
    this.likeAndDisLike.like = 0;
    this.likeAndDisLike.dislike = 1;
    this.discussionService
      .postLike(this.likeAndDisLike)
      .subscribe((data) => {});
  }

  clearnText() {
    document.getElementById('content').innerText = '';
  }
}
