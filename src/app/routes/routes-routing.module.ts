// library
import { NgModule } from '@angular/core';
import { environment } from '@env/environment';
import { RouterModule, Routes } from '@angular/router';

/// -f-share
// ng-zorro
// call
import { CallbackComponent } from './f-share/callback/callback.component';
// layoutx
// passport
import { LoginComponent } from './passport/login/login.component';
import { RegisterComponent } from './passport/register/register.component';
import { ForgotPasswordComponent } from './passport/forgot-password/forgot-password.component';
// f-share manage
import { ManagePostComponent } from './f-share/manage/post/manage-post.component';
import { PostEditComponent } from './f-share/manage/post-edit/post-edit.component';
import { PostChangeComponent } from './f-share/manage/post-change/post-change.component';
// f-share money
import { SubjectsComponent } from './f-share/money/subject/subjects/subjects.component';
import { SubjectsListComponent } from './f-share/money/subject/subjects-list/subjects-list.component';
import { SubjectsDetailsComponent } from './f-share/money/subject/subjects-details/subjects-details.component';
import { SubjectAllComponent } from './f-share/money/subject/subject-all/subject-all.component';
// f-share user
import { ShowregistrationComponent } from './f-share/manage/showregistration/showregistration.component';
import { ShowDetairegistrationComponent } from './f-share/manage/show-detairegistration/show-detairegistration.component';
import { MoneyuserHistoryComponent } from './f-share/money/moneyuser-history/moneyuser-history.component';
import { MoneyuserBookcheludComponent } from './f-share/money/moneyuser-bookchelud/moneyuser-bookchelud.component';
// step feedback
// step informaiton-change
import { InformationChangeComponent } from './f-learn/members/profile/information-change/information-change.component';
// step money-change
import { MoneyChangeComponent } from './f-learn/members/profile/money-change/money-change.component';
import { Moneystep3Component } from './f-learn/members/profile/money-change/moneystep3/moneystep3.component';
// step psssword-change
import { PasswordChangeComponent } from './f-learn/members/profile/password-change/password-change.component';
// step bookupload-change
import { BookscheduleComponent } from './f-share/money/subject/bookschedule/bookschedule.component';
// step addtocard
// out
import { CheckoutComponent } from './f-share/money/subject/checkout/checkout.component';
// commentpost


// f-learn
import { FLearnComponent } from './f-learn/f-learn.component';
import { ProfileComponent } from './f-learn/members/profile/profile.component';
import { BlogDetailComponent } from './f-learn/blogs/blog-detail/blog-detail.component';
import { BlogComponent } from './f-learn/blogs/blog/blog.component';
import { ConfessionShareComponent } from './f-learn/confession-share/confession-share.component';
import { CourseComponent } from './f-learn/courses/course/course.component';
import { MemberScreenComponent } from './f-learn/courses/member-screen/member-screen.component';
import { ExerciseComponent } from './f-learn/courses/quizzs/exercise/exercise.component';
import { QuizzComponent } from './f-learn/courses/quizzs/quizz/quizz.component';
import { TheoryComponent } from './f-learn/courses/quizzs/theory/theory.component';
import { SessionDetailComponent } from './f-learn/courses/session-detail/session-detail.component';
import { SessionComponent } from './f-learn/courses/session/session.component';
import { DiscussionDetailComponent } from './f-learn/discussions/discussion-detail/discussion-detail.component';
import { DiscussionPostComponent } from './f-learn/discussions/discussion-post/discussion-post.component';
import { DiscussionComponent } from './f-learn/discussions/discussion/discussion.component';
import { EvaluateComponent } from './f-learn/evaluate/evaluate.component';
import { ChangeInformationComponent } from './f-learn/members/change-information/change-information.component';
import { MemberComponent } from './f-learn/members/member/member.component';
import { CodeShareComponent } from './f-learn/code-share/code-share.component';
import { SessionViewComponent } from './f-learn/courses/session-view/session-view.component';

const routes: Routes = [
  {
    path: 'money', component: FLearnComponent,
    children: [
      { path: '', redirectTo: 'subject', pathMatch: 'full' },
      {
        path: 'subject-all', component: SubjectAllComponent,
        data: { title: 'Tất cả trung tâm' },
      },
      {
        path: 'subject', component: SubjectsComponent,
        data: { title: 'Trung tâm' },
      },
      {
        path: 'subject-detail/:ID', component: SubjectsDetailsComponent,
        data: { title: 'Khoá học' },
      },
      {
        path: 'subject-list/:ID', component: SubjectsListComponent,
        data: { title: 'Thông tin khoá học' },
      },
      {
        path: 'user-history', component: MoneyuserHistoryComponent,
        data: { title: 'Lịch sử đăng ký' },
      },
      {
        path: 'manage-post', component: ManagePostComponent,
        data: { title: 'Danh sách khoá học' },
      },
      {
        path: 'post-show/:ID', component: ShowregistrationComponent,
        data: { title: 'Danh sách đăng ký khoá học' },
      },
      {
        path: 'post-showdetail/:ID', component: ShowDetairegistrationComponent,
        data: { title: 'Chi tiết thông tin đăng ký' },
      },
      {
        path: 'post-add', component: PostChangeComponent,
        data: { title: 'Thêm khoá học' },
      },
      {
        path: 'post-edit/:ID', component: PostEditComponent,
        data: { title: 'Sửa khoá học' },
      },
      {
        path: 'checkout', component: CheckoutComponent,
        data: { title: 'Kiểm tra giỏ hàng' },
      },
      {
        path: 'bookchedule/:ID', component: BookscheduleComponent,
        data: { title: 'Đăng ký khoá học' },
      },
      {
        path: 'checkbookchelus', component: MoneyuserBookcheludComponent,
        data: { title: 'Kiểm tra đăng ký khoá học' },
      },
      // Exception
      {
        path: 'exception',
        loadChildren: () =>
          import('./f-share/exception/exception.module').then((m) => m.ExceptionModule),
      },
    ],
  },

  {
    path: 'f-learn.poly', component: FLearnComponent,
    data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
    children: [
      { 
        path: 'home', component: MemberScreenComponent, 
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
      },
      { 
        path: 'user/information', component: ProfileComponent, 
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
      },
      {
        path: 'user/information/change', component: ChangeInformationComponent,
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
      },
      {
        path: 'information-change', component: InformationChangeComponent,
        data: { titleI18n: 'Thay đổi người dùng' },
      },
      {
        path: 'money-change', component: MoneyChangeComponent,
        data: { titleI18n: 'Nạp tiền' },
      },
      {
        path: 'money-done', component: Moneystep3Component,
        data: { titleI18n: 'Nạp thành công' },
      },
      {
        path: 'password-change', component: PasswordChangeComponent,
        data: { titleI18n: 'Thay đổi mật khẩu' },
      },
      { 
        path: 'course', component: CourseComponent, 
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
      },
      { 
        path: 'discussion', component: DiscussionComponent, 
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
      },
      {
        path: 'discussion/post', component: DiscussionPostComponent,
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
      },
      {
        path: 'discussion/:id', component: DiscussionDetailComponent,
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
      },
      { 
        path: 'blog', component: BlogComponent, 
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
      },
      { 
        path: 'blog/:id', component: BlogDetailComponent, 
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
      },
      {
        path: 'blog/findBlogByTitle/:title', component: BlogComponent,
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
      },
      { 
        path: 'members', component: MemberComponent, 
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
      },
      { 
        path: 'evaluate', component: EvaluateComponent, 
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
      },
      {
        path: 'confession-share', component: ConfessionShareComponent,
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
      },
      { 
        path: ':idCourse/sessions', component: SessionComponent, 
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
      },
      { 
        path: 'code-share', component: CodeShareComponent, 
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
      },
      {
        path: 'session/:id', component: SessionDetailComponent,
        data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
        children: [
          {
            path: ':idSession', component: SessionViewComponent,
            data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
          },
          {
            path: ':idSession/theorys', component: TheoryComponent,
            data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
          },
          {
            path: ':idSession/:idQuiz/questions', component: QuizzComponent,
            data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
          },
          {
            path: ':idSession/:idexe/exercises', component: ExerciseComponent,
            data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
          },
        ],
      },
    ],
  },


  { path: '', redirectTo: 'f-learn.poly/home', pathMatch: 'full' },
  { 
    path: 'f-learn.poly/login', component: LoginComponent, 
    data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' } 
  },
  {
    path: 'f-learn.poly/forgot-password', component: ForgotPasswordComponent,
    data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
  }, 
  {
    path: 'f-learn.poly/register', component: RegisterComponent,
    data: { titleI18n: 'FLearn - Học lập trình trở nên đơn giản và thú vị' },
  },


  // Layout
  { path: 'callback/:type', component: CallbackComponent },
  { path: '**', redirectTo: 'exception/404' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: environment.useHash,
      scrollPositionRestoration: 'top',
    }),
  ],
  exports: [RouterModule],
})
export class RouteRoutingModule {}
