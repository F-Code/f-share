import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Notification } from 'src/app/model/Notification';
import { NotificationService } from 'src/app/service/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.less']
})
export class NotificationComponent implements OnInit {
  notifications = 0;
  noti: Notification[] = [];
  image;
  idMember;
  member;
  loading = true;
  private checkring = [];
  statuscheckring = 0;
  changecheckring = null;

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private notificationService: NotificationService,
  ) {

  }

  change() {
    setTimeout(() => {
      this.loading = false;
      this.cdr.detectChanges();
    }, 500);
  }

  ngOnInit(): void {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.idMember = 1;
    } else {
      this.member = JSON.parse(localStorage.getItem('information'));
      this.idMember = this.member.id;
      this.notificationService.getAllNotificationId(this.idMember).subscribe(data => {
        this.noti = data;
        for (let i = 0; i < data.length; i++) {
          this.checkring.push(data[i].status);
        }
        if (this.checkring.includes(0)) {
          this.statuscheckring = 1;
        }
      });
    }
  }

  checkringdone(){
    this.statuscheckring = 0;
    this.notificationService.getNotificationIdmember(this.idMember).subscribe(data => {
    });
  }
}
