import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { App, SettingsService } from '@delon/theme';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
  searchToggleStatus: boolean;
  status = 0;
  get app(): App {
    return this.settings.app;
  }

  get collapsed(): boolean {
    return this.settings.layout.collapsed;
  }

  constructor(
    private settings: SettingsService,
    private settingSrv: SettingsService,
  ) {}

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account === null) {
      this.status = 0;
    } else {
      this.status = 1;
    }
  }

  toggleCollapsedSidebar() {
    this.settings.setLayout('collapsed', !this.settings.layout.collapsed);
  }

  searchToggleChange() {
    this.searchToggleStatus = !this.searchToggleStatus;
  }

  setLayout(name: string, value: any) {
    this.settingSrv.setLayout(name, value);
  }

  get layout() {
    return this.settingSrv.layout;
  }
}
