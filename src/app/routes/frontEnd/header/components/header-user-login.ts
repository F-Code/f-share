import { ChangeDetectionStrategy, Component, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { Member } from '../../../../model/Member';
import { Interviews } from '../../../../model/Interviews';
import { LandingPageService } from '../../../../service/landing-page.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
    selector: 'header-user-login',
    template: `
<div class="alain-default__nav-item d-flex align-items-center px-sm" nz-dropdown nzPlacement="bottomRight"
    [nzDropdownMenu]="userMenu">
    <nz-avatar [nzSrc]="account.avatar" nzSize="small" class="mr-sm"></nz-avatar>
    {{ account.displayName }}
</div>
<nz-dropdown-menu #userMenu="nzDropdownMenu">
    <div nz-menu class="width-sm">
        <div nz-menu-item routerLink="/f-learn.poly/user/information">
            <i nz-icon nzType="user" class="mr-sm"></i>Thông tin tài khoản
        </div>
        <div *ngIf="!account.centralRight">
            <div nz-menu-item (click)="showModal()">
                <i nz-icon nzType="user" class="mr-sm"></i>Đăng ký trung tâm
            </div>
        </div>
        <div *ngIf="account.centralRight">
            <div nz-menu-item routerLink="/money/manage-post">
                <i nz-icon nzType="setting" class="mr-sm"></i>Chia sẻ khoá học
            </div>
        </div>
        <div nz-menu-item routerLink="/f-learn.poly/confession-share">
            <i nz-icon nzType="close-circle" class="mr-sm"></i>Chia sẻ Confesstion
        </div>
        <div nz-menu-item routerLink="/money/user-history">
            <i nz-icon nzType="close-circle" class="mr-sm"></i>Lịch sử đăng ký
        </div>
        <div nz-menu-item (click)="logout()">
            <i nz-icon nzType="logout" class="mr-sm"></i>Đăng xuất
        </div>
    </div>
</nz-dropdown-menu>

<ng-template #template>
    <div class="ant-notification-notice-content">
        <div class="ant-notification-notice-with-icon">
            <span class="ant-notification-notice-icon"><i class="fas fa-check-circle fa-2x"></i></span>
            <div class="ant-notification-notice-message">Thông tin của bạn đã được gửi. Chúng tôi sẽ kiểm tra và liên hệ với bạn sớm nhất</div>
        </div>
    </div>
</ng-template>
<nz-modal [(nzVisible)]="isVisible" nzTitle="Bạn muốn đăng ký chia sẻ khóa học" (nzOnCancel)="cancel()" [nzCancelText]="null" [nzOkText]="null" nzWidth="730px">
    <div id="test">
        <h3>Nhập đầy đủ thông tin đăng ký</h3>
        <form [formGroup]="addInterviewForm">
            <br>
            <nz-form-item>
                <nz-form-control nzErrorTip="Vui lòng nhập họ và tên">
                    <nz-input-group nzSize="large" nzPrefixIcon="user">
                        <input nz-input type="text" name="name" placeholder="Họ và tên" [(ngModel)]="Interview.familyName" formControlName="familyName" class="input-table" required />
                    </nz-input-group>
                </nz-form-control>
            </nz-form-item>
            <nz-form-item>
                <nz-form-control nzErrorTip="Vui lòng nhập email">
                    <nz-input-group nzSize="large" nzPrefixIcon="mail">
                        <input nz-input type="text" name="email" placeholder="Email" [(ngModel)]="Interview.emailAddress" formControlName="emailAddress" class="input-table" readonly />
                    </nz-input-group>
                </nz-form-control>
            </nz-form-item>
            <nz-form-item>
                <nz-form-control nzErrorTip="Vui lòng nhập số điện thoại">
                    <nz-input-group nzSize="large">
                        <input nz-input type="text" name="phone" placeholder="Số điện thoại" [(ngModel)]="Interview.phoneNumber" formControlName="phoneNumber" class="input-table" required />
                    </nz-input-group>
                </nz-form-control>
            </nz-form-item>
            <nz-form-item>
                <nz-form-control nzErrorTip="Vui lòng chọn ngày sinh">
                    <nz-input-group nzSize="large">
                        <input nz-input type="date" name="birthday" [(ngModel)]="Interview.birthYear" formControlName="birthYear" class="input-table" required />
                    </nz-input-group>
                </nz-form-control>
            </nz-form-item>
            <textarea nz-input type="text" name="long" placeholder="Lời nhắn" [nzAutosize]="{ minRows: 8, maxRows: 19 }"></textarea>
            <br><br>
            <button nz-button nzType="primary" (click)="onSubmit(template)">
            Gửi
              </button>
        </form>
    </div>
</nz-modal>
`, styles: [
        `
#test {
width: 700px;
height: 370px;
overflow-x: hidden;
overflow-y: auto;
}
.fa-check-circle{
  margin-left: -17px;
  color: rgb(79, 226, 158);
}
`
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderUserLoginComponent {
    account: Member[];
    Interview: Interviews = new Interviews();
    addInterviewForm: FormGroup;

    constructor(
        private router: Router,
        private landingPageService: LandingPageService,
        private leadingValidator: FormBuilder,
        private notification: NzNotificationService,
    ) {
        this.addInterviewForm = this.leadingValidator.group({
            familyName: ['', [Validators.required, Validators.minLength(4)]],
            emailAddress: ['', Validators.required],
            phoneNumber: ['', Validators.required],
            birthYear: ['', Validators.required],
        });
    }

    logout() {
        localStorage.removeItem('account');
        localStorage.removeItem('information');
        localStorage.removeItem('informationchange');
        localStorage.removeItem('addtocard');
        localStorage.removeItem('addtocardxprice');
        localStorage.removeItem('infomationpost');
        localStorage.removeItem('informationcheck');
        localStorage.removeItem('showmodal');
        localStorage.removeItem('linkVNP');
        localStorage.removeItem('image');
        localStorage.removeItem('bookchedule');
        localStorage.removeItem('VNPhitory');
        localStorage.removeItem('moneyup');
        this.router.navigate(['/f-learn.poly/login']);
    }
    ngOnInit(): void {
        this.account = JSON.parse(window.localStorage.getItem('information'));
        var account = JSON.parse(window.localStorage.getItem('information'));
        this.Interview.familyName = account.familyName  + account.displayName
        this.Interview.emailAddress = account.emailAddress 
        this.Interview.phoneNumber = account.phoneNumber
        this.Interview.birthYear = account.birthYear
        this.Interview.idMember = account.id
    }

    isVisible = false;

    onSubmit(template: TemplateRef<{}>) {
        Object.keys(this.addInterviewForm.controls).forEach((key) => {
            this.addInterviewForm.controls[key].markAsDirty();
            this.addInterviewForm.controls[key].updateValueAndValidity();
        });
        if (this.addInterviewForm.invalid) {
            return;
        }
        this.isVisible = false;
        this.landingPageService.addInterview(this.Interview).subscribe((data) => {
            this.notification.template(template);
        });
    }

    untilmodal() {
        localStorage.setItem('showmodal', '1');
        this.isVisible = false;
    }

    showModal() {
        this.isVisible = true;
    }
    cancel() {
        this.isVisible = false;
    }
}