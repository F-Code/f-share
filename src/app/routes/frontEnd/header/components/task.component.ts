import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  } from '@angular/core';
import { NotificationService } from 'src/app/service/notification.service';
  
  @Component({
  selector: 'header-task',
  template: `
  <div nz-dropdown [nzDropdownMenu]="taskMenu" nzTrigger="click" nzPlacement="bottomRight"
      (nzVisibleChange)="change()">
      <nz-badge>
        <notice-icon
            [count]="notifications"
            btnClass="alain-default__nav-item"
            btnIconClass="alain-default__nav-item-icon"
        ></notice-icon>
      </nz-badge>
  </div>
  <nz-dropdown-menu #taskMenu="nzDropdownMenu">
      <div nz-menu class="wd-lg">
          <div *ngIf="loading" class="mx-lg p-lg">
              <nz-spin></nz-spin>
          </div>
          <nz-card *ngIf="!loading" nzTitle="Thông báo" nzBordered="false" class="ant-card__body-nopadding">
              <ng-template #extra><i nz-icon nzType="plus"></i></ng-template>
              <div nz-row [nzType]="'flex'" [nzJustify]="'center'" [nzAlign]="'middle'" class="py-sm point">
                  
                <div nz-col [nzSpan]="4" class="text-center">
                      <nz-avatar [nzSrc]="'./assets/tmp/img/1.png'"></nz-avatar>
                  </div>
                  <div nz-col [nzSpan]="20"  >
                      <strong>cipchk</strong>
                      <p class="mb0">
                          Please tell me what happened in a few words, don't go into
                          details.
                      </p>
                  </div>
              </div>
              <div nz-row>
                  <div nz-col [nzSpan]="24" class="pt-md border-top-1 text-center text-grey point">
                      See All
                  </div>
              </div>
          </nz-card>
      </div>
  </nz-dropdown-menu>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  })
  export class HeaderTaskComponent {
   notifications = 1
   noti = [];
   loading = true;
  
    constructor(
      private cdr: ChangeDetectorRef,
    ) {}
  
    change() {
        setTimeout(() => {
        this.loading = false;
        this.cdr.detectChanges();
        }, 500);
    }
  }