import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'header-search',
  template: `
    <div class="menu">
      <ul>
        <li>
          <a routerLink="/f-learn.poly/course">
          Học tập
          </a>
        </li>
        <li>
          <a routerLink="/f-learn.poly/discussion">
          Thảo luận
          </a>
        </li>
        <li>
          <a routerLink="/f-learn.poly/blog">
          Blog
          </a>
        </li>
        <li>
          <a routerLink="/money/subject">
          Trung tâm
          </a>
        </li>
        <li>
          <a routerLink="/f-learn.poly/members">
          Thành viên
          </a>
        </li>
      </ul>
    </div>
  `,
  styles: [
    `
      .menu ul {
        margin: 10px;
      }

      .menu ul li {
        display: inline-block;
        list-style: none;
        line-height: 36px;
      }

      .menu ul li a {
        text-decoration: none;
        color: white;
        display: block;
        padding: 0px 20px;
        font-size: 13px;
      }

      .menu ul li a:hover {
        background-color: #2a7ac4;
      }
      li {
        color: white;
        background-color: #1890ff;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderSearchComponent implements AfterViewInit, OnDestroy {
  q: string;
  qIpt: HTMLInputElement;
  options: string[] = [];
  search$ = new BehaviorSubject('');
  loading = false;

  @HostBinding('class.alain-default__search-focus')
  focus = false;
  @HostBinding('class.alain-default__search-toggled')
  searchToggled = false;

  @Input()
  set toggleChange(value: boolean) {
    if (typeof value === 'undefined') {
      return;
    }
    this.searchToggled = true;
    this.focus = true;
    setTimeout(() => this.qIpt.focus(), 300);
  }

  constructor(
    private el: ElementRef<HTMLElement>,
    private cdr: ChangeDetectorRef
  ) {}

  ngAfterViewInit(): void {
    this.qIpt = this.el.nativeElement.querySelector(
      '.ant-input'
    ) as HTMLInputElement;
    this.search$
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe((value) => {
        this.options = value
          ? [value, value + value, value + value + value]
          : [];
        this.loading = false;
        this.cdr.detectChanges();
      });
  }

  qFocus(): void {
    this.focus = true;
  }

  qBlur(): void {
    this.focus = false;
    this.searchToggled = false;
  }

  search(ev: KeyboardEvent): void {
    if (ev.key === 'Enter') {
      return;
    }
    this.loading = true;
    this.search$.next((ev.target as HTMLInputElement).value);
  }

  ngOnDestroy(): void {
    this.search$.complete();
    this.search$.unsubscribe();
  }
}
