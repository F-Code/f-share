import { ChangeDetectionStrategy, Component, Inject, TemplateRef } from '@angular/core';
import { Interviews } from '../../../../model/Interviews';
import { LandingPageService } from '../../../../service/landing-page.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../../../../service/account.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'header-user-logout',
  template: `
<div class="alain-default__nav-item d-flex align-items-center px-sm" nz-dropdown nzPlacement="bottomRight"
    [nzDropdownMenu]="userMenu">
    <nz-avatar [nzSrc]="account" nzSize="small" class="mr-sm"></nz-avatar>
</div>
<nz-dropdown-menu #userMenu="nzDropdownMenu">
    <div nz-menu class="width-sm">
        <div nz-menu-item routerLink="/f-learn.poly/register">
            <i nz-icon nzType="user" class="mr-sm"></i>Đăng ký
        </div>
        <div nz-menu-item routerLink="/f-learn.poly/login">
            <i nz-icon nzType="logout" class="mr-sm"></i>Đăng nhập
        </div>
    </div>
</nz-dropdown-menu>
`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderUserLogoutComponent {

  account = null;

  constructor(
  ) {
  }
  ngOnInit() {
    localStorage.setItem('image', JSON.stringify('https://upload.wikimedia.org/wikipedia/commons/3/34/PICA.jpg'));
    this.account = JSON.parse(window.localStorage.getItem('image'));
  }
}