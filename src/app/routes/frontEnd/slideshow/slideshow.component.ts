import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slideshow',
  templateUrl: './slideshow.component.html',
  styleUrls: ['./slideshow.component.less']
})
export class SlideshowComponent implements OnInit {
  array = [1, 2, 3, 4];

  constructor() { }

  ngOnInit(): void {
  }

}
