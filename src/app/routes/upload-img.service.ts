import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UploadImgService {
  private url = 'https://api.imgur.com/3/image';
  private accessToken = 'd8788cf14d732775b74b0059c454c1fc1f1300b9';
  imageLink: any;

  constructor(private http: HttpClient) {}

  uploadImage(imageFile: File) {
    const formData = new FormData();
    formData.append('image', imageFile, imageFile.name);
    const myHeaders = new HttpHeaders({
      Authorization: 'Bearer ' + this.accessToken,
    });
    const imageData = this.http
      .post(this.url, formData, { headers: myHeaders })
      .toPromise();
    return imageData;
  }
  getImages() {
    return this.imageLink;
  }
}
