// library
import { SharedModule } from '@shared';
import { NgModule } from '@angular/core';
import { RouteRoutingModule } from './routes-routing.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

/// -f-share
// ng-zorro
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzButtonModule } from 'ng-zorro-antd/button';
// call
import { CallbackComponent } from './f-share/callback/callback.component';
// layout
import { FooterComponent } from './frontEnd/footer/footer.component';
import { HeaderComponent } from './frontEnd/header/header.component';
import { SlideshowComponent } from './frontEnd/slideshow/slideshow.component';
import { HeaderUserLoginComponent } from './frontEnd/header/components/header-user-login';
import { HeaderUserLogoutComponent } from './frontEnd/header/components/header-user-logout';
import { HeaderFullScreenComponent } from './frontEnd/header/components/fullscreen.component';
import { HeaderI18nComponent } from './frontEnd/header/components/i18n.component';
import { HeaderIconComponent } from './frontEnd/header/components/icon.component';
import { HeaderNotifyComponent } from './frontEnd/header/components/notify.component';
import { HeaderSearchComponent } from './frontEnd/header/components/search.component';
import { HeaderStorageComponent } from './frontEnd/header/components/storage.component';
import { HeaderTaskComponent } from './frontEnd/header/components/task.component';
// passport
import { LoginComponent } from './passport/login/login.component';
import { RegisterComponent } from './passport/register/register.component';
import { ForgotPasswordComponent } from './passport/forgot-password/forgot-password.component';
// f-share manage
import { ManagePostComponent } from './f-share/manage/post/manage-post.component';
import { PostEditComponent } from './f-share/manage/post-edit/post-edit.component';
import { PostChangeComponent } from './f-share/manage/post-change/post-change.component';
// f-share money
import { SubjectsComponent } from './f-share/money/subject/subjects/subjects.component';
import { SubjectsListComponent } from './f-share/money/subject/subjects-list/subjects-list.component';
import { SubjectsDetailsComponent } from './f-share/money/subject/subjects-details/subjects-details.component';
import { SubjectAllComponent } from './f-share/money/subject/subject-all/subject-all.component';
// f-share user
import { ProfilreShow } from './f-learn/members/profile/profilre-show';
import { ShowregistrationComponent } from './f-share/manage/showregistration/showregistration.component';
import { MoneyuserHistoryComponent } from './f-share/money/moneyuser-history/moneyuser-history.component';
import { MoneyuserBookcheludComponent } from './f-share/money/moneyuser-bookchelud/moneyuser-bookchelud.component';
// step feedback
import { Stepfeedback1Component } from './f-share/money/subject/subjects-details/stepfeedback/stepfeedback1.component';
import { Stepfeedback2Component } from './f-share/money/subject/subjects-details/stepfeedback/stepfeedback2.component';
// step informaiton-change
import { InformationChangeComponent } from './f-learn/members/profile/information-change/information-change.component';
import { Informationstep1Component } from './f-learn/members/profile/information-change/informationstep1/informationstep1.component';
import { Informationstep2Component } from './f-learn/members/profile/information-change/informationstep2/informationstep2.component';
import { Informationstep3Component } from './f-learn/members/profile/information-change/informationstep3/informationstep3.component';
// step money-change
import { MoneyChangeComponent } from './f-learn/members/profile/money-change/money-change.component';
import { Moneystep1Component } from './f-learn/members/profile/money-change/moneystep1/moneystep1.component';
import { Moneystep2Component } from './f-learn/members/profile/money-change/moneystep2/moneystep2.component';
import { Moneystep3Component } from './f-learn/members/profile/money-change/moneystep3/moneystep3.component';
// step psssword-change
import { PasswordChangeComponent } from './f-learn/members/profile/password-change/password-change.component';
import { Passwordstep1Component } from './f-learn/members/profile/password-change/passwordstep1/passwordstep1.component';
import { Passwordstep2Component } from './f-learn/members/profile/password-change/passwordstep2/passwordstep2.component';
import { Passwordstep3Component } from './f-learn/members/profile/password-change/passwordstep3/passwordstep3.component';
// step bookupload-change
import { BookscheduleComponent } from './f-share/money/subject/bookschedule/bookschedule.component';
import { Bookdtep1Component } from './f-share/money/subject/bookschedule/bookdtep1/bookdtep1.component';
import { Bookdtep2Component } from './f-share/money/subject/bookschedule/bookdtep2/bookdtep2.component';
import { Bookdtep3Component } from './f-share/money/subject/bookschedule/bookdtep3/bookdtep3.component';
// step addtocard
import { ShoppingCartComponent1 } from './f-share/money/subject/subjects/shopping-cart.component1';
import { ShoppingCartComponent2 } from './f-share/money/subject/subjects-list/shopping-cart.component2';
// out
import { CheckoutComponent } from './f-share/money/subject/checkout/checkout.component';
// commentpost
import { CommentPostComponent } from './f-share/money/subject/comment-post/comment-post.component';



/// f-lead
import { BlogComponent } from './f-learn/blogs/blog/blog.component';
import { BlogDetailComponent } from './f-learn/blogs/blog-detail/blog-detail.component';
import { CodeShareComponent } from './f-learn/code-share/code-share.component';
import { ConfessionShareComponent } from './f-learn/confession-share/confession-share.component';
import { CourseComponent } from './f-learn/courses/course/course.component';
import { MemberScreenComponent } from './f-learn/courses/member-screen/member-screen.component';
import { ExerciseComponent } from './f-learn/courses/quizzs/exercise/exercise.component';
import { QuizzComponent } from './f-learn/courses/quizzs/quizz/quizz.component';
import { TheoryComponent } from './f-learn/courses/quizzs/theory/theory.component';
import { RankComponent } from './f-learn/courses/rank/rank.component';
import { SessionComponent } from './f-learn/courses/session/session.component';
import { SessionDetailComponent } from './f-learn/courses/session-detail/session-detail.component';
import { DiscussionComponent } from './f-learn/discussions/discussion/discussion.component';
import { DiscussionCommentComponent } from './f-learn/discussions/discussion-comment/discussion-comment.component';
import { DiscussionDetailComponent } from './f-learn/discussions/discussion-detail/discussion-detail.component';
import { DiscussionPostComponent } from './f-learn/discussions/discussion-post/discussion-post.component';
import { EvaluateComponent } from './f-learn/evaluate/evaluate.component';
import { ChangeInformationComponent } from './f-learn/members/change-information/change-information.component';
import { MemberComponent } from './f-learn/members/member/member.component';
import { ProfileComponent } from './f-learn/members/profile/profile.component';
import { FLearnComponent } from './f-learn/f-learn.component';
import { ShowDetairegistrationComponent } from './f-share/manage/show-detairegistration/show-detairegistration.component';
import { SessionViewComponent } from './f-learn/courses/session-view/session-view.component';
import { NotificationComponent } from './frontEnd/header/Notification/notification/notification.component';

const Layout = [
  FooterComponent,
  HeaderComponent,
  SlideshowComponent,
  HeaderUserLoginComponent,
  HeaderUserLogoutComponent,
  HeaderFullScreenComponent,
  HeaderI18nComponent,
  HeaderIconComponent,
  HeaderNotifyComponent,
  HeaderSearchComponent,
  HeaderStorageComponent,
  HeaderTaskComponent,
];

const Passport = [
  LoginComponent,
  RegisterComponent,
  ForgotPasswordComponent,
];

const Fsharemanage = [
  ManagePostComponent,
  PostEditComponent,
  PostChangeComponent,
];

const Fsharemoney = [
  SubjectsComponent,
  SubjectsListComponent,
  SubjectsDetailsComponent,
  SubjectAllComponent,
];

const Fshareuser = [
  ProfilreShow,
  ShowregistrationComponent,
  MoneyuserHistoryComponent,
  MoneyuserBookcheludComponent,
];

const Step = [
  // step - feedback
  Stepfeedback1Component,
  Stepfeedback2Component,

  // step - information-change
  InformationChangeComponent,
  Informationstep1Component,
  Informationstep2Component,
  Informationstep3Component,

  // step - information-change
  PasswordChangeComponent,
  Passwordstep1Component,
  Passwordstep2Component,
  Passwordstep3Component,

  // step - money-change
  MoneyChangeComponent,
  Moneystep1Component,
  Moneystep2Component,
  Moneystep3Component,

  // step - password-change
  BookscheduleComponent,
  Bookdtep1Component,
  Bookdtep2Component,
  Bookdtep3Component,

  // step - addtocard
  ShoppingCartComponent1,
  ShoppingCartComponent2,
];



const COMPONENTS = [
  // call
  CallbackComponent,
  // checkout
  CheckoutComponent,
  // comment
  CommentPostComponent,
];
const Imports = [
  // library
  SharedModule,
  EditorModule,
  NzCollapseModule,
  RouteRoutingModule,
  NzUploadModule,
  Ng2SearchPipeModule,
  NgxPaginationModule,
  // ng-zorro
  NzListModule,
  NzButtonModule,
  NzTypographyModule,
];
const COMPONENTS_NOROUNT = [
];

@NgModule({
  imports: [...Imports],
  declarations: [
    ...Layout,
    ...Passport,
    ...Fsharemanage,
    ...Fsharemoney,
    ...Fshareuser,
    ...Step,
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT,
    BlogComponent,
    BlogDetailComponent,
    CodeShareComponent,
    ConfessionShareComponent,
    CourseComponent,
    MemberScreenComponent,
    ExerciseComponent,
    QuizzComponent,
    TheoryComponent,
    RankComponent,
    SessionComponent,
    SessionDetailComponent,
    DiscussionComponent,
    DiscussionCommentComponent,
    DiscussionDetailComponent,
    DiscussionPostComponent,
    EvaluateComponent,
    ChangeInformationComponent,
    MemberComponent,
    ProfileComponent,
    FLearnComponent,
    ShowDetairegistrationComponent,
    SessionViewComponent,
    NotificationComponent,
  ],
  entryComponents: COMPONENTS_NOROUNT,
})
export class RoutesModule {}
