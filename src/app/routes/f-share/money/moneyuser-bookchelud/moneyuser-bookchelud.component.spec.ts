import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyuserBookcheludComponent } from './moneyuser-bookchelud.component';

describe('MoneyuserBookcheludComponent', () => {
  let component: MoneyuserBookcheludComponent;
  let fixture: ComponentFixture<MoneyuserBookcheludComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoneyuserBookcheludComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyuserBookcheludComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
