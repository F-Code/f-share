import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Bookchelud } from '../../../../model/Bookchelud';
import { BookcheludService } from '../../../../service/bookchelud.service';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
} from 'ng-zorro-antd/table';

export interface Filter {
  nameTitletable: string;
  sortFn?: NzTableSortFn;
  filterTable?: NzTableFilterList;
  filterFn?: NzTableFilterFn;
}

@Component({
  selector: 'app-moneyuser-bookchelud',
  templateUrl: './moneyuser-bookchelud.component.html',
  styleUrls: ['./moneyuser-bookchelud.component.less']
})
export class MoneyuserBookcheludComponent implements OnInit {

  /// Bỏ rồi
  constructor(private historyService: BookcheludService, private router: Router) {}

  history = [];
  historyorder: Bookchelud = new Bookchelud();
  time = new Date();
  posts = [];

  loaddatahistory(){
    var account = JSON.parse(localStorage.getItem('information'));
    this.historyService.getAllregistrationId(account.id).subscribe((data) => {
      this.history = data;
    });
  }

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else{
      this.loaddatahistory();
      this.getQueryParameter();
    }
  }
  
  private getQueryParameter() {
  }

  addhistoryorder(){
  }
  
  // lọc
  filterData: Filter[] = [
    {
      nameTitletable: 'Tên khách hàng',
    },
    {
      nameTitletable: 'Khoá học',
    },
  ];

  trackByName(_: number, item: Filter): string {
    return item.nameTitletable;
  }

  isVisible = false;

  showModal(): void {
    this.isVisible = true;
  }
  handleOk(): void {
    this.isVisible = false;
  }
}
