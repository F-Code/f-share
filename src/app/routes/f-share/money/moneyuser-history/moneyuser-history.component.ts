import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Bookchelud } from '../../../../model/Bookchelud';
import { BookcheludService } from '../../../../service/bookchelud.service';
import { ManageService } from '../../../../service/manage.service';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
} from 'ng-zorro-antd/table';

export interface Filter {
  nameTitletable: string;
  sortFn?: NzTableSortFn;
  filterTable?: NzTableFilterList;
  filterFn?: NzTableFilterFn;
}


@Component({
  selector: 'app-moneyuser-history',
  templateUrl: './moneyuser-history.component.html',
  styleUrls: ['./moneyuser-history.component.less'],
})
export class MoneyuserHistoryComponent implements OnInit {
  constructor(
    private bookcheludService: BookcheludService, 
    private manageService: ManageService,
    private router: Router,
    ) {}

  history = [];
  historyorder: Bookchelud = new Bookchelud();
  time = new Date();
  posts = [];
  uploadcheckcouraffi = null;

  loaddatahistory(){
    function compareValues(key, order = 'asc') {
      return function (a, b) {
        const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
        const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

        let comparison = 0;
        if (varA < varB) {
          comparison = 1;
        } else if (varA > varB) {
          comparison = -1;
        }
        return order === 'desc' ? comparison * -1 : comparison;
      };
    }
    var account = JSON.parse(localStorage.getItem('information'));
    this.bookcheludService.getAllregistrationId(account.id).subscribe((data) => {
      data.sort(compareValues('registrationPeriod', 'asc'));
      this.history = data;
    });
  }

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else{
      this.loaddatahistory();
      this.getQueryParameter();
    }
  }
  
  private getQueryParameter() {
    const parameters = window.location.href;
    var VNPhitory = JSON.parse(localStorage.getItem('VNPhitory'));
    var checkdone = parameters.includes('NCB&vnp_BankTranNo')
    var IdcoureAffi = JSON.parse(localStorage.getItem('idcoureaffi'));
    if(parameters.length > 350  && VNPhitory === 0 && checkdone === true){
      this.loaddatahistory();
      var bookcheduleData = JSON.parse(localStorage.getItem('bookchedule'));
      this.bookcheludService.addRegistration(bookcheduleData).subscribe((data) => {
      });
      this.manageService.getCourseaffiliateSeclectById(IdcoureAffi).subscribe((data) => {
        this.uploadcheckcouraffi = data;
        this.uploadcheckcouraffi.check = 1;
        this.uploadcheckcouraffi.status = data.status;
        this.manageService.updateCourseaffiliate(this.uploadcheckcouraffi).subscribe((data) => {
        });
      });
      localStorage.removeItem('VNPhitory');
      localStorage.removeItem('linkVNP');
      localStorage.removeItem('bookchedule');
      localStorage.removeItem('idcoureaffi');
      this.ngOnInit();
    }
  }
  
  // lọc
  filterData: Filter[] = [
    {
      nameTitletable: 'Trạng thái',
      filterTable: [
        { text: 'Chưa xem', value: '0' },
        { text: 'Đã xem', value: '1' },
      ],
      filterFn: (list: string[], item: Bookchelud) => list.some((name) => item.status.toString().indexOf(name) !== -1),
    },
    {
      nameTitletable: 'Tên khoá học',},  
    {
      nameTitletable: 'Thể loại',
      filterTable: [
        { text: 'JAVA', value: 'JAVA' },
        { text: 'PHP', value: 'PHP' },
        { text: 'C++', value: 'C++' },
        { text: 'SQL', value: 'SQL' },
      ],
      filterFn: (list: string[], item: Bookchelud) =>
        list.some(
          (listLanguage) => item.category.indexOf(listLanguage) !== -1
        ),
    },
    {
      nameTitletable: 'Hình thức học',
      filterTable: [
        { text: 'OnLine', value: 'OnLine' },
        { text: 'OffLine', value: 'OffLine' },
      ],
      filterFn: (list: string[], item: Bookchelud) => list.some((name) => item.type.toString().indexOf(name) !== -1),
    },
    {
      nameTitletable: 'Giá khoá học',
      sortFn: (a: Bookchelud, b: Bookchelud) => a.price - b.price,
    },
  ];

  trackByName(_: number, item: Filter): string {
    return item.nameTitletable;
  }

  isVisible = false;

  showModal(): void {
    this.isVisible = true;
  }
  handleOk(): void {
    this.isVisible = false;
  }
}
