import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyuserHistoryComponent } from './moneyuser-history.component';

describe('MoneyuserHistoryComponent', () => {
  let component: MoneyuserHistoryComponent;
  let fixture: ComponentFixture<MoneyuserHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MoneyuserHistoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyuserHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
