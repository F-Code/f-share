import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FeedbackService } from '../../../../../../service/feedback.service';

@Component({
  selector: 'app-stepfeedback2',
  templateUrl: './stepfeedback2.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Stepfeedback2Component {
  constructor(private feedbackService: FeedbackService) {}

  get item() {
    return this.feedbackService;
  }
}
