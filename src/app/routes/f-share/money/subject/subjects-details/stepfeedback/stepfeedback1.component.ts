import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { FeedbackService } from '../../../../../../service/feedback.service';
import { Feedback } from '../../../../../../model/Feedback';

@Component({
  selector: 'app-stepfeedback1',
  templateUrl: './stepfeedback1.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Stepfeedback1Component implements OnInit {
  constructor(
    private feedbackValidator: FormBuilder,
    private activaRoute: ActivatedRoute,
    private feedbackService: FeedbackService
  ) {}

  ngOnInit() {
    this.feedbackForm = this.feedbackValidator.group({
      title: new FormControl('', [Validators.required]),
      content: new FormControl('', [Validators.required]),
    });
    this.feedbackForm.patchValue(this.item);
  }

  get title() {
    return this.feedbackForm.controls.title;
  }
  get content() {
    return this.feedbackForm.controls.content;
  }
  get item() {
    return this.feedbackService;
  }

  feedbackForm2: Feedback = new Feedback();
  feedbackForm: FormGroup;
  loading = false;
  time = new Date();

  _submitForm() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      ++this.item.step;
    }, 500);
    var submitFeedback = JSON.parse(localStorage.getItem('idcoureaffi'));
    this.feedbackForm2.idCourseAffiliate = submitFeedback;
    var time = this.time.getHours() + ":" + this.time.getMinutes() + ":" + this.time.getSeconds();
    var date = this.time.getDate()+'-'+(this.time.getMonth()+1)+'-'+this.time.getFullYear();
    var fulltime = time + " " + date
    this.feedbackForm2.registrationPeriod = fulltime;
    this.feedbackService
      .addFeedback(this.feedbackForm2)
      .subscribe((data) => {});
  }
}
