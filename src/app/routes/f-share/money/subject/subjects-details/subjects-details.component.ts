import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { MoneyService } from '../../../../../service/money.service';
import { FeedbackService } from '../../../../../service/feedback.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-subjects-details',
  templateUrl: './subjects-details.component.html',
  styleUrls: ['./subjects-details.component.less'],
  providers: [FeedbackService],
})
export class SubjectsDetailsComponent implements OnInit {
  constructor(
    private srv: FeedbackService,
    private activaRoute: ActivatedRoute,
    private motelService: MoneyService,
    private fb: FormBuilder,
    private router: Router,
    private notification: NzNotificationService,
  ) { }

  postsForm = new FormGroup({
    id: new FormControl(null),
    idMember: new FormControl('', []),
    theCourseOfName: new FormControl('', []),
    imagePreviewTitle: new FormControl('', []),
    participantOfNumber: new FormControl('', []),
    shortDescription: new FormControl('', []),
    longDescription: new FormControl('', []),
    curriculum: new FormControl('', []),
    category: new FormControl('', []),
    itemsOfNumber: new FormControl('', []),
    totalStudyTime: new FormControl('', []),
    price: new FormControl('', []),
    feedback: new FormControl('', []),
    type: new FormControl('', []),
    status: new FormControl('', []),
    imagesList: new FormControl('', []),
    registrationPeriod: new FormControl('', []),
    check: new FormControl('', []),
    checkdelete: new FormControl('', []),
    level: new FormControl('', []),
  });

  // product = [];
  lengthorder = null;
  // sum = 0;
  // form: FormGroup;
  productListdata = null;
  // accountcheck = 0;

  ngOnInit() {
    // this.activaRoute.params.subscribe((param) => {
    //   this.motelService.getPostsSeclectById(param.ID).subscribe((data) => {
    //     this.productListdata = data;
    //     console.log(data)
    //   });
    // });
    // this.product = JSON.parse(localStorage.getItem('addtocard'));
    // this.lengthorder = this.product.length;
    this.activaRoute.params.subscribe((param) => {
      this.motelService.getCourseaffiliateSeclectById(param.ID).subscribe((data) => {
        if (data.status === false) {
          this.router.navigate(['/money/subject']);
        } else {
          localStorage.removeItem('idcoureaffi');
          this.postsForm.setValue(data);
        }
      });
    });
    // this.product = JSON.parse(localStorage.getItem('addtocard'));
    // if (this.product === null) {
    // this.lengthorder = 9999
    // } if (this.lengthorder === 9999) {
    // this.accountcheck = 0;
    // this.lengthorder = 0;
    // localStorage.setItem('addtocard', JSON.stringify([]));
    // localStorage.setItem('addtocardxprice', JSON.stringify([]));
    // this.form = this.fb.group({
    // items: this.fb.array([]),
    // });
    // } else {
    // this.loaddataaddtocard();
    // }
  }

  // sửa lượt xem
  // updateview() {
  //   this.product = JSON.parse(localStorage.getItem('addtocard'));
  //   this.lengthorder = this.product.length;
  //   this.activaRoute.params.subscribe((param) => {
  //     this.motelService.getPostsSeclectById(param.ID).subscribe((data) => {
  //       this.postsForm.setValue(data);
  //       var updateview = this.postsForm.value.view + 1;
  //       this.postsForm.value.view = updateview;
  //       this.postsForm.value.status = true;
  //       this.motelService.updatePost(this.postsForm.value).subscribe((data) => {
  //         localStorage.setItem('infomationpost', JSON.stringify(data));
  //       });
  //     });
  //   });
  // }

  // gọi lại thông tin khoá học
  // updatedata() {
  //   this.activaRoute.params.subscribe((param) => {
  //     this.motelService.getPostsSeclectById(param.ID).subscribe((data) => {
  //       this.productListdata = data;
  //     });
  //   });
  // this.product = JSON.parse(localStorage.getItem('addtocard'));
  // this.lengthorder = this.product.length;
  // this.activaRoute.params.subscribe((param) => {
  //   this.motelService.getPostsSeclectById(param.ID).subscribe((data) => {
  //     this.postsForm.setValue(data);
  //   });
  // });
  // this.form = this.fb.group({
  //   items: this.fb.array([]),
  // });
  // const userList = this.product
  // userList.forEach((i) => {
  //   const field = this.createUser();
  //   field.patchValue(i);
  //   this.items.push(field);
  // });
  // }

  // load lại giá
  // loadprive() {
  //   this.sum = 0;
  //   var account = JSON.parse(localStorage.getItem('addtocardxprice'));
  //   for (var i = 0; i < account.length; i++) {
  //     this.sum += account[i];
  //   }
  // }


  // phẩn hồi và ktra tài khoản phản hồi
  visible = false;

  open(template: TemplateRef<{}>): void {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.notification.template(template);
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.visible = true;
      this.activaRoute.params.subscribe((param) => {
        localStorage.setItem('idcoureaffi', JSON.stringify(param.ID));
      });
    }
  }
  get item() {
    return this.srv;
  }
  close(): void {
    this.item.again();
    localStorage.removeItem('idcoureaffi');
    this.visible = false;
  }

  /// ktra tài khoản addtocart
  checkaccount(template: TemplateRef<{}>) {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.notification.template(template);
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.router.navigate(['/money/checkout']);
    }
  }


  // load data addtocard
  // loaddataaddtocard() {
  //   this.accountcheck = 1;
  //   this.activaRoute.params.subscribe((param) => {
  //     this.motelService.getPostsSeclectById(param.ID).subscribe((data) => {
  //       this.productListdata = data;
  //     });
  //   });
  // this.updateview();
  // this.form = this.fb.group({
  //   items: this.fb.array([]),
  // });
  // const userList = this.product
  // userList.forEach((i) => {
  //   const field = this.createUser();
  //   field.patchValue(i);
  //   this.items.push(field);
  // });
  // this.loadprive();
  // }


  // createUser(): FormGroup {
  //   return this.fb.group({
  //     imagePreviewTitle: [null],
  //     name: [null],
  //     xprice: [null],
  //   });
  // }

  // get items() {
  //   return this.form.controls.items as FormArray;
  // }


  // thêm sản phẩm vào giỏ hàng
  // list = [];
  // listxprice = [];

  // addlistpost(template: TemplateRef<{}>, addtocard: TemplateRef<{}>) {
  //   var account = JSON.parse(localStorage.getItem('account'));
  //   if (account == null) {
  //     this.notification.template(template);
  //     this.router.navigate(['/f-learn.poly/login']);
  //   } else {
  //     this.notification.template(addtocard);
  //     this.list = JSON.parse(localStorage.getItem('addtocard'));
  //     this.listxprice = JSON.parse(localStorage.getItem('addtocardxprice'));
  //     this.list.push(this.productListdata);
  //     this.listxprice.push(this.productListdata.xprice);
  //     localStorage.setItem('addtocard', JSON.stringify(this.list));
  //     localStorage.setItem('addtocardxprice', JSON.stringify(this.listxprice));
  //     this.loadprive();
  //     this.updatedata();
  //   }
  // }


  // đến giỏ hàng ktra sản phẩm
  // addpost(template: TemplateRef<{}>) {
  //   var account = JSON.parse(localStorage.getItem('account'));
  //   if (account == null) {
  //     this.notification.template(template);
  //     this.router.navigate(['/f-learn.poly/login']);
  //   } else {
  //     this.list = JSON.parse(localStorage.getItem('addtocard'));
  //     this.listxprice = JSON.parse(localStorage.getItem('addtocardxprice'));
  //     this.list.push(this.productListdata);
  //     this.listxprice.push(this.productListdata.xprice);
  //     localStorage.setItem('addtocard', JSON.stringify(this.list));
  //     localStorage.setItem('addtocardxprice', JSON.stringify(this.listxprice));
  //     this.router.navigate(['/money/checkout']);
  //   }
  // }

  // xoá sản phẩm trong giỏ
  // del(index) {
  //   this.items.removeAt(index);
  //   var account = JSON.parse(localStorage.getItem('addtocard'));
  //   var accountprice = JSON.parse(localStorage.getItem('addtocardxprice'));
  //   const removed = account.splice(index, 1);
  //   const removedprice = accountprice.splice(index, 1);
  //   localStorage.setItem('addtocard', JSON.stringify(account));
  //   localStorage.setItem('addtocardxprice', JSON.stringify(accountprice));
  //   this.loadprive();
  //   this.updatedata();
  // }


  // xoá tất cả sản phẩm trong giỏ
  // deleteallorder() {
  //   var addtocard = JSON.parse(localStorage.getItem('addtocard'));
  //   var addtocardxprice = JSON.parse(localStorage.getItem('addtocardxprice'));
  //   if (addtocardxprice.length > 0 && addtocard.length > 0) {
  //     let conf = confirm('Bạn muốn xoá hết ??');
  //     if (conf == true) {
  //       localStorage.setItem('addtocard', JSON.stringify([]));
  //       localStorage.setItem('addtocardxprice', JSON.stringify([]));
  //       this.loadprive();
  //       this.updatedata();
  //     }
  //   }
  // }
}
