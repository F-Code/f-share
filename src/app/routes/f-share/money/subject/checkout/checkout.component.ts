import { Component, OnInit } from '@angular/core';
import { MoneyService } from '../../../../../service/money.service';
import { Pay } from '../../../../../model/Pay';
import { Bookchelud } from '../../../../../model/Bookchelud';
import { Router } from '@angular/router';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.less'],
})
export class CheckoutComponent implements OnInit {

  /// Bỏ rồi
  constructor(
    private moneyService: MoneyService,
    private fb: FormBuilder,
    private router: Router,
  ) { }

  history: Bookchelud = new Bookchelud();
  posts = [];
  searchText;
  linkVNP = null;
  page: 5;
  sum = 0;

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      var account = JSON.parse(localStorage.getItem('addtocard'));
      this.posts = account;
      this.form = this.fb.group({
        items: this.fb.array([]),
      });
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      var account = JSON.parse(localStorage.getItem('addtocard'));
      this.posts = account;
      this.sum = 0;
      var account = JSON.parse(localStorage.getItem('addtocardxprice'));
      for (var i = 0; i < account.length; i++) {
        this.sum += account[i];
      }
      this.form = this.fb.group({
        items: this.fb.array([]),
      });
      const userList = this.posts
      userList.forEach((i) => {
        const field = this.createUser();
        field.patchValue(i);
        this.items.push(field);
      });
    }
  }

  pay1: Pay = new Pay();
  visible = false;
  time = new Date();
  form: FormGroup;
  vnphitory = 0;
  isVisible = false;

  open() {
    var account = JSON.parse(localStorage.getItem('addtocardxprice'));
    if(account.length ===  0){
      alert("Chưa có sản phẩm trong giỏ")
    } else {
      this.isVisible = true;
      var account = JSON.parse(localStorage.getItem('addtocardxprice'));
      var tong = 0;
      for (var i = 0; i < account.length; i++) {
        tong += account[i];
      }
      this.pay1.price = tong.toString();
      this.moneyService.pay(this.pay1).subscribe((data) => {
        localStorage.setItem('linkVNP', JSON.stringify(data.data));
        this.linkVNP = JSON.parse(localStorage.getItem('linkVNP'));
        localStorage.setItem('VNPhitory', JSON.stringify(this.vnphitory));
      });
    }
  }

  handleOk() {
    this.isVisible = false;
    localStorage.removeItem('VNPhitory');
  }

  deleteallorder() {
    var addtocard = JSON.parse(localStorage.getItem('addtocard'));
    var addtocardxprice = JSON.parse(localStorage.getItem('addtocardxprice'));
    if (addtocardxprice.length > 0 && addtocard.length > 0) {
      let conf = confirm('Bạn muốn xoá hết ??');
      if (conf == true) {
        localStorage.setItem('addtocard', JSON.stringify([]));
        localStorage.setItem('addtocardxprice', JSON.stringify([]));
        this.ngOnInit();
      }
    }
  }

  createUser(): FormGroup {
    return this.fb.group({
      imagePreviewTitle: [null],
      name: [null],
      xprice: [null],
    });
  }

  get items() {
    return this.form.controls.items as FormArray;
  }

  del(index) {
    this.items.removeAt(index);
    var account = JSON.parse(localStorage.getItem('addtocard'));
    var accountprice = JSON.parse(localStorage.getItem('addtocardxprice'));
    const removed = account.splice(index, 1);
    const removedprice = accountprice.splice(index, 1);
    localStorage.setItem('addtocard', JSON.stringify(account));
    localStorage.setItem('addtocardxprice', JSON.stringify(accountprice));
    this.ngOnInit();
  }
}
