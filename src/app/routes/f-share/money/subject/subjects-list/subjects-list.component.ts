import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoneyService } from '../../../../../service/money.service';

@Component({
  selector: 'app-subject-list',
  templateUrl: './subjects-list.component.html',
  styleUrls: ['./subjects-list.component.less'],
})
export class SubjectsListComponent implements OnInit {
  constructor(
    private actionroute: ActivatedRoute,
    private moneyService: MoneyService
  ) {}

  posts = [];
  searchText;
  page: 9;

  ngOnInit() {
    this.actionroute.params.subscribe((param) => {
      this.moneyService.getAllCourseaffiliateId(param.ID).subscribe((data) => {
        this.posts = data;
      });
    });
  }
}
