import { Component, OnInit, TemplateRef  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostComment } from '../../../../../model/Post-Comment';
import { Member } from '../../../../../model/Member';
import { MoneyService } from '../../../../../service/money.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-comment-post',
  templateUrl: './comment-post.component.html',
  styleUrls: ['./comment-post.component.less'],
})
export class CommentPostComponent implements OnInit {

  /// Bỏ rồi
  disCommet: PostComment = new (class implements PostComment {
    dateComment: string;
    avatar: string;
    content: string;
    id: string;
    idPost: string;
    reviewName: string;
  })();

  comment: PostComment[];
  users: Member[];
  data: any[] = [];
  tinymceInit: any;
  time = new Date();
  private jsonString;
  private member;
  loginForm: FormGroup;

  constructor(
    private loginValidator: FormBuilder,
    private discussionService: MoneyService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private notification: NzNotificationService,
  ) {
    this.loginForm = loginValidator.group({
      content: [null, [Validators.required]],
    });
  }

  get content() {
    return this.loginForm.controls.content;
  }

  ngOnInit() {
    this.getComment();
    this.jsonString = localStorage.getItem('information');
    this.member = JSON.parse(this.jsonString);
    this.activatedRoute.params.subscribe((param) => {
      this.disCommet.idPost = param.ID;
    });
  }

  getComment() {
    this.activatedRoute.params.subscribe((param) => {
      this.discussionService.getCommentPost(param.ID).subscribe((data) => {
        this.comment = data;
      });
    });
  }

  postComment(template: TemplateRef<{}>) {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.notification.template(template);
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      Object.keys(this.loginForm.controls).forEach((key) => {
        this.loginForm.controls[key].markAsDirty();
        this.loginForm.controls[key].updateValueAndValidity();
      });
      if (this.loginForm.invalid) {
        return;
      }
      this.disCommet.avatar = this.member.avatar;
      this.disCommet.reviewName = this.member.familyName;
      var time = this.time.getHours() + ":" + this.time.getMinutes() + ":" + this.time.getSeconds();
      var date = this.time.getDate()+'-'+(this.time.getMonth()+1)+'-'+this.time.getFullYear();
      var fulltime = time + " " + date
      this.disCommet.dateComment = fulltime;
      this.activatedRoute.params.subscribe((param) => {
        this.discussionService
          .postCommentPost(this.disCommet)
          .subscribe((data) => {
            this.getComment();
          });
      });
    }
  }
}
