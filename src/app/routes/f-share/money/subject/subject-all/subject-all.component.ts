import { Component, OnInit } from '@angular/core';
import { MoneyService } from '../../../../../service/money.service';

@Component({
  selector: 'app-subject-all',
  templateUrl: './subject-all.component.html',
  styleUrls: ['./subject-all.component.less']
})
export class SubjectAllComponent implements OnInit {
  constructor(
    private moneyService: MoneyService
  ) { }

  posts = [];
  searchText;
  page: 9;

  ngOnInit() {
    var levelcourdaffi = JSON.parse(localStorage.getItem('levelcoureaffi'));
    this.moneyService.getAllselectId(levelcourdaffi).subscribe((data) => {
      this.posts = data;
    });
  }
}