import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BookcheludService } from '../../../../../../service/bookchelud.service';

@Component({
  selector: 'app-bookdtep3',
  templateUrl: './bookdtep3.component.html',
  styleUrls: ['./bookdtep3.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Bookdtep3Component implements OnInit {

  constructor(
    private bookcheludService: BookcheludService,
  ) { }

  get item() {
    return this.bookcheludService;
  }

  ngOnInit() {
  }

  isVisible = false;
  linkVNP = null;
  vnphitory = 0;

  open() {
    this.isVisible = true;
    this.linkVNP = JSON.parse(localStorage.getItem('linkVNP'));
    localStorage.setItem('VNPhitory', JSON.stringify(this.vnphitory));
  }

  handleOk() {
    this.isVisible = false;
    localStorage.removeItem('VNPhitory');
  }

  prev() {
    --this.item.step;
    localStorage.removeItem('VNPhitory');
    localStorage.removeItem('linkVNP');
  }
}
