import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bookdtep3Component } from './bookdtep3.component';

describe('Bookdtep3Component', () => {
  let component: Bookdtep3Component;
  let fixture: ComponentFixture<Bookdtep3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bookdtep3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bookdtep3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
