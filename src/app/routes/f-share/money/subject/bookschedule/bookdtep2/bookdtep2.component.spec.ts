import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bookdtep2Component } from './bookdtep2.component';

describe('Bookdtep2Component', () => {
  let component: Bookdtep2Component;
  let fixture: ComponentFixture<Bookdtep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bookdtep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bookdtep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
