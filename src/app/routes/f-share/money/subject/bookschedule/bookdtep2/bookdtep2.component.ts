import { Component, OnInit } from '@angular/core';
import { BookcheludService } from '../../../../../../service/bookchelud.service';
import { ManageService } from '../../../../../../service/manage.service';
import { Pay } from '../../../../../../model/Pay';

@Component({
  selector: 'app-bookdtep2',
  templateUrl: './bookdtep2.component.html',
  styleUrls: ['./bookdtep2.component.less']
})
export class Bookdtep2Component implements OnInit {

  constructor(
    private bookcheludService: BookcheludService,
    private manageService: ManageService,
  ) { }

  get item() {
    return this.bookcheludService;
  }

  pay1: Pay = new Pay();
  bookchedule = null;
  linkVNP = null;
  uploadcheckcouraffi = null;

  ngOnInit() {
    var bookcheduleData = JSON.parse(localStorage.getItem('bookchedule'));
    this.bookchedule = bookcheduleData;
  }

  checkdone() {
    var IdcoureAffi = JSON.parse(localStorage.getItem('idcoureaffi'));
    var bookcheduleData = JSON.parse(localStorage.getItem('bookchedule'));
    this.bookcheludService.addRegistration(bookcheduleData).subscribe((data) => {
      this.manageService.getCourseaffiliateSeclectById(IdcoureAffi).subscribe((data) => {
        this.uploadcheckcouraffi = data;
        this.uploadcheckcouraffi.check = 1;
        this.uploadcheckcouraffi.status = data.status;
        this.manageService.updateCourseaffiliate(this.uploadcheckcouraffi).subscribe((data) => {
          ++this.item.step;
          localStorage.removeItem('VNPhitory');
          localStorage.removeItem('linkVNP');
          localStorage.removeItem('bookchedule');
          localStorage.removeItem('idcoureaffi');
        });
      });
    });
    // this.pay1.price = "10000";
    // this.bookcheludService.pay(this.pay1).subscribe((data) => {
    //   localStorage.setItem('linkVNP', JSON.stringify(data.data));
    //   ++this.item.step;
    // });
  }
  prev() {
    --this.item.step;
    localStorage.removeItem('bookchedule');
  }

  isVisible2 = false;

  showModal2(): void {
    this.isVisible2 = true;
  }
  cancel2() {
    this.isVisible2 = false;
  }
}
