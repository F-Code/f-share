import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bookdtep1Component } from './bookdtep1.component';

describe('Bookdtep1Component', () => {
  let component: Bookdtep1Component;
  let fixture: ComponentFixture<Bookdtep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bookdtep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bookdtep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
