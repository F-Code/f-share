import { Component, OnInit } from '@angular/core';
import { BookcheludService } from '../../../../../../service/bookchelud.service';
import { Bookchelud } from '../../../../../../model/Bookchelud';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-bookdtep1',
  templateUrl: './bookdtep1.component.html',
  styleUrls: ['./bookdtep1.component.less']
})
export class Bookdtep1Component implements OnInit {

  constructor(
    private postFormValidator: FormBuilder,
    private activaRoute: ActivatedRoute,
    private bookcheludService: BookcheludService,
    private router: Router,
  ) { 
    this.postForm = this.postFormValidator.group({
      id: ['', []],
      idMember: ['', []],
      idCourseAffiliate: ['', []],
      theCourseOfName: ['', [Validators.required]],
      price: ['', [Validators.required]],
      category: ['', [Validators.required]],
      type: ['', [Validators.required]],
      familyName: ['', [Validators.required]],
      emailAddress: ['', [Validators.required]],
      birthYear: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required,
        Validators.maxLength(10),
        Validators.minLength(10),
        Validators.pattern('[0-9]*'),]],
      province: ['', [Validators.required]],
      streetInformation: ['', [Validators.required]],
      registrationPeriod: ['', []],
    });
  }

  postForm: FormGroup;
  datacouraffi = null;
  idcoureaffi = null;

  get idMember() { return this.postForm.controls.idMember; }
  get theCourseOfName() { return this.postForm.controls.theCourseOfName; }
  get price() { return this.postForm.controls.price; }
  get category() { return this.postForm.controls.category; }
  get type() { return this.postForm.controls.type; }
  get familyName() { return this.postForm.controls.familyName; }
  get emailAddress() { return this.postForm.controls.emailAddress; }
  get birthYear() { return this.postForm.controls.birthYear; }
  get phoneNumber() { return this.postForm.controls.phoneNumber; }
  get province() { return this.postForm.controls.province; }
  get streetInformation() { return this.postForm.controls.streetInformation; }
  get registrationPeriod() { return this.postForm.controls.registrationPeriod; }
  get item() {
    return this.bookcheludService;
  }

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    this.idcoureaffi = JSON.parse(localStorage.getItem('idcoureaffi'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      var loadatainfo = JSON.parse(localStorage.getItem('information'));
      this.activaRoute.params.subscribe((params) => {
        localStorage.setItem('idcoureaffi', JSON.stringify(params.ID));
        this.bookcheludService.getCoureaffibyId(params.ID).subscribe((data) => {
          this.post.theCourseOfName = data.theCourseOfName;
          this.post.price = data.price;
          this.post.category = data.category;
          this.post.type = data.type;
          this.post.familyName = loadatainfo.familyName + loadatainfo.displayName;
          this.post.emailAddress = loadatainfo.emailAddress;
          this.post.birthYear = loadatainfo.birthYear;
          this.post.phoneNumber = loadatainfo.phoneNumber;
          this.post.province = loadatainfo.province;
          this.post.streetInformation = loadatainfo.streetInformation;
        });
      });
    }
    this.post.theCourseOfName
  }

  post: Bookchelud = new Bookchelud();
  time = new Date();

  async submit() {
    Object.keys(this.postForm.controls).forEach((key) => {
      this.postForm.controls[key].markAsDirty();
      this.postForm.controls[key].updateValueAndValidity();
    });
    if (this.postForm.invalid) {
      return;
    }
    Object.assign(this.item, this.postForm.value);
    ++this.item.step;
    var bookcheduleinformation = JSON.parse(localStorage.getItem('information'));
    var bookcheduleinfomationpost = JSON.parse(localStorage.getItem('idcoureaffi'));
    this.post.idMember = bookcheduleinformation.id;
    this.post.idCourseAffiliate = bookcheduleinfomationpost;
    this.post.imagePreviewTitle = bookcheduleinformation.avatar;
    var time = this.time.getHours() + ":" + this.time.getMinutes() + ":" + this.time.getSeconds();
    var date = this.time.getDate()+'-'+(this.time.getMonth()+1)+'-'+this.time.getFullYear();
    var fulltime = time + " " + date
    this.post.registrationPeriod = fulltime;
    const data = this.post;
    localStorage.setItem('bookchedule', JSON.stringify(data));
  }
}
