import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookcheludService } from '../../../../../service/bookchelud.service';

@Component({
  selector: 'app-bookschedule',
  templateUrl: './bookschedule.component.html',
  styleUrls: ['./bookschedule.component.less'],
  providers: [BookcheludService],
})
export class BookscheduleComponent implements OnInit {
  constructor(private srv: BookcheludService, private router: Router) {}

  get item() {
    return this.srv;
  }

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    }
  }
}
