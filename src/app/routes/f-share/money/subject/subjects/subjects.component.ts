import { Component, OnInit } from '@angular/core';
import { MoneyService } from '../../../../../service/money.service';
import { Interviews } from '../../../../../model/Interviews';
import { LandingPageService } from '../../../../../service/landing-page.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../../../../../service/account.service';


@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.less'],
})
export class SubjectsComponent implements OnInit {
  constructor(
    private moneyService: MoneyService,
    private landingPageService: LandingPageService,
    private leadingValidator: FormBuilder,
    private infomationServicestep1: AccountService
  ) {
    this.addInterviewForm = this.leadingValidator.group({
      familyName: ['', [Validators.required, Validators.minLength(4)]],
      emailAddress: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      birthYear: ['', Validators.required],
    });
  }

  get familyName() {
    return this.addInterviewForm.controls.familyName;
  }
  get emailAddress() {
    return this.addInterviewForm.controls.emailAddress;
  }
  get phoneNumber() {
    return this.addInterviewForm.controls.phoneNumber;
  }
  get birthYear() {
    return this.addInterviewForm.controls.birthYear;
  }
  get status() {
    return this.addInterviewForm.controls.status;
  }


  addInterviewForm: FormGroup;
  submitted = false;
  Interview = null;
  buttonClicked: any;
  subject = [];
  searchText;
  page: 4;

  infomationChangeform: FormGroup;
  informationChangestep1 = null;
  informationStep1 = null;
  account = null;

  ngOnInit() {
    this.moneyService.getAllMember().subscribe((data) => {
      this.subject = data;
    });
    this.showModal();
  }

  isVisible = false;

  showModal(): void {
    var check = localStorage.getItem('showmodal')
    if (check === '1') {
      this.isVisible = false;
    } else {
      this.isVisible = true;
    }
  }

  onSubmit() {
    Object.keys(this.addInterviewForm.controls).forEach((key) => {
      this.addInterviewForm.controls[key].markAsDirty();
      this.addInterviewForm.controls[key].updateValueAndValidity();
    });
    if (this.addInterviewForm.invalid) {
      return;
    }
    this.Interview.status = 0;
    this.landingPageService.addInterview(this.Interview).subscribe((data) => {
      this.isVisible = false;
    });
  }

  // không xem tab con nữa
  untilmodal() {
    localStorage.setItem('showmodal', '1');
    this.isVisible = false;
  }
  cancel(){
    this.isVisible = false;
  }
}
