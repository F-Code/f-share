import { Component, TemplateRef } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'shopping-cart1',
  template: `
  <ng-template #template>
      <div class="ant-notification-notice-content">
          <div class="ant-notification-notice-with-icon">
          <span class="ant-notification-notice-icon"><i class="fas fa-sign-in-alt"></i></span>
              <div class="ant-notification-notice-message">Vui lòng đăng nhập tài khoản</div>
          </div>
      </div>
  </ng-template>
  <div class="mid-header pull-right">
    <div class="widget sw_top-3 sw_top pull-left">
      <div class="widget-inner">
        <div class="top-form top-form-minicart etrostore-minicart pull-right">
          <div class="top-minicart-icon pull-right">
            <i class="fa fa-shopping-cart"></i>
            <a
              class="cart-contents"
              routerLink="/money/subject"
              title="View your shopping cart"
            >
              <span class="minicart-number">{{ lengthorder }}</span>
            </a>
          </div>

          <div class="wrapp-minicart">
            <div class="minicart-padding">
              <div class="number-item">
                Hiện có <span>{{ lengthorder }} sản phẩm</span> trong giỏ hàng của bạn
              </div>
              <form nz-form [formGroup]="form">
              <ul class="minicart-content">
              <li *ngFor="let item of items.controls; let i = index">
                <div class="container-left">
                  <a routerLink="/money/subject" class="product-image">
                    <img
                      [src]="items.value[i].imagePreviewTitle"
                      class="product-image-detail"
                    />
                  </a>
                </div>
                <div style="width: 128px;" class="container-right">
                  <h4>
                    <a class="title-item" routerLink="/money/subject">{{
                      items.value[i].name
                    }}</a>
                  </h4>

                  <div class="product-price">
                    <span class="price">
                      {{ items.value[i].xprice | currency:"VND"}} VNĐ
                    </span>
                  </div>
                  <a nz-button nzType="primary" (click)="del(i)">Xoá</a>
                </div>
              </li>
            </ul>
            </form>
              <div style=" padding: 20px 0px 0px 0px; " class="cart-checkout">
                <div class="price-total">
                  <span class="label-price-total">Tổng tiền</span>

                  <span class="price-total-w">
                    <span class="price"> {{ sum | currency:"VND"}} VNĐ </span>
                  </span>
                </div>

                <div class="cart-links">
                  <div class="checkout-link">
                    <a (click)="checkaccount(template)" title="Pay">Xem giỏ hàng</a>
                  </div>
                  <div class="cart-link">
                    <a
                      (click)="deleteallorder()"
                      >Xoá hết</a
                    >
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  `,
  styles: [
    `
      .container-left {
        width: auto;
      }

      .container-right {
        float: right;
        margin-top: -33%;
      }
      .product-image-detail {
        height: 100px;
        width: 140px;
      }
      .fa-sign-in-alt{
        color: #ff4343;
      }
    `,
  ],
})
export class ShoppingCartComponent1 {
  constructor(
    private notification: NzNotificationService,
    private fb: FormBuilder, 
    private router: Router,
    ) {}

  products = [];
  lengthorder = null;
  sum = 0;
  form: FormGroup;

  ngOnInit() {
    this.products = JSON.parse(localStorage.getItem('addtocard'));
    if(this.products === null){
      this.lengthorder = 9999
    }
    if (this.lengthorder === 9999) {
      this.lengthorder = 0
      localStorage.setItem('addtocard', JSON.stringify([]));
      localStorage.setItem('addtocardxprice', JSON.stringify([]));
      this.form = this.fb.group({
        items: this.fb.array([]),
      });
    } else {
      this.products = JSON.parse(localStorage.getItem('addtocard'));
      this.lengthorder = this.products.length;
      this.sum = 0;
      var account = JSON.parse(localStorage.getItem('addtocardxprice'));
      for (var i = 0; i < account.length; i++) {
        this.sum += account[i];
      }
      this.form = this.fb.group({
        items: this.fb.array([]),
      });
      const userList = this.products
      userList.forEach((i) => {
        const field = this.createUser();
        field.patchValue(i);
        this.items.push(field);
      });
    }
  }

  createUser(): FormGroup {
    return this.fb.group({
      imagePreviewTitle: [null],
      name: [null],
      xprice: [null],
    });
  }

  get items() {
    return this.form.controls.items as FormArray;
  }

  del(index) {
    this.items.removeAt(index);
    var account = JSON.parse(localStorage.getItem('addtocard'));
    var accountprice = JSON.parse(localStorage.getItem('addtocardxprice'));
    const removed = account.splice(index, 1);
    const removedprice = accountprice.splice(index, 1);
    localStorage.setItem('addtocard', JSON.stringify(account));
    localStorage.setItem('addtocardxprice', JSON.stringify(accountprice));
    this.ngOnInit();
  }

  deleteallorder() {
    var addtocard = JSON.parse(localStorage.getItem('addtocard'));
    var addtocardxprice = JSON.parse(localStorage.getItem('addtocardxprice'));
    if(addtocardxprice.length > 0 && addtocard.length > 0){
      let conf = confirm('Bạn muốn xoá hết ??');
      if (conf == true) {
        localStorage.setItem('addtocard', JSON.stringify([]));
        localStorage.setItem('addtocardxprice', JSON.stringify([]));
        this.ngOnInit();
      }
    }
  }
  checkaccount(template: TemplateRef<{}>){
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.notification.template(template);
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      this.router.navigate(['/money/checkout']);
    }
  }
}
