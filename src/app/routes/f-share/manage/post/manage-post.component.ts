import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Courseaffiliate } from '../../../../model/Courseaffiliate';
import { ManageService } from '../../../../service/manage.service';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
} from 'ng-zorro-antd/table';

export interface Filter {
  nameTitletable: string;
  sortFn?: NzTableSortFn;
  filterTable?: NzTableFilterList;
  filterFn?: NzTableFilterFn;
}

@Component({
  selector: 'app-manage-post',
  templateUrl: './manage-post.component.html',
  styleUrls: ['./manage-post.component.less'],
})
export class ManagePostComponent implements OnInit {
  constructor(
    private manageService: ManageService,
    private router: Router,
  ) { }

  Courseaffiliate = [];

  trackByName(_: number, item: Filter): string {
    return item.nameTitletable;
  }

  Courseaffiliates = null;
  updateCoureaffi = null;

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      function compareValues(key, order = 'asc') {
        return function (a, b) {
          const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
          const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

          let comparison = 0;
          if (varA < varB) {
            comparison = 1;
          } else if (varA > varB) {
            comparison = -1;
          }
          return order === 'desc' ? comparison * -1 : comparison;
        };
      }
      var infomationadmin = JSON.parse(localStorage.getItem('information'));
      if (infomationadmin.centralRight === 1) {
        var accountcheck = JSON.parse(window.localStorage.getItem('information'));
        this.manageService.getAllCourseaffiliateId(accountcheck.id).subscribe((data) => {
          data.sort(compareValues('registrationPeriod', 'asc'));
          this.Courseaffiliate = data;
          this.showtitle = data;
        });
      } else {
        this.router.navigate(['/f-learn.poly/home']);
      }
    }
  }

  deletePost(id) {
    let conf = confirm('Bạn muốn xoá khoá học này ???');
    if (conf == true) {
      this.manageService.getCourseaffiliateSeclectById(id).subscribe((data) => {
        this.updateCoureaffi = data;
        this.updateCoureaffi.checkdelete = 1;
        this.updateCoureaffi.status = false;
        this.manageService.updateCourseaffiliate(this.updateCoureaffi).subscribe((data) => {
          this.ngOnInit();
        });
      });
    }
  }

  filterData: Filter[] = [
    {
      nameTitletable: 'Tên khoá học',
    },
    {
      nameTitletable: 'Giá',
      sortFn: (a: Courseaffiliate, b: Courseaffiliate) => a.price - b.price,
    },
    {
      nameTitletable: 'Thể loại',
      filterTable: [
        { text: 'JAVA', value: 'JAVA' },
        { text: 'PHP', value: 'PHP' },
        { text: 'C++', value: 'C++' },
        { text: 'SQL', value: 'SQL' },
      ],
      filterFn: (list: string[], item: Courseaffiliate) =>
        list.some(
          (listLanguage) => item.category.indexOf(listLanguage) !== -1
        ),
    },
    {
      nameTitletable: 'Thời gian học',
    },
    {
      nameTitletable: 'Trạng thái',
      filterTable: [
        { text: 'Đã duyệt', value: true },
        { text: 'Chưa duyệt', value: false },
      ],
      filterFn: (list: string[], item: Courseaffiliate) => list.some((name) => item.status.toString().indexOf(name) !== -1),
    },
  ];

  isVisible = false;

  showModal(postId) {
    this.isVisible = true;
    this.manageService.getCourseaffiliateSeclectById(postId).subscribe((data) => {
      this.Courseaffiliates = data;
    });
  }
  handleOk() {
    this.isVisible = false;
  }

  isVisiblecheck = false;
  showtitle = null;

  showModalcheck(id): void {
    this.isVisiblecheck = true;
      this.manageService.getCourseaffiliateSeclectById(id).subscribe((data) => {
        this.showtitle = data;
      });
  }
  handleOkcheck(): void {
    this.isVisiblecheck = false;
  }
}