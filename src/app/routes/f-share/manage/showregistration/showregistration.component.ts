import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ManageService } from '../../../../service/manage.service';
import { Bookchelud } from '../../../../model/Bookchelud';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
} from 'ng-zorro-antd/table';
interface ColumnItem {
  name: string;
  sortFn?: NzTableSortFn;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn;
}


@Component({
  selector: 'app-showregistration',
  templateUrl: './showregistration.component.html',
  styleUrls: ['./showregistration.component.less']
})
export class ShowregistrationComponent implements OnInit {

  constructor(
    private actionroute: ActivatedRoute,
    private manageService: ManageService,
    private router: Router,
    private activaRoute: ActivatedRoute,
    private postManageservice: ManageService,
  ) { }

  ngOnInit(): void {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      var checkthu3 = JSON.parse(localStorage.getItem('information'));
      if (checkthu3.centralRight === 0) {
        this.router.navigate(['/f-learn.poly/home']);
      } else {
        function compareValues(key, order = 'asc') {
          return function (a, b) {
            const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
            const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];
    
            let comparison = 0;
            if (varA < varB) {
              comparison = 1;
            } else if (varA > varB) {
              comparison = -1;
            }
            return order === 'desc' ? comparison * -1 : comparison;
          };
        }
        this.activaRoute.params.subscribe((params) => {
          this.postManageservice.getAllregistrationId(params.ID).subscribe((data) => {
            data.sort(compareValues('registrationPeriod', 'asc'));
            this.listOfData = data;
            // this.book = data
            this.postManageservice.getCourseaffiliateSeclectById(params.ID).subscribe((data) => {
              this.updatecheckcoure = data;
              var updateregistration = 0;
              this.updatecheckcoure.check = updateregistration;
              this.updatecheckcoure.status = data.status;
              this.postManageservice.updateCourseaffiliate(this.updatecheckcoure).subscribe((data) => {
              });
            });
          });
        });
      }
    }
  }

  listOfData = null;
  updatecheckcoure = null;
  posts = null;

  deletePost(id) {
    let conf = confirm('You want to delete ???');
    if (conf == true) {
      this.manageService.removeCourseaffiliate(id).subscribe((response) => {
      });
    }
  }

  listOfColumns: ColumnItem[] = [
    {
      name: 'Trạng thái',
      listOfFilter: [
        { text: 'Chưa xem', value: '0' },
        { text: 'Đã xem', value: '1' },
      ],
      filterFn: (list: string[], item: Bookchelud) => list.some((name) => item.status.toString().indexOf(name) !== -1),
    },
    {
      name: 'Ảnh',
    },
    {
      name: 'Họ tên',
    },
    {
      name: 'Số điện thoại',
    },
    {
      name: 'Ngày sinh',
    },
    {
      name: 'Email',
    },
  ];

  trackByName(_: number, item: ColumnItem): string {
    return item.name;
  }
  // isVisible2 = false;
  // book = null;

  // showModal2(id): void {
  //   this.isVisible2 = true;
  //   this.postManageservice.showregistrationId(id).subscribe((data) => {
  //     this.book = data;
  //     var updateregistration = 1;
  //     if (this.book.status === 0){
  //       this.book.status = updateregistration;
  //       // this.postManageservice.updateRegistration(this.book).subscribe((data) => {
  //       //   this.book = data;
  //       // });
  //       this.ngOnInit();
  //       console.log('1s')
  //       console.log(this.book.status)
  //     }
  //   });
  // }
  // cancel2() {
  //   this.isVisible2 = false;
  // }
}