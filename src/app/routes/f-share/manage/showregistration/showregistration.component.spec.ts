import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowregistrationComponent } from './showregistration.component';

describe('ShowregistrationComponent', () => {
  let component: ShowregistrationComponent;
  let fixture: ComponentFixture<ShowregistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowregistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowregistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
