import { Component, OnInit, TemplateRef } from '@angular/core';
import { Courseaffiliate } from '../../../../model/Courseaffiliate';
import { ActivatedRoute, Router } from '@angular/router';
import { ManageService } from '../../../../service/manage.service';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { UploadImgService } from '../../../upload-img.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.less'],
})
export class PostEditComponent implements OnInit {
  constructor(
    private activaRoute: ActivatedRoute,
    private postManageservice: ManageService,
    private uploadImgService: UploadImgService,
    private notification: NzNotificationService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  postForm = new FormGroup({
    id: new FormControl(null),
    idMember: new FormControl('', [
      Validators.required,
    ]),
    theCourseOfName: new FormControl('', [
      Validators.required,
    ]),
    imagePreviewTitle: new FormControl('', [
    ]),
    participantOfNumber: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]*'),
    ]),
    shortDescription: new FormControl('', [
      Validators.required,
      Validators.maxLength(400),
    ]),
    longDescription: new FormControl('', [
      Validators.required,
      Validators.maxLength(1000),
    ]),
    curriculum: new FormControl('', [
      Validators.required,
    ]),
    category: new FormControl('', [
      Validators.required,
    ]),
    itemsOfNumber: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]*'),
    ]),
    totalStudyTime: new FormControl('', [
      Validators.required,
    ]),
    price: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]*'),
    ]),
    feedback: new FormControl('', [
      Validators.required,
    ]),
    type: new FormControl('', [
      Validators.required,
    ]),
    status: new FormControl('', [
      Validators.required,
    ]),
    imagesList: new FormControl('', [
    ]),
    registrationPeriod: new FormControl('', [
    ]),
    check: new FormControl('', [
    ]),
    checkdelete: new FormControl('', [
    ]),
    level: new FormControl('', [
    ]),
  });

  get idMember() { return this.postForm.controls.idMember; }
  get theCourseOfName() { return this.postForm.controls.theCourseOfName; }
  get imagePreviewTitle() { return this.postForm.controls.imagePreviewTitle; }
  get participantOfNumber() { return this.postForm.controls.participantOfNumber; }
  get shortDescription() { return this.postForm.controls.shortDescription; }
  get longDescription() { return this.postForm.controls.longDescription; }
  get curriculum() { return this.postForm.controls.curriculum; }
  get category() { return this.postForm.controls.category; }
  get itemsOfNumber() { return this.postForm.controls.itemsOfNumber; }
  get totalStudyTime() { return this.postForm.controls.totalStudyTime; }
  get price() { return this.postForm.controls.price; }
  get feedback() { return this.postForm.controls.feedback; }
  get type() { return this.postForm.controls.type; }
  get status() { return this.postForm.controls.status; }
  get imagesList() { return this.postForm.controls.imagesList; }
  get registrationPeriod() { return this.postForm.controls.registrationPeriod; }
  get check() { return this.postForm.controls.check; }
  get checkdelete() { return this.postForm.controls.checkdelete; }
  get level() { return this.postForm.controls.level; }

  post: Courseaffiliate = new Courseaffiliate();
  subjects = [];
  listImage = [];
  imagenotchange = null;
  imagenotchangelist: [];
  checkrunfuntionimage = 0;
  checkrunfuntionlistimage = 0;

  form = new FormGroup({
    items: new FormControl('', [
    ]),
  });

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      var infomationadmin = JSON.parse(localStorage.getItem('information'));
      if (infomationadmin.centralRight === 1) {
        this.activaRoute.params.subscribe((params) => {
          this.postManageservice.getCourseaffiliateSeclectById(params.ID).subscribe((data) => {
              this.post = data;
              this.postForm.setValue(data);
              localStorage.setItem('imagenotchange', JSON.stringify(this.postForm.value.imagePreviewTitle));
              localStorage.setItem('imagenotchangelist', JSON.stringify(this.postForm.value.imagesList));
              if (this.link === undefined) {
                this.link = this.postForm.value.imagePreviewTitle;
                this.link2 = this.postForm.value.imagesList;
                this.listImage = this.postForm.value.imagesList;
                this.form = this.fb.group({
                  items: this.fb.array([]),
                });
                const userList = this.listImage;
                userList.forEach((i) => {
                  const field = this.createUser();
                  field.patchValue(i);
                  this.items.push(field);
                });
              }
            });
        });
      } else {
        this.router.navigate(['/f-learn.poly/home']);
      }
    }
  }

  loading = false;

  async submit(template: TemplateRef<{}>) {
    Object.keys(this.postForm.controls).forEach((key) => {
      this.postForm.controls[key].markAsDirty();
      this.postForm.controls[key].updateValueAndValidity();
    });
    if (this.postForm.invalid) {
      return;
    }
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 10000);
    if (this.checkrunfuntionimage === 0 && this.checkrunfuntionlistimage === 0) {
      this.imagenotchange = JSON.parse(localStorage.getItem('imagenotchange'));
      this.imagenotchangelist = JSON.parse(localStorage.getItem('imagenotchangelist'));
      this.post.imagePreviewTitle = this.imagenotchange;
      this.post.imagesList = this.imagenotchangelist;
      this.post.status = false;
      this.postManageservice.updateCourseaffiliate(this.post).subscribe((data) => {
        this.notification.template(template);
        this.router.navigate([`/money/manage-post`]);
        localStorage.removeItem('imagenotchange');
        localStorage.removeItem('imagenotchangelist');
      });
    } 
    else if (this.checkrunfuntionimage === 1 && this.checkrunfuntionlistimage === 2) {
      const image_res = await this.uploadImgService.uploadImage(this.imageFile);
      localStorage.setItem('uploadimage', JSON.stringify(image_res));
      var image = JSON.parse(localStorage.getItem('uploadimage'));
      this.post.imagePreviewTitle = image.data.link;
      this.post.imagesList = this.listImage;
      this.post.status = false;
      this.postManageservice.updateCourseaffiliate(this.post).subscribe((data) => {
        this.notification.template(template);
        this.router.navigate([`/money/manage-post`]);
        localStorage.removeItem('uploadimage');
        localStorage.removeItem('uploadimage2');
      });
    } else if (this.checkrunfuntionimage === 1) {
      const image_res = await this.uploadImgService.uploadImage(this.imageFile);
      localStorage.setItem('uploadimage', JSON.stringify(image_res));
      var image = JSON.parse(localStorage.getItem('uploadimage'));
      this.post.imagePreviewTitle = image.data.link;
      this.imagenotchangelist = JSON.parse(localStorage.getItem('imagenotchangelist'));
      this.post.imagesList = this.imagenotchangelist;
      this.post.status = false;
      this.postManageservice.updateCourseaffiliate(this.post).subscribe((data) => {
        this.notification.template(template);
        this.router.navigate([`/money/manage-post`]);
        localStorage.removeItem('uploadimage');
        localStorage.removeItem('imagenotchangelist');
      });
    } else if (this.checkrunfuntionlistimage === 2) {
      this.imagenotchange = JSON.parse(localStorage.getItem('imagenotchange'));
      this.post.imagePreviewTitle = this.imagenotchange;
      this.post.imagesList = this.listImage;
      this.post.status = false;
      this.postManageservice.updateCourseaffiliate(this.post).subscribe((data) => {
        this.notification.template(template);
        this.router.navigate([`/money/manage-post`]);
        localStorage.removeItem('uploadimage2');
        localStorage.removeItem('imagenotchange');
      });
    }
  }

  imageFile: File;
  link: string | ArrayBuffer;

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.imageFile = event.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.link = event.target.result;
      };
      this.checkrunfuntionimage = 1;
    }
  }

  imageFile2: File;
  link2: string | ArrayBuffer;

  async onSelectFile2(event) {
    if (event.target.files && event.target.files[0]) {
      this.imageFile2 = event.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.link2 = event.target.result;
      };
      this.checkrunfuntionlistimage = 2;
      const image_res = await this.uploadImgService.uploadImage(
        this.imageFile2
      );
      localStorage.setItem('uploadimage2', JSON.stringify(image_res));
      var image = JSON.parse(localStorage.getItem('uploadimage2'));
      this.listImage.push(image.data.link);
      localStorage.setItem('imagehangedelete', JSON.stringify(this.listImage));
      var imagedelete = JSON.parse(localStorage.getItem('imagehangedelete'));
      this.listImage = imagedelete;
      this.form = this.fb.group({
        items: this.fb.array([]),
      });
      const userList = this.listImage;
      userList.forEach((i) => {
        const field = this.createUser();
        field.patchValue(i);
        this.items.push(field);
      });
    }
  }

  createUser(): FormGroup {
    return this.fb.group({
      listimagechange: [null, [Validators.required]],
    });
  }

  get items() {
    return this.form.controls.items as FormArray;
  }

  del(index) {
    this.checkrunfuntionlistimage = 2;
    this.items.removeAt(index);
    const removed = this.listImage.splice(index, 1);
    localStorage.setItem('imagehangedelete', JSON.stringify(this.listImage));
    var imagedelete = JSON.parse(localStorage.getItem('imagehangedelete'));
    this.listImage = imagedelete;
    this.form = this.fb.group({
      items: this.fb.array([]),
    });
    const userList = this.listImage;
    userList.forEach((i) => {
      const field = this.createUser();
      field.patchValue(i);
      this.items.push(field);
    });
  }
}
