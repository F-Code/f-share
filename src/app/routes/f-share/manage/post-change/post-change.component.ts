import { Component, OnInit, TemplateRef } from '@angular/core';
import { Courseaffiliate } from '../../../../model/Courseaffiliate';
import { Router } from '@angular/router';
import { ManageService } from '../../../../service/manage.service';
import { AccountService } from '../../../../service/account.service';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { UploadImgService } from '../../../upload-img.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { NzUploadChangeParam } from 'ng-zorro-antd/upload';

@Component({
  selector: 'app-post-change',
  templateUrl: './post-change.component.html',
  styleUrls: ['./post-change.component.less'],
})
export class PostChangeComponent implements OnInit {
  constructor(
    private manageService: ManageService,
    private accountService: AccountService,
    private uploadImgService: UploadImgService,
    private router: Router,
    private notification: NzNotificationService,
    private msg: NzMessageService,
    private fb: FormBuilder
  ) { }

  postForm = new FormGroup({
    id: new FormControl(null),
    idMember: new FormControl('', [
    ]),
    feedback: new FormControl('', [
    ]),
    status: new FormControl('', [
    ]),
    theCourseOfName: new FormControl('', [
      Validators.required,
    ]),
    imagePreviewTitle: new FormControl('', [
      Validators.required,
    ]),
    participantOfNumber: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]*'),
    ]),
    shortDescription: new FormControl('', [
      Validators.required,
      Validators.maxLength(400),
    ]),
    longDescription: new FormControl('', [
      Validators.required,
      Validators.maxLength(1000),
    ]),
    curriculum: new FormControl('', [
      Validators.required,
    ]),
    category: new FormControl('', [
      Validators.required,
    ]),
    itemsOfNumber: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]*'),
    ]),
    totalStudyTime: new FormControl('', [
      Validators.required,
    ]),
    price: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]*'),
    ]),
    type: new FormControl('', [
      Validators.required,
    ]),
    imagesList: new FormControl('', [
      Validators.required,
    ]),
    level: new FormControl('', [
      Validators.required,
    ]),
    registrationPeriod: new FormControl('', [
    ]),
    check: new FormControl('', [
    ]),
    checkdelete: new FormControl('', [
    ]),
  });

  get idMember() { return this.postForm.controls.idMember; }
  get theCourseOfName() { return this.postForm.controls.theCourseOfName; }
  get imagePreviewTitle() { return this.postForm.controls.imagePreviewTitle; }
  get participantOfNumber() { return this.postForm.controls.participantOfNumber; }
  get shortDescription() { return this.postForm.controls.shortDescription; }
  get longDescription() { return this.postForm.controls.longDescription; }
  get curriculum() { return this.postForm.controls.curriculum; }
  get category() { return this.postForm.controls.category; }
  get itemsOfNumber() { return this.postForm.controls.itemsOfNumber; }
  get totalStudyTime() { return this.postForm.controls.totalStudyTime; }
  get price() { return this.postForm.controls.price; }
  get feedback() { return this.postForm.controls.feedback; }
  get type() { return this.postForm.controls.type; }
  get status() { return this.postForm.controls.status; }
  get imagesList() { return this.postForm.controls.imagesList; }
  get registrationPeriod() { return this.postForm.controls.registrationPeriod; }
  get check() { return this.postForm.controls.check; }
  get checkdelete() { return this.postForm.controls.checkdelete; }
  get level() { return this.postForm.controls.level; }

  subjects = [];
  listImage = [];
  post: Courseaffiliate = new Courseaffiliate();

  ngOnInit() {
    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
    console.log(time + " " + date)
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/f-learn.poly/login']);
    } else {
      var infomationadmin = JSON.parse(localStorage.getItem('information'));
      if (infomationadmin.centralRight === 1) {
        if (this.link === undefined) {
          this.link = "https://imttrade.com/wp-content/uploads/2016/12/white-background-2.jpg";
          this.link2 = "https://imttrade.com/wp-content/uploads/2016/12/white-background-2.jpg";
          this.form = this.fb.group({
            items: this.fb.array([]),
          });
          const userList = this.listImage;
          userList.forEach((i) => {
            const field = this.createUser();
            field.patchValue(i);
            this.items.push(field);
          });
        }
      } else {
        this.router.navigate(['/f-learn.poly/home']);
      }
    }
  }

  loading = false;
  checkmoney = 100000;
  chengemoneyuser = null;;
  time = new Date();

  submit() {
    var informaitonid = JSON.parse(localStorage.getItem('information'));
    Object.keys(this.postForm.controls).forEach((key) => {
      this.postForm.controls[key].markAsDirty();
      this.postForm.controls[key].updateValueAndValidity();
    });
    if (this.postForm.invalid) {
      return;
    }
    if (informaitonid.moneyIntalent - this.checkmoney < 0) {
      this.showModal1();
    } else {
      this.showModal2();
    }
  }

  async doneupcoureaff(template: TemplateRef<{}>){
    this.isVisible2 = false;
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 5000);
    const image_res = await this.uploadImgService.uploadImage(this.imageFile);
    localStorage.setItem('uploadimage', JSON.stringify(image_res));
    var image = JSON.parse(localStorage.getItem('uploadimage'));
    this.post.imagePreviewTitle = image.data.link;
    this.post.imagesList = this.listImage;
    var informaitonid = JSON.parse(localStorage.getItem('information'));
    this.chengemoneyuser = informaitonid;
    this.chengemoneyuser.moneyIntalent = informaitonid.moneyIntalent - this.checkmoney
    this.accountService.updateAccount(this.chengemoneyuser).subscribe((data) => {
      localStorage.setItem('information', JSON.stringify(data));
    });
    this.post.idMember = informaitonid.id;
    var time = this.time.getHours() + ":" + this.time.getMinutes() + ":" + this.time.getSeconds();
    var date = this.time.getDate()+'-'+(this.time.getMonth()+1)+'-'+this.time.getFullYear();
    var fulltime = time + " " + date
    this.post.registrationPeriod = fulltime;
    this.post.feedback = "Thông tin khoá học đang được kiểm tra vui lòng chờ bài đăng hoàn thành kiểm tra";
    this.manageService.addCourseaffiliate(this.post).subscribe((data) => {
      this.notification.template(template);
      this.router.navigate([`/money/manage-post`]);
    });
    localStorage.removeItem('uploadimage');
    localStorage.removeItem('uploadimage2');
  }

  imageFile: File;
  link: string | ArrayBuffer;

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.imageFile = event.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.link = event.target.result;
      };
    }
  }

  imageFile2: File;
  link2: string | ArrayBuffer;

  async onSelectFile2(event) {
    if (event.target.files && event.target.files[0]) {
      this.imageFile2 = event.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        this.link2 = event.target.result;
      };
      const image_res = await this.uploadImgService.uploadImage(
        this.imageFile2
      );
      localStorage.setItem('uploadimage2', JSON.stringify(image_res));
      var image = JSON.parse(localStorage.getItem('uploadimage2'));
      this.listImage.push(image.data.link);
    }
  }

  isVisible1 = false;

  showModal1(): void {
      this.isVisible1 = true;
  }
  cancel(){
    this.isVisible1 = false;
  }

  isVisible2 = false;

  showModal2(): void {
      this.isVisible2 = true;
  }
  cancel2(){
    this.isVisible2 = false;
  }

  form = new FormGroup({
    items: new FormControl('', [
    ]),
  });

  createUser(): FormGroup {
    return this.fb.group({
      listimagechange: [null, [Validators.required]],
    });
  }

  get items() {
    return this.form.controls.items as FormArray;
  }

  del(index) {
    this.items.removeAt(index);
    const removed = this.listImage.splice(index, 1);
    localStorage.setItem('imagehangedelete', JSON.stringify(this.listImage));
    var imagedelete = JSON.parse(localStorage.getItem('imagehangedelete'));
    this.listImage = imagedelete;
    this.form = this.fb.group({
      items: this.fb.array([]),
    });
    const userList = this.listImage;
    userList.forEach((i) => {
      const field = this.createUser();
      field.patchValue(i);
      this.items.push(field);
    });
  }
}
