import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDetairegistrationComponent } from './show-detairegistration.component';

describe('ShowDetairegistrationComponent', () => {
  let component: ShowDetairegistrationComponent;
  let fixture: ComponentFixture<ShowDetairegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDetairegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDetairegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
