import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ManageService } from '../../../../service/manage.service';
import {
  NzTableFilterFn,
  NzTableFilterList,
  NzTableSortFn,
} from 'ng-zorro-antd/table';

interface ColumnItem {
  name: string;
  sortFn?: NzTableSortFn;
  listOfFilter?: NzTableFilterList;
  filterFn?: NzTableFilterFn;
}

@Component({
  selector: 'app-show-detairegistration',
  templateUrl: './show-detairegistration.component.html',
  styleUrls: ['./show-detairegistration.component.less']
})
export class ShowDetairegistrationComponent implements OnInit {

  constructor(
    private manageService: ManageService,
    private router: Router,
    private activaRoute: ActivatedRoute,
    private postManageservice: ManageService,
  ) { }
  listOfData = [];
  book = null;

  ngOnInit(): void {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account == null) {
      this.router.navigate(['/passport/login']);
    } else {
      this.activaRoute.params.subscribe((params) => {
        this.postManageservice.showregistrationId(params.ID).subscribe((data) => {
          this.book = data;
          var updateregistration = 1;
          this.book.status = updateregistration;
          this.postManageservice.updateRegistration(this.book).subscribe((data) => {
            this.book = data;
          });
        });
      });
    }
  }

  posts = null;

  deletePost(id) {
    let conf = confirm('You want to delete ???');
    if (conf == true) {
      this.manageService.removeCourseaffiliate(id).subscribe((response) => {
      });
    }
  }

  listOfColumns: ColumnItem[] = [
    {
      name: 'Product is name',
      //   sortFn: (a: Bookchelud, b: Bookchelud) => a.name.localeCompare(b.name),
      //   listOfFilter: [
      //     { text: 'Joe', value: 'Joe' },
      //     { text: 'Jim', value: 'Jim' },
      //   ],
      //   filterFn: (list: string[], item: Bookchelud) =>
      //     list.some((name) => item.name.indexOf(name) !== -1),
    },
    {
      name: 'Image show',
    },
    {
      name: 'Price',
      // sortFn: (a: Bookchelud, b: Bookchelud) => a.xprice - b.xprice,
    },
    {
      name: 'Species',
      // listOfFilter: [
      //   { text: 'Joe', value: 'Joe' },
      //   { text: 'Jim', value: 'Jim' },
      // ],
      // filterFn: (list: string[], item: Bookchelud) =>
      //   list.some(
      //     (listLanguage) => item.listLanguage.indexOf(listLanguage) !== -1
      //   ),
    },
    {
      name: 'Capacity',
    },
  ];

  trackByName(_: number, item: ColumnItem): string {
    return item.name;
  }
}