import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../../../service/account.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd';


@Component({
  selector: 'passport-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
})
export class LoginComponent implements OnInit {
  constructor(
    private loginValidator: FormBuilder,
    private loginService: AccountService,
    private notification: NzNotificationService,

    private router: Router,
  ) {
    this.loginForm = loginValidator.group({
      remember: [true],
      password: [null,
        [
          Validators.required,
        ]
      ],
      displayNameOrEmailAddress: [null,
        [
          Validators.required,
        ]
      ],
    });
  }

  get displayNameOrEmailAddress() {
    return this.loginForm.controls.displayNameOrEmailAddress;
  }
  get password() {
    return this.loginForm.controls.password;
  }

  loginForm: FormGroup;
  public pass;

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account != null) {
      this.router.navigate(['/f-learn.poly/home']);
    }
  }

  // đăng nhập
  submit() {
    Object.keys(this.loginForm.controls).forEach((key) => {
      this.loginForm.controls[key].markAsDirty();
      this.loginForm.controls[key].updateValueAndValidity();
    });
    if (this.loginForm.invalid) {
      return;
    }
    this.pass = <HTMLInputElement>document.getElementById('password')['value'];
    window.localStorage.setItem('pass', JSON.stringify(this.pass));
    const dataLogin = this.loginForm.value;
    this.loginService.userLogin(dataLogin).subscribe((data) => {
      this.router.navigate(['/f-learn.poly/home']);
      window.localStorage.setItem('account', JSON.stringify(dataLogin));
      window.localStorage.setItem('information', JSON.stringify(data));
      window.localStorage.setItem('showmodal', '0');
      },
      (err) => {
        if (err.status == 404) {
          this.notification.create('error', 'Đăng nhập thất bại!', 'Thông tin tài khoản hoặc mật khẩu không chính xác!');
        }
        if (err.status == 401) {
          this.notification.create('error', 'Đăng nhập thất bại!', 'Tài khoản của bạn đã bị khoá.');
        }
      },
    );
  }
}
