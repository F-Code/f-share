import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { LockService } from '../../../service/lock.service';
import { Member } from '../../../model/Member';
import { Forgot } from '../../../model/Forgot';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.less'],
  providers: [LockService],
})
export class ForgotPasswordComponent implements OnInit {
  private emailAdderss = [];
  guestByEmail: Member = new (class implements Member {
    id: number;
    familyName: string;
    emailAddress: string;
    password: string;
    phoneNumber: string;
    avatar: string;
    status: string;
    displayName: string;
    birthYear: string;
    province: string;
    streetInformation: string;
    position: string;
    summary: string;
    school: string;
    department: string;
    grade: string;
    moneyIntalent: number;
    centralRight: string;
  })();

  passwordVisible1 = false;
  passwordVisible2 = false;
  password?: string;

  forgot: Forgot = new (class implements Forgot {
    emailAddress: string;
    randomNumber: number;
  })();

  private email;

  validateForm!: FormGroup;
  validatePass!: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  submitPass(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }
  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() =>
      this.validatePass.controls.repassword.updateValueAndValidity()
    );
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validatePass.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  constructor(private fb: FormBuilder, private lockService: LockService) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      emailAddress: [null, [Validators.email, Validators.required]],
    });
    this.validatePass = this.fb.group({
      password: ['', [Validators.required]],
      repassword: ['', [this.confirmationValidator]],
    });
    this.lockService.getAllaccount().subscribe((data) => {
      for (let i = 0; i < data.length; i++) {
        this.emailAdderss.push(data[i].emailAddress);
      }
    });
    document.getElementById('notEmail').style.display = 'none';
  }

  account = [];
  // thay doi pass word
  xacNhanEmail() {
    var email = <HTMLInputElement>(
      document.getElementById('email')['value'].toString().trim()
    );
    if (!this.emailAdderss.includes(email)) {
      document.getElementById('notEmail').style.display = 'block';
      document.getElementById('notEmail').style.color = 'red';
    } else {
      this.lockService
        .getAccount(this.validateForm.value.emailAddress)
        .subscribe((data) => {
          this.account = data;
          localStorage.setItem('passwordforgot', JSON.stringify(data));
        });
      document.getElementById('form1').style.display = 'none';
      document.getElementById('form2').style.display = 'block';
    }
  }

  getText() {
    if ((document.getElementById('notEmail').style.display = 'block')) {
      document.getElementById('notEmail').style.display = 'none';
    }
  }

  // dung vay
  btnDungVay() {
    document.getElementById('form2').style.display = 'none';
    document.getElementById('form3').style.display = 'block';

    var datachange = this.validateForm.value;
    var randum = Math.floor(Math.random() * 100000 + 100000);
    this.forgot.emailAddress = datachange.emailAddress;
    this.forgot.randomNumber = randum;
    localStorage.setItem('randum', JSON.stringify(randum));
    this.lockService.userForgot(this.forgot).subscribe((data) => {});
  }

  btnKhongPhai() {
    document.getElementById('form2').style.display = 'none';
    document.getElementById('form1').style.display = 'block';
  }

  // btnXacNhan
  btnXacNhan() {
    var maXacNhan = document.getElementById('maXacNhan')['value'];

    if (this.forgot.randomNumber == maXacNhan) {
      document.getElementById('form3').style.display = 'none';
      document.getElementById('form4').style.display = 'block';
    } else {
      alert('Mã xác nhận không tồn tại');
    }
  }

  // doi mat khau
  btnDoiMatKhau() {
    var pass = document.getElementById('password')['value'];
    this.guestByEmail = JSON.parse(localStorage.getItem('passwordforgot'));
    this.guestByEmail.password = pass;
    this.lockService.updateAccount(this.guestByEmail).subscribe((data) => {
    });
    localStorage.removeItem('passwordforgot');
    localStorage.removeItem('randum');
    document.getElementById('form4').style.display = 'none';
    document.getElementById('form5').style.display = 'block';
  }
}
