import { Component, OnInit, TemplateRef } from '@angular/core';
import { Member } from '../../../model/Member';
import { Router } from '@angular/router';
import { AccountService } from '../../../service/account.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  constructor(
    private registerValidator: FormBuilder,
    private registerService: AccountService,
    private notification: NzNotificationService,
    private router: Router,
  ) {
    this.registerForm = registerValidator.group({
      emailAddress: [null, 
        [
          Validators.required, 
          Validators.email,
        ]
      ],
      password: [
        null,
        [
          Validators.required,
          Validators.minLength(6),
        ],
      ],
      displayName: [null, 
        [
          Validators.required,
        ]
      ],
      familyName: [null, 
        [
          Validators.required,
        ]
      ],
      phoneNumber: [
        null,
        [
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(10),
          Validators.pattern('[0-9]*'),
        ],
      ],
      confirm: [
        null,
        [
          Validators.required, 
          RegisterComponent.passwordEquar,
        ],
      ],
    });
  }

  get emailAddress() {
    return this.registerForm.controls.emailAddress;
  }
  get familyName() {
    return this.registerForm.controls.familyName;
  }
  get displayName() {
    return this.registerForm.controls.displayName;
  }
  get phoneNumber() {
    return this.registerForm.controls.phoneNumber;
  }
  get password() {
    return this.registerForm.controls.password;
  }
  get confirm() {
    return this.registerForm.controls.confirm;
  }

  register: Member = new Member();
  registerForm: FormGroup;
  sbackp = " ";

  ngOnInit() {
    var account = JSON.parse(localStorage.getItem('account'));
    if (account != null) {
      this.router.navigate(['/f-learn.poly/home']);
    }
  }
  
  // kiểm tra lại mật khẩu
  static passwordEquar(control: FormControl) {
    if (!control || !control.parent) {
      return null;
    }
    if (control.value !== control.parent.get('password').value) {
      return { equar: true };
    }
    return null;
  }

  // cắt dấu cách đầu và cuối
  checkTrimffamilyName() {
    this.register.familyName = this.register.familyName.trim();
  }
  checkTrimdisplayName() {
    this.register.displayName = this.register.displayName.trim();
  }
  checkTrimemailAddress() {
    this.register.emailAddress = this.register.emailAddress.trim();
  }
  checkTrimpassword() {
    this.register.password = this.register.password.trim();
  }
  checkTrimphone() {
    this.register.phoneNumber = this.register.phoneNumber.trim();
  }

  // đăng ký
  submit(template: TemplateRef<{}>) {
    Object.keys(this.registerForm.controls).forEach((key) => {
      this.registerForm.controls[key].markAsDirty();
      this.registerForm.controls[key].updateValueAndValidity();
    });
    if (this.registerForm.invalid) {
      return;
    }
    var disname = <HTMLInputElement>(
      document.getElementById('disnamee')['value'].toString()
    );
    this.register.displayName = " " + disname
    this.register.position = "Thành viên"
    this.register.avatar =
      'https://upload.wikimedia.org/wikipedia/commons/3/34/PICA.jpg';
    this.registerService.addAcount(this.register).subscribe((data) => {
      this.notification.template(template);
      this.router.navigateByUrl('f-learn.poly/login');
    },
    (err) => {
      if (err.status == 500) {
        this.notification.create('error', 'Đăng ký thất bại!', 'Email hoặc số điện thoại đã được sử dụng. Vui lòng nhập lại thông tin khác');
      }
    },
    );
  }
}
