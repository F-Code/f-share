export interface Notification{
    id: string;
    idMember: string;
    message: string;
    count: number;
}