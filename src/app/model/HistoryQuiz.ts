export interface HistoryQuiz {
  // id
  id: string;
  // idQuiz
  idQuiz: string;
  // idMember
  idMember: string;
  // diem
  point: number;
  // thoi gian lam bai
  durationQuiz: string;
  // lan choi
  timesPlayed: number;
}
