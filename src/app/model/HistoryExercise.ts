export interface HistoryExercise {
  // id
  id: string;
  // idExe
  idExercise: string;
  // idMember
  idMember: string;
  // diem
  point: number;
  // thoi gian choi
  durationExercise: string;
  // lan choi
  timesPlayed: number;
}
