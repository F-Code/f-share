export interface PostComment {
  id: string;
  idPost: string;
  content: string;
  reviewName: string;
  avatar: string;
  dateComment: string;
}
