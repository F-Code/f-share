export interface DiscussionComment {
  // id
  id: string;
  // id discus
  idDiscuss: string;
  // nội dung
  content: string;
  // ten nguoi cmt
  reviewName: string;
  // avata
  avatar: string;
  // ngay comment
  dateComment: string;
  like: number;
  dislike: number;
  idMember: string;
}
