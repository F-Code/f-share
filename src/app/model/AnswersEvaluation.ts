export interface AnswersEvaluation {
  id: string;

  // id người chơi
  idMember: string;

  // Id Evaluation
  idEvaluation: string;

  // Id Question
  idQuestionEvaluation: string;

  // Nội dung đáp án
  answerEvaluation: string;

  // lanchoi
  timesPlayed: number;

  // đúng/sai
  status: number;
}
