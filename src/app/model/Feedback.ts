export class Feedback {
  idCourseAffiliate: number;
  title: string;
  content: string;
  registrationPeriod: string;
}
