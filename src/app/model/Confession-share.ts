export interface ConfessionShare {
  id: string;
  idMember: string;
  content: string;
  status: boolean;
}
