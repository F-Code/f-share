export class Bookchelud {
    id: string;
    idMember: string;
    idCourseAffiliate: string;
    theCourseOfName: string;
    imagePreviewTitle: string;
    price: number;
    category: string;
    familyName: string;
    emailAddress: string;
    birthYear: string;
    phoneNumber: string;
    province: string;
    streetInformation: string;
    type: number;
    status: boolean;
    registrationPeriod: string;
}
  