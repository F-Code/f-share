export interface Sessions {
  id: string;
  idCourse: string;
  title: string;
  content: string;
}
