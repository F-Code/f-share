export class Deposithistory {
  familyName: string;
  emailAddress: string;
  phoneNumber: string;
  depositAmount: string;
  registrationPeriod: string;
}
