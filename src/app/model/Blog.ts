export interface Blog {
  // id
  id: string;
  // tiêu đề blog
  title: string;
  // ảnh blog
  picture: string;
  // nội dung
  content: string;
  // view
  view: number;
}
