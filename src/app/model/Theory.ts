export interface Theory {
  // id
  id: string;
  // id bai hoc
  idSession: string;
  // title
  title: string;
  // noi dung
  content: string;
  // huong dan
  tutorial: string;
}
