export interface Discussion {
  // id
  id: string;
  // tiêu đề blog
  title: string;
  // nội dung
  content: string;
  // avatar
  avatar: string;
  // name
  reviewName: string;
  // emailReviewName
  emailReviewName: string;
  // view
  view: number;
}
