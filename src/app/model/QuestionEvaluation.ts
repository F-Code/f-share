export interface QuestionEvaluation {
    // id
    id: string;
    // id idEvaluate
    idEvaluate: string;
  
    theQuestionOfName: string;
  
    content: string;
    answerA: string;
    answerB: string;
    answerC: string;
    answerD: string;
    correctAnswer: string;
  }
  