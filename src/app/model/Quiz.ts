export interface Quiz {
  // id Quiz
  id: string;
  // id session
  idSession: string;
  // reviewName
  reviewName: string;
}
