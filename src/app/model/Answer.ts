export interface Answer {
  // id
  id: string;
  // idMember
  idMember: string;
  // idQuestion
  idQuestion: string;
  // idQuiz
  idQuiz: string;
  // Dap an
  answerQuiz: string;
  // lan choi
  timesPlayed: number;
  // dung/sai
  status: number;
  // thoi gian
  durationQuiz: string;
}
