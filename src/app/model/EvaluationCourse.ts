export  interface EvaluationCourse {
  // ID danh sách khóa học đánh giá
  idEvaluationCourse: string;

  // Tên source code cần chia sẻ
  name: string;

  // link ảnh hiển thị
  imagePreviewTitle: string;

  // Giá tiền
  xprice: number;

  // danh sách ngôn ngữ sử dụng
  listLanguage: string;

  // danh sách ide sử dụng
  listId: string;

  // dung lượng của project
  capacity: string;

  // Link downloadSourceCode
  urlLink: string;

  // Mô tả dài
  longDesc: string;

  // Tài liệu hướng dẫn cài đặt
 guideInstall: string;
}
