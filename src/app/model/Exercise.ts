export interface Exercise {
  // id
  id: string;
  // idSession
  idSession: string;
  // tieu de bai tap
  title: string;
}
