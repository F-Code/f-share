export class Courseaffiliate {
    id: number;
    idMember: number;
    theCourseOfName: string;
    imagePreviewTitle: string;
    participantOfNumber: number;
    shortDescription: number;
    longDescription: string;
    curriculum: string;
    category: string;
    itemsOfNumber: string;
    totalStudyTime: string;
    price: number;
    feedback: string;
    type: number;
    status: boolean;
    imagesList: Array<string>;
    registrationPeriod: string;
    check: number;
}
