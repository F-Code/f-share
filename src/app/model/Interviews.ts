export class Interviews {
  id: String;
  idMember: string;
  familyName: String;
  emailAddress: String;
  phoneNumber: String;
  birthYear: String;
  status: Number;
}
