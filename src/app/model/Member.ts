export class Member {
  id: number;
  familyName: string;
  emailAddress: string;
  password: string;
  phoneNumber: string;
  avatar: string;
  status: string;
  displayName: string;
  birthYear: string;
  province: string;
  streetInformation: string;
  position: string;
  summary: string;
  school: string;
  department: string;
  grade: string;
  moneyIntalent: number;
  centralRight: string;
}

export class AccountChange {
  displayNameOrEmailAddress: string;
  password: string;
  remember: string;
}
