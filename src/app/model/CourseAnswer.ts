export interface CourseAnswer {
  // id
  id: string;
  // idMember
  idMember: string;
  // idexe
  idExercise: string;
  // idQuestionExercise
  idQuestionExercise: string;
  // dap an
  answerExercise: string;
  // lan choi
  timePlayed: string;
  // dung/sai
  status: number;
  // thoi gian choi
  durationExercise: string;
}
