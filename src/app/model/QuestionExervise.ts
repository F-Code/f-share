export interface QuestionExervise {
  // id
  td: string;
  // idExercise
  idExercise: string;
  // ten cau hoi
  reviewName: string;
  // cau tra loi
  content: string;
  // dap an dung
  rightAnswer: string;
  // diem
  point: number;
}
