export interface Login {
  displayNameOrEmailAddress: string;
  password: string;
}
