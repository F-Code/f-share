export interface Question {
  // id
  id: string;
  // idQuiz
  idQuiz: string;
  // ten cau hoi
  reviewName: string;
  // noi dung cau hoi
  content: string;
  // dap an A
  answerA: string;
  // dap an B
  answerB: string;
  // dap an C
  answerC: string;
  // dap an D
  answerD: string;
  // dap an dung: 
  correctAnswer: string;
  // diem
  point: number;
}
