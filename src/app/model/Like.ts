export interface Like {
  // id
  id: string;
  // id thao luan
  idDiscuss: string;
  // id member
  idMember: string;
  // idCommentDiscuss
  idCommentDiscuss: string;
  // like
  like: number;
  // disLike
  dislike: number;
}
