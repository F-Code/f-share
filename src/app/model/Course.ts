export interface Course {
  // id khoa hoc
  id: string;
  // tiêu đề khóa học
  name: string;
  // anh
  urlLink: string;
  // miêu tả khóa học
  desscription: string;
  // trạng thái khóa học
  status: number;
}
